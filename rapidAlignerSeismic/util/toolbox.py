#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy
import os
import sys
import cupy as cp

# Get the step according to the thread id  to obtain shifted time windows for templates
def getPassStep(numThread, length):

    passStep = numThread * length / 2
    return passStep

def date2decYear(utcDate):
    decYr    = float(utcDate.year) + (utcDate - UTCDateTime(utcDate.year, 1, 1))/(UTCDateTime(utcDate.year+1, 1, 1) - UTCDateTime(utcDate.year, 1, 1))

    return decYr


def progressbar(it, prefix="", size=60, out=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        print("{}[{}{}] {}/{}".format(prefix, u"█"*x, "."*(size-x), j, count),
                end='\r', file=out, flush=True)
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    print("\n", flush=True, file=out)


def getThreshold(corr, templateLen, systMultFactor):
    # Mean average deviation (MAD)
    #mad = cp.mean(cp.absolute(corr - cp.median(corr)))
    #treshold = float((mad * systMultFactor) / templateLen)

    # Noise root mean square deviation (RMS)
    rms = cp.sqrt(cp.mean(cp.square(corr / templateLen)))
    threshold = float(rms * systMultFactor)
    return threshold


#Remove from dailyFiles list files that have been already computed in a previous run in case of crash
def stopAndRestart(dailyFiles, outdir):
    outputFiles = os.listdir(outdir)
    newdailyFiles = dailyFiles.copy()

    for daily in dailyFiles:
        for output in outputFiles:
            if output.endswith("_0"):
            #We look for a perfect match on network, station, channel and date
                if output.split('.')[0:2]==daily.split('.')[0:2] and output.split('.')[2]==daily.split('.')[3] and output.replace('_','.').split('.')[3:5]==daily.split('.')[5:7]:
                    newdailyFiles.remove(daily)

    return newdailyFiles
