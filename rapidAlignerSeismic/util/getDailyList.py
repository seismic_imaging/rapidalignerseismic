#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy as np
import os
import cupy as cp
from rapidAlignerSeismic.util.getInfos import *
from rapidAlignerSeismic.util.Loader import DailyLoader

#Return two 2D list: one containing the values of all daily files, the other the information on daily filenames
def getDailyList(wfdir, quotiFiles, freqMin, freqMax, decimate):
    dailySigList = []
    dailyInfoList = []

    for daily in quotiFiles:
        # Load new daily file
        ntwkSt, chan, year, jday = getDailyInfo(daily)
        dailySig, dailySr, dailyTbeg, dailyTend = DailyLoader(wfdir + '/' + daily, freqMin, freqMax, decimate).data

        dailySigList.append(dailySig)
        dailyInfoList.append([ntwkSt, chan, year, jday, dailySr, dailyTbeg, dailyTend])

    return dailySigList, dailyInfoList

def getConcatList(wfdir, concatFiles):
    concatSigList = []
    concatInfoList = []

    for concat in concatFiles:
        ntwkSt, chan, year, jday, sr = getConcatInfo(concat)
        concatSig = np.genfromtxt(wfdir + '/' +concat)

        concatSigList.append(concatSig)
        concatInfoList.append([ntwkSt, chan, year, jday, sr])

    return concatSigList, concatInfoList


