import glob, os

import numpy as np
import matplotlib.pyplot as plt

from obspy.core import Stream, read
from obspy import UTCDateTime
from obspy.io.xseed import Parser


def MatchFinder(cc, tresh, jday, yr, n, sr):
    n_tr = 0  # initialize total number of matches
    match_lst = []
    tr_t_lst, tr_coef_lst = [], []

    # ----- Get idx of peaks above treshold -----
    idxs, coefs = [], []
    for idx in range(1,len(cc) - 1):
        if (
                cc[idx] > cc[idx - 1] and
                cc[idx] > cc[idx + 1] and
                cc[idx] / n >= tresh
        ):
            idxs.append(idx)
            coefs.append(cc[idx] / n)
    peaks = idxs


    # Here we store information about matches and corresponding cc
    for i, idx in enumerate(idxs):
        t = idx / sr
        tr_t = UTCDateTime(year=int(yr), julday=jday) + t
        tr_t_lst.append(tr_t)
        tr_coef_lst.append(coefs[i])

    # Total number of matches
    n_tr = n_tr + len(peaks)

    for i in range(n_tr):
        mtc = '{:d} {:} {:4.2f}'.format(i, tr_t_lst[i], tr_coef_lst[i])
        # print(mtc)
        match_lst.append(mtc)

    return match_lst


# _______________________________________________________________________________
def CleanMatches(match):
    dupls, match_clean = [], []

    # This is to identify the doublets to be removed
    for iline in match:
        it = UTCDateTime(iline.split()[1])
        icc = float(iline.split()[2])
        for jline in match:
            jt = UTCDateTime(jline.split()[1])
            jcc = float(jline.split()[2])

            tdiff = jt - it
            if tdiff <= 10 and tdiff >= 0 and icc != jcc:
                # dupls.append(jline)
                # TODO: Find better method to find duplicates
                if icc < jcc:
                    dupls.append(iline)
                elif jcc < icc:
                    dupls.append(jline)
                elif jcc == icc:
                    dupls.append(jline)

    # print line when not in the doublet list
    for line in match:
        if line not in dupls:
            match_clean.append(line)
            print(line)

    return match_clean


# _______________________________________________________________________________
def PlotCleanMatches(network, stat, channel, jday, sr, mtc_clean, wf_dir):
    tr_z_lst, tr_e_lst, tr_n_lst, tr_lst = [], [], [], []  # Init. list of traces
    tr_nrm_z_lst, tr_nrm_e_lst, tr_nrm_n_lst, tr_nrm_lst = [], [], [], []  # Init. list of norm. traces

    idxs = []
    for item in mtc_clean:
        t = UTCDateTime(item.split()[1])

        # Make sure only to take idx corresponding to jday
        if int(jday[-3:]) == t.julday:
            # print(jday,t.julday)
            t_h0 = UTCDateTime(item.split()[1].split('T')[0])
            idx = int((t - t_h0) * sr)

            idxs.append(idx)

    peaks = idxs

    # Extract matches
    st = Stream()
    # TODO : make sure that the trace starts at 00:00 !

    # load trace in stream: raises error if trace doesn't exist
    st += read(wf_dir + '/' + network + '.' + stat + '.00.' + channel + '.D.' + jday)

    st.merge(fill_value=0)
    t0 = st[0].stats.starttime
    # stnm = st[0].stats.station

    for tr in st:
        for tr_id, idx in enumerate(peaks):
            # Make a trace copy, estimate delay and trim
            tr_c = tr.copy()
            delay = idx / sr
            t = t0 + delay
            tr_c.trim(t, t + 10)

            # Clean up the traces a little bit
            tr_c.detrend('spline', order=2, dspline=sr)
            tr_c.taper(max_percentage=0.05, type="hann")
            tr_c.filter("bandpass", freqmin=1, freqmax=15)

            # Get traces amplitudes and normalize
            tr_amp = tr_c.data
            tr_amp_nrm = tr_amp / max(tr_amp)

            # one component case
            if len(st) == 1:
                tr_lst.append(tr_amp)
                tr_nrm_lst.append(tr_amp_nrm)

            # Here we create three lists, one for each comp
            if tr.stats.channel in ['BHZ', 'EHZ', 'HHZ']:
                tr_z_lst.append(tr_amp)
                tr_nrm_z_lst.append(tr_amp_nrm)

            elif tr.stats.channel in ['BHE', 'EHE', 'HHE']:
                tr_e_lst.append(tr_amp)
                tr_nrm_e_lst.append(tr_amp_nrm)

            elif tr.stats.channel in ['BHN', 'EHN', 'HHN']:
                tr_n_lst.append(tr_amp)
                tr_nrm_n_lst.append(tr_amp_nrm)
    # Plot matches
    # Multi component case
    if len(st) == 3:
        for comp in ['Z', 'E', 'N']:

            # Make sure to get the right component
            if comp == 'Z':
                tr_nrm_lst = tr_nrm_z_lst
                tr_lst = tr_z_lst

            elif comp == 'E':
                tr_nrm_lst = tr_nrm_e_lst
                tr_lst = tr_e_lst

            elif comp == 'N':
                tr_nrm_lst = tr_nrm_n_lst
                tr_lst = tr_n_lst

            # Create a np array with all the traces in tr_nrm_lst
            arr_tr = np.array(tr_nrm_lst)

            # Stack traces
            tr_amp_stk = np.mean(tr_lst, axis=0)
            tr_amp_stk_nrm = tr_amp_stk / max(tr_amp_stk)

            # Plot traces + stack
            for i_tr, tr_amp_nrm in enumerate(tr_nrm_lst):
                plt.plot(tr_amp_nrm * .5 + i_tr, '-k', linewidth=1)
            plt.plot(tr_amp_stk_nrm * .5 + i_tr + 1, '-r', linewidth=1)
            plt.title(stat + ' ' + comp)
            plt.show()

    # Single component case
    elif len(st) == 1:
        # Create a np array with all the traces in tr_nrm_lst
        arr_tr = np.array(tr_nrm_lst)
        # Stack traces
        tr_amp_stk = np.mean(tr_lst, axis=0)
        tr_amp_stk_nrm = tr_amp_stk / max(tr_amp_stk)

        # Plot traces + stack
        for i_tr, tr_amp_nrm in enumerate(tr_nrm_lst):
            plt.plot(tr_amp_nrm * .5 + i_tr, '-k', linewidth=1)
        plt.plot(tr_amp_stk_nrm * .5 + i_tr + 1, '-r', linewidth=1)
        plt.title(stat + ' ' + tr.stats.channel)
        plt.show()


# _______________________________________________________________________________
def MultiCompStack(net, stat, jday, outdir, n):
    print('Stacking station', stat, 'julian day', jday)
    ccfs = []  # Initialize list of stacks
    flist = sorted(glob.glob(outdir + '/corr.' + net + '.' + stat + '.HH?.' + jday + '.npy'))

    if len(flist) == 3:
        for f in flist:
            cc = np.load(f)
            ccfs.append(cc)
            stack = np.mean(ccfs, axis=0)

        mad = np.mean(np.absolute(stack - np.median(stack)))
        treshold = float((mad * 15) / n)

        t = []
        for i in range(len(stack)):
            t.append(i / (3600 * 100))
        ax = plt.gca()
        # ax.set_ylim([-0.6, 1])
        ax.set_xlim([0, 24])
        plt.plot(t, stack / n)
        plt.title('3-components stack' + ' ' + net + '.' + stat + ' ' + jday)
        plt.xlabel('Time [hour]')
        plt.ylabel('CC')
        plt.axhline(y=treshold, color='r', linestyle='--', linewidth=1)
        plt.show()

        # np.save(outdir+'/corr.'+net+'.'+stat+'.stacked.'+jday + '.npy', stack)
        return stack, treshold

    else:
        print('ERROR: the number of components must be 3,', len(flist), 'found')


# _______________________________________________________________________________

def plot_match(network, stat, channel, jday, n, sr, tresh, outDir, wf_dir):
    cc = np.load(outDir + '/corr.' + network + '.' + stat + '.' + channel + '.' + jday + '.npy')

    # Create x time vector:
    t = []
    for i in range(len(cc)):
        t.append(i / (3600 * sr))

    # Show normalized correlation coefficient between template and daily signal
    print('normalized correlation coefficients')
    sfig, ax = plt.subplots(1, sharex='col', sharey='row')
    ax.set_ylim([-1, 1])
    ax.set_xlim([0, 24])
    ax.plot(t, cc / n)  # Normalise correlation coefficient by lenght of the template
    ax.legend(channel[-1])
    ax.axhline(y=tresh, color='k', linestyle='--', linewidth=1)
    ax.set_xlabel('time [hours]')
    ax.set_ylabel('cc')
    plt.show()

    # Show template and waveform with the best correlation
    print('find matches...')
    match = MatchFinder(cc, tresh, jday[-3:], jday[:4], n, sr)

    print('cleanup matches...')
    matches_clean = CleanMatches(match)

    print('Plotting clean matches for station', stat)
    PlotCleanMatches(network, stat, channel, jday, sr, matches_clean, wf_dir)


# _______________________________________________________________________________

def plot_match_multi_comp(network, stat, channel, jday, n, sr, outDir, wf_dir):
    # Stack the 3-component stack and display
    cc, tresh = MultiCompStack(network, stat, jday, outDir, n)

    # Show template and waveform with the best correlation
    print('find matches...')
    match = MatchFinder(cc, tresh, jday[-3:], jday[:4], n, sr)

    print('cleanup matches...')
    matches_clean = CleanMatches(match)

    print('Plotting clean matches for station', stat)
    PlotCleanMatches(network, stat, channel, jday, sr, matches_clean, wf_dir)
