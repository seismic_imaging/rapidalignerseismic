# Copyright (c) 2020, NVIDIA CORPORATION.

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__all__ = ['TemplateLoader', 'DailyLoader']

import os
import cupy as cp
import numpy as np
from obspy.core import Stream, read
from obspy.io.xseed import Parser
from obspy import UTCDateTime
from scipy import stats
#import matplotlib.pyplot as plt # This is only for debug


class TemplateLoader :

    def __init__(self, path_to_patern, freqMin, freqMax, decimate):
        self.path_to_patern = path_to_patern
        self.freqMin = freqMin
        self.freqMax = freqMax
        self.decimate = decimate
	
    @property
    def query(self):
        Patern = read(self.path_to_patern)  #Patern
        #Patern.taper(max_percentage=0.05, type="hann")
        #Patern.filter("bandpass", freqmin=self.freqMin, freqmax=self.freqMax)

        # DEBUG: check template
        #plt.plot(Patern[0].data)

        # Decimate the data based on freqMax
        if self.decimate == True:
            decFactor = int(Patern[0].stats.sampling_rate / (2 * self.freqMax))
            Patern.decimate(factor=decFactor, no_filter=True)
            # DEBUG print freqMin freqmax and decFactor, plot decimated patern
            #oriSr = Patern[0].stats.sampling_rate * decFactor
            #print(self.freqMin, self.freqMax, decFactor, oriSr / decFactor)
            #plt.plot(np.linspace(0,Patern[0].stats.npts * decFactor,Patern[0].stats.npts),Patern[0].data)
            #plt.show()

        return Patern[0].data.astype('float64'), Patern[0].stats.npts, Patern[0].stats.sampling_rate

    @property
        
    def data(self):
        return self.query

class DailyLoader :

    def __init__(self, path_to_daily, freqMin, freqMax, decimate):
        self.path_to_daily = path_to_daily
        self.freqMin = freqMin
        self.freqMax = freqMax
        self.decimate = decimate
	
    @property
    def subject(self):
        # Load daily stream with Obspy
        Daily = read(self.path_to_daily)	#Day file
        assert self.freqMax <= Daily[0].stats.sampling_rate / 2

        # Stream preparation (tapering, removing gaps, etc.)
        Daily.taper(max_percentage=0.0001, type="hann")
        Daily.merge(fill_value=0)
        #t_ini = UTCDateTime(year=Daily[0].stats.starttime.year, julday=Daily[0].stats.starttime.julday)
        #t_ini = Daily[0].stats.starttime
        t_ini = UTCDateTime(year=int(self.path_to_daily[-8:-4]), julday=int(self.path_to_daily[-3:]))
        t_end = t_ini + 24 * 3600
        Daily.trim(t_ini ,t_end, pad=True, nearest_sample = False, fill_value=0)
        #!!Add BP: dirty fix
        if len(Daily[0].data) > int(24 * 3600 * round(Daily[0].stats.sampling_rate) ):
            Daily[0].data = Daily[0].data[:-int(len(Daily[0].data) - 24 * 3600 * round(Daily[0].stats.sampling_rate))]
        Daily.filter("bandpass", freqmin=self.freqMin, freqmax=self.freqMax)

        temp=np.copy(Daily[0].data)
        temp = np.ma.masked_where(abs(temp) < 1.e-100, temp)
        mad = stats.median_abs_deviation(temp.compressed()) / 1e3

        randVec = np.random.randint(-1, high=1, size=len(Daily[0].data), dtype=int)
        w = np.multiply(mad, randVec)
        #w = np.multiply(stats.median_abs_deviation(Daily[0].data) / 1e6, randVec)
        #w = np.multiply(np.amax(Daily[0].data) / 1e6, randVec)
        Daily[0].data = Daily[0].data + w


        # DEBUG: check daily
        #plt.plot(Daily[0].data)
        #plt.show()

        # Decimate the data based on freqMax
        if self.decimate == True:
            decFactor = int(Daily[0].stats.sampling_rate / (2 * self.freqMax))
            Daily.decimate(factor=decFactor, no_filter=True)
            # DEBUG print freqMin freqmax and decFactor
            #oriSr = Daily[0].stats.sampling_rate * decFactor
            #print(self.freqMin, self.freqMax, decFactor, oriSr / decFactor)

        return Daily[0].data.astype('float64'), Daily[0].stats.sampling_rate, t_ini, t_end
		
    @property
    def data(self):
        return self.subject

class SismoLoader :
    def __init__(self, path_to_daily, freqMin, freqMax):
        self.path_to_daily = path_to_daily
        self.freqMin = freqMin
        self.freqMax = freqMax


    @property
    def subject(self):
        # Load daily stream with Obspy
        Daily = read(self.path_to_daily)	#Day file
        assert self.freqMax <= Daily[0].stats.sampling_rate / 2

        # Stream preparation (tapering, removing gaps, etc.)
        Daily.taper(max_percentage=0.0001, type="hann")
        Daily.merge(fill_value=0)
        t_ini = Daily[0].stats.starttime
        #t_end = Daily[0].stats.endtime
        #Ajout BP
        #t_ini = UTCDateTime(year=Daily[0].stats.starttime.year, julday=Daily[0].stats.starttime.julday)
        t_end = t_ini + 24 * 3600
        Daily.trim(t_ini ,t_end, pad=False, nearest_sample = False, fill_value=0)
        #!!Add BP: dirty fix
        if len(Daily[0].data) > int(24 * 3600 * Daily[0].stats.sampling_rate ):
            Daily[0].data = Daily[0].data[:-int(len(Daily[0].data) - 24 * 3600 * Daily[0].stats.sampling_rate)]

        #Ajout BP
        randVec = np.random.randint(-10, high=10, size=len(Daily[0].data), dtype=int)
        w = np.multiply(np.amax(Daily[0].data) / 1e6, randVec)
        Daily[0].data = Daily[0].data + w
        Daily.filter("bandpass", freqmin=self.freqMin, freqmax=self.freqMax)

        location = Daily[0].stats.location
        # DEBUG: check daily
        #plt.plot(Daily[0].data)
        #plt.show()

        return Daily[0].data.astype('float64'), Daily[0].stats.sampling_rate, t_ini, t_end, location

    @property
    def data(self):
        return self.subject



if __name__ == "__main__":
	
        query   = TemplateLoader().data 
        subject = DailyLoader().data
        print(len(query), len(subject))
        
