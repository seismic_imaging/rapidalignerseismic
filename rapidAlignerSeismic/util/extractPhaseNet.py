#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import os
import numpy as np
from obspy import UTCDateTime
from collections import Counter
from rapidAlignerSeismic.util.Loader import SismoLoader


# Define the key to sort event per dates on a given station
def getDate(event):
    return event[0][2]


# Reconstruct file name with component
def getFileName(name, comp):
    splName = name.split('.')
    splName[3] = splName[3] + comp[-1]
    fileCompName = '.'.join(splName)
    return fileCompName


def getSr(component, daily, wfdir):
    dailyName = getFileName(daily, component)
    dailySig, dailySr, dailyTbeg, dailyTend, location = SismoLoader(wfdir + '/' + dailyName,10,20).data
    return dailySr


# Construct file name base on Code.Station.Component.dateBegin_dateEnd
def getConcatFileName(firstFile, lastFile, component, samplingRate):
    resFileName = getFileName(firstFile, component)
    resFileName = resFileName + '_' + lastFile[-3:]+'.'+str(samplingRate)
    return resFileName


# Method to handling events at the begining and the end of the day
def handlingBorderEvents(dailySig, yesterdaySig, stEventBeg, stEventEnd, dailyTbeg, dailyTend, dailySr, dailyName, wfdir,
                        offset, stShift, sizePt, freqMin, freqMax):
    daily = dailySig
    isDailyExtended = 0

    # The first event is too close to the daily file begining so need to add end of previous day's data
    if ((stEventBeg[2] - dailyTbeg) * dailySr) < stShift:
        print('beg')
        daily = np.append(yesterdaySig, dailySig)
        offset = stShift

    # The last event is too close to the daily file ending so need to add begin of the next day's data
    if ((dailyTend - stEventEnd[2]) * dailySr) < (sizePt - stShift):
        print('end')
        newDayName = dailyName[:-3] + str(dailyTbeg.julday + 1)
        dailyTemp, dailySr, dailyTTbeg, dailyTTend, location = SismoLoader(wfdir + '/' + newDayName, freqMin, freqMax).data

        daily = np.append(daily, dailyTemp[:int(sizePt - stShift)])
        isDailyExtended = 1

    return daily, offset, isDailyExtended



def extractPN(phaseNetFileName, concatEventsDir, length, t0Shift, userDailyLimit, wfdir, vecComp, pnEventsDir, freqMin, freqMax):

    pnDetect = np.genfromtxt(phaseNetFileName, dtype='str', skip_header=1, usecols=(0, 1, 2), delimiter=",")
    resInfo = pnDetect[:,0] # Contain info on every station, network : station.network..HH.D
    eventInfo = []

    if not os.path.isdir(pnEventsDir):
        os.mkdir(pnEventsDir)

    for i in range(len(pnDetect[:,2])):
        try:
            eventInfo.append(UTCDateTime(pnDetect[i,2])) # Contain info on every phasenet detection time: YYYY-MM-DDTHH:MM:ss.xxxxxxZ obspy format
        except:
            eventInfo.append('NAN')


    #Identify list stations to process
    uniqStat = list(set(resInfo))
    uniqStat.sort()

    #Don't know if this is because on results concatenation in working file or not, so in case
    if 'seedid' in uniqStat:
        uniqStat.remove('seedid')


    #If there is any duplicates for a station, we need to consider a special treatments
    eventList      = []
    duplicatesList = []


    #For each station, for each component, extract signal detected via phasenet
    for i in range(len(uniqStat)):
        stEventList   = []
        duplEventList = []
        dateList = []
        #Identify and remove duplicate
        for j in range(len(resInfo)):
            if resInfo[j] == uniqStat[i]:
                #Duplicate filter
                if(pnDetect[j][2] in dateList):
                    duplEventList.append(pnDetect[j])
                else:
                    # get the event: PhaseNet detection, sismo file name, event time in second
                    stEventList.append([pnDetect[j],
                    #resInfo[j].replace('00', '') + '.' + str(eventInfo[j].year) + '.' + str(eventInfo[j].julday), eventInfo[j]])
                    resInfo[j] + '.' + str(eventInfo[j].year) + '.' + str(eventInfo[j].julday).zfill(3), eventInfo[j]])
                dateList.append(pnDetect[j][2])
        stEventList.sort(key=getDate)


        dateEventList = []
        for j in range(len(stEventList)):
            dateEventList.append(stEventList[j][1])

        #Count the amount of unique events detected per day for a given station
        countEvent = dict(Counter(dateEventList))


        #Option to write extracted events in several files to avoid huge file in the end that may cause memory issue
        if userDailyLimit == 0:
            dailyLimit = len(countEvent)
        else:
            dailyLimit = userDailyLimit


        stEventConcat = np.array([])

        dailySr = getSr(vecComp[0], list(countEvent.keys())[0], wfdir)

        #Loop over component defined in vecComp
        for component in vecComp:
            totCount = 0
            yesterdaySig = np.array([])
            dailyCount = 0


            #Loop over days
            for daily in countEvent.keys():

                if(dailyCount%dailyLimit == 0):
                    try:
                        concat.close()
                        events.close()
                    except:
                        print("Let's begin\n")

                    #Create file to store results
					#This to handle possible not even between dailyLimit and amount of days to process
                    if (dailyCount+dailyLimit-1)<=len(countEvent):
                        endEx = dailyCount+dailyLimit-1
                    else:
                        endEx = len(countEvent)-1
                    concatFileName = getConcatFileName(list(countEvent.keys())[dailyCount],list(countEvent.keys())[endEx],component, dailySr)
                    concat = open(concatEventsDir+'/'+concatFileName, "w")
                    eventsName = phaseNetFileName.split('/')[-1] + '.' + concatFileName.split('.')[-2]
                    events = open(pnEventsDir+'/'+eventsName, "w")

                #Load daily + sampling
                print(daily)
                dailyName = getFileName(daily, component)
                dailySig, dailySr, dailyTbeg, dailyTend, location = SismoLoader(wfdir + '/' + dailyName, freqMin, freqMax).data

                sizePt = int(length * dailySr)
                offset = 0
                stShift = int(t0Shift * dailySr)

                #Special care for first and last event of the day because of possible overlap accross days
                dailySig, offset, isDailyExtended = handlingBorderEvents(dailySig, yesterdaySig, stEventList[totCount], stEventList[totCount+countEvent.get(daily)-1], dailyTbeg, dailyTend, dailySr, dailyName, wfdir, offset, stShift, sizePt, freqMin, freqMax)

                #Loop over events for this day
                for ev in range(countEvent.get(daily)):

                    idEv = int((stEventList[totCount][2] - dailyTbeg) * dailySr + offset - stShift)
                    events.write(str(stEventList[ev][0])[1:-1]+'\n')

                    stEventConcat = np.append(stEventConcat, dailySig[idEv:idEv+sizePt])
                    totCount += 1

                #Just store the X last points of the current day signal in case for use in border handling method
                if isDailyExtended == 0:
                    yesterdaySig = dailySig[-stShift:]
                else:
                    yesterdaySig = dailySig[-sizePt:-sizePt+stShift]

                print("Len of concatenated vector=",len(stEventConcat)," vs len expected= ",countEvent.get(daily)*sizePt)
                np.savetxt(concat,stEventConcat, fmt='%f', delimiter=' ')
                stEventConcat = np.array([])


        eventList.append(stEventList)
        duplicatesList.append(duplEventList)

    concat.close()
    events.close()