#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import glob, os

import numpy as np
import matplotlib.pyplot as plt

from obspy.core import Stream, read
from obspy.io.xseed import Parser

#-------------------------------------------------------------------------------
def GetTemplateLength(tplf):
    # Load template in obspy stream
    st = Stream()
    st += read(tplf)
    sr = st[0].stats.sampling_rate

    # Resample template if sr > 100 Hz
    # This is to mimic what actually happens in SismicMatch loader
    if sr > 100.:
        st.resample(100., strict_length=False, no_filter=True)
    
    #st.decimate(factor=2, strict_length=False, no_filter=True)
    n = st[0].stats.npts
    return n

#-------------------------------------------------------------------------------
def MultiCompStack(stat, jday, n):

    print('Stacking station', stat, 'julian day', jday)
    ccfs = []	# Initialize list of stacks
    #flist = sorted(glob.glob(workdir + 'ccf_sauvt/corr.*' + stat + '*?H?*.' + str(jday) + '.npy'))
    #flist = sorted(glob.glob(workdir + 'ex_cnrs/corr.*' + stat + '*?H?*.' + str(jday) + '.npy'))
    flist = sorted(glob.glob(workdir + 'ccf_chile/corr.*' + stat + '*?H?*.' + str(jday) + '.npy'))

    if len(flist) == 3:
        for f in flist:
            ntwk = os.path.basename(f).split('.')[1]
            year = os.path.basename(f).split('.')[4]

            cc = np.load(f)
            ccfs.append(cc)
            stack = np.mean(ccfs, axis=0)

        # Plot to make sure that everything is fine
        #ax = plt.gca()
        #ax.set_ylim([0, 1])
        #plt.plot(ccfs[0] / n, color='gray')
        #plt.plot(ccfs[1] / n, color='gray' )
        #plt.plot(ccfs[2] / n, color='gray')
        #plt.plot(stack / n, color='r')
        #plt.plot(stack / n)
        #plt.title('3-components stack' + ' ' + stat + ' ' + year + '.' + str(jday))
        #plt.axhline(y=0.3, color='k', linestyle='--', linewidth=1)
        #plt.show()
        #plt.savefig(stat + '_' + year + '.' + str(jday) + '.png', format='png')

        # Other way to plot (paper)
        #Create x time vector:
        t = []
        for i in range(len(stack)):
            t.append(i / (3600 * 100))

        tresh = 0.3

        sfig, ax = plt.subplots(4, sharex='col', sharey='row')
        ax[0].set_ylim([0, 1])
        ax[0].set_xlim([0, 24])
        ax[0].plot(t, ccfs[0] / n)
        ax[0].legend('Z')
        ax[0].axhline(y=tresh, color='k', linestyle='--', linewidth=1)
        ax[1].set_xlim([0, 24])
        ax[1].set_ylim([0, 1])
        ax[1].plot(t, ccfs[1] / n)
        ax[1].axhline(y=tresh, color='k', linestyle='--', linewidth=1)
        ax[1].legend('E')
        ax[2].set_xlim([0, 24])
        ax[2].set_ylim([0, 1])
        ax[2].plot(t, ccfs[2] / n)
        ax[2].axhline(y=tresh, color='k', linestyle='--', linewidth=1)
        ax[2].legend('N')
        ax[3].set_xlim([0, 24])
        ax[3].set_ylim([0, 1])
        ax[3].plot(t, stack / n, '-r')
        ax[3].legend('Stack')
        ax[3].axhline(y=tresh, color='k', linestyle='--', linewidth=1)
        plt.show()

        # Write stack to file
        #np.save('stacked_ccf_sauvt/corr.' + ntwk +'.'+ stat + '.stacked.' + str(year) + '.' + str(jday) + '.npy', stack / n)
        np.save('stacked_ccf_chile/corr.' + ntwk +'.'+ stat + '.stacked.' + str(year) + '.' + str(jday) + '.npy', stack / n)      
        #np.save('ex_cnrs/corr.' + ntwk +'.'+ stat + '.stacked.' + str(year) + '.' + str(jday) + '.npy', stack / n)

    else:
        print('ERROR: the number of components must be 3,', len(flist), 'found')

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    workdir = './'

    stat_lst = ['MT03']
    #stat_lst = ['S0313', 'S0316', 'S0319', 'S0513', 'S0516', 'S0519', 'S0719',
    #s            'N0117', 'N0118', 'N0413', 'N0715', 'N0717', 'N0718', 'MBB11']
    jday_lst = ['053']
    #jday_lst = []
    #for jday in range(90,275):
    #    jday_lst.append('{:03d}'.format(jday))
    #stat_lst = ['S0313', 'S0316', 'S0319', 'S0513', 'S0516', 'S0519', 'S0716', 'S0719']
    #stat_lst = ['N0118', 'N0718', 'N0715', 'N0117', 'N0413', 'N0717']

    for stat in stat_lst:
        n_sample = GetTemplateLength(workdir + 'templates_chile/*' + stat + '*')
        #print(n_sample)

        for jday in jday_lst:
            MultiCompStack(stat, jday, n_sample)
