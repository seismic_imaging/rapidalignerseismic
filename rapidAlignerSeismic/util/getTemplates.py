#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy
import os
import cupy as cp
from rapidAlignerSeismic.util.getInfos import *
from rapidAlignerSeismic.util.Loader import TemplateLoader

#Return two 2D list: one containing the values of all templates, the other the information on templates filenames
def getTemplateList(templates, vecComp, freqMin, freqMax, decimate, template_dir):
    templatesList       = []
    templateSigList_gpu = []
    templatesNameList   = []
    for i in range(len(vecComp)):
        templatesList.append(getList(vecComp[i], templates))

    # Create a 2D list of templates: 1D for each component and 1D for each template along this component
    for i in range(len(templatesList)):
        templateSigList  = []
        templateNameList = []
        for j in range(len(templatesList[i])):
            templateSig, templateLen, templateSr = TemplateLoader(template_dir +'/'+ templatesList[i][j], freqMin, freqMax, decimate).data
            templateSigList.append(cp.asarray(templateSig))
            templateNameList.append(getTemplInfo(templatesList[i][j]))

        templateSigList_gpu.append(templateSigList)
        templatesNameList.append(templateNameList)

    return templateSigList_gpu, templatesNameList


#Generate template list from daily file
def TrimDaily2Templates(dailySig, length, sampling, tbegin, tend, pStep=0):
    #initialise
    ts= tbegin
    iterStep = round(pStep * sampling)
    iter1 = iterStep
    step = round((length * sampling))
    iter2 = iter1 + step - 1 # JBA 2022-08-24: Avoid iter2-iter1=999 when pStep=10

    templSigList = []

    # Perform splitting of the daily file in templates and store them
    while ts <= tend - ( length - 1 / sampling):
        t1 = ts + pStep
        t2 = ts + length - 1 / sampling + pStep

        if t2 > tend:
            #print('pass window out of bound, t2 = te')
            t2 = tend
            iter2 = len(dailySig) - 1

        templSig = dailySig[iter1:iter2]
        templSigList.append(cp.asarray(templSig))

        #print(len(templSig))

        ts += length
        iter1 += step
        iter2 += step

    return templSigList

#Generate template list from concatenated events
def TrimConcat2Templates(concatSig, length, sampling):
    step = round(length * int(sampling))
    iter1 = 0
    iter2 = step

    templSigList = []

    while iter2 <= len(concatSig):
        templSig = concatSig[iter1:iter2]
        templSigList.append(cp.asarray(templSig))
        iter1 += step
        iter2 += step

    return templSigList


    