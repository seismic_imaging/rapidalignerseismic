#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy
import os
import cupy as cp
from rapidAlignerSeismic.util.toolbox import getThreshold

# For DEBUG only
import matplotlib.pyplot as plt

#Return the list of templates filenames
def getList(comp, templates):
    #templateList = []
    #templates = os.listdir(template_dir)
    templateList = [file for file in templates if comp in file]
    return templateList

#Return informations contained in template filename
def getTemplInfo(tempName):
    tempNtwSt = tempName.split(".")[0]+'.'+tempName.split(".")[1]
    tempDate  = tempName.split(".")[-4]+'.'+tempName.split(".")[-3]+'.'+tempName.split(".")[-2]+'.'+tempName.split(".")[-1]
    return tempNtwSt,tempDate


#Return informations contained in daily filenames
def getDailyInfo(daily):
    ntwk    = daily.split(".")[0]
    station = daily.split(".")[1]
    chan    = daily.split(".")[3]
    year    = daily.split(".")[5]
    jday    = daily.split(".")[6]
    return ntwk+'.'+station, chan , year, jday


def getConcatInfo(concat):
    ntwk = concat.split(".")[0]
    station = concat.split(".")[1]
    chan = concat.split(".")[3]
    year = concat.split(".")[5]
    jday = concat.split(".")[6]
    sr   = concat.split(".")[7]
    return ntwk + '.' + station, chan, year, jday, sr

# Filter best matches
def getBestMatch(corr, templateLen, dailyTbeg, dailyNextTbeg, idxAutoCorr, idxNextAutoCorr,  dailySr, numThread, thresholdMeth, threshVal, systMultFactor):
    match = []

    # Filter best matches
    # Dynamic treshold based on mean average deviation (MAD)
    if(thresholdMeth == "RMS"):
        threshold = getThreshold(corr, templateLen, systMultFactor)
    else:
        threshold = threshVal

    # DEBUG: plot corr and treshold for all sub-windows
    #plt.plot(cp.asnumpy(corr) / templateLen)
    #plt.axhline(y = treshold, color = 'r', linestyle = '--')
    #plt.axhline(y = treshold * -1, color = 'r', linestyle = '--')
    #plt.axhline(y = treshold * 5, color = 'b', linestyle = '--')
    #plt.axhline(y = treshold * 10, color = 'g', linestyle = '--')
    #plt.ylim([-1, 1])
    #plt.show()

    # Get max value
    maxCorrVal = int(cp.amax(corr))
    if (maxCorrVal / templateLen) > threshold:
        idxMaxCorr = int(cp.argmax(corr))
        maxCorrTime = dailyNextTbeg + (idxNextAutoCorr + idxMaxCorr) / dailySr
        patternTime = dailyTbeg + idxAutoCorr / dailySr
        match = [numThread, patternTime, maxCorrTime, '{:.2f}'.format(maxCorrVal / templateLen)]

        # DEBUG: print MaxCorrTime and patternTime
        #print('From GetInfos', patternTime, maxCorrTime, maxCorrVal/templateLen, templateLen, dailySr)

        # DEBUG: plot corr and treshold for all sub-windows
        #plt.plot(cp.asnumpy(corr) / templateLen)
        #plt.axhline(y = treshold, color = 'r', linestyle = '--')
        #plt.ylim([-1, 1])
        #plt.show()

    return match

def getBestMatchPN(corr, templateLen, Jday, JNextDay, id, idxNextAutoCorr, numThread, systMultFactor):
    match = []

    # Filter best matches
    # Dynamic treshold based on mean average deviation (MAD)
    treshold = getTreshold(corr, templateLen, systMultFactor)

    # DEBUG: plot corr and treshold for all sub-windows
    # plt.plot(cp.asnumpy(corr) / templateLen)
    # plt.axhline(y = treshold, color = 'r', linestyle = '--')
    # plt.axhline(y = treshold * -1, color = 'r', linestyle = '--')
    # plt.axhline(y = treshold * 5, color = 'b', linestyle = '--')
    # plt.axhline(y = treshold * 10, color = 'g', linestyle = '--')
    # plt.ylim([-1, 1])
    # plt.show()

    # Get max value
    maxCorrVal = int(cp.amax(corr))
    if (maxCorrVal / templateLen) > treshold:
        idxMaxCorr = int(cp.argmax(corr))
        idDetect = (idxMaxCorr) % templateLen + id
        #paternDetect = idxAutoCorr % templateLen  #PAS CA!!!

        match = [numThread, JNextDay, idDetect, Jday, id,'{:.2f}'.format(maxCorrVal / templateLen)]

        # DEBUG: print MaxCorrTime and patternTime
        # print('From GetInfos', patternTime, maxCorrTime, maxCorrVal/templateLen, templateLen, dailySr)

        # DEBUG: plot corr and treshold for all sub-windows
        # plt.plot(cp.asnumpy(corr) / templateLen)
        # plt.axhline(y = treshold, color = 'r', linestyle = '--')
        # plt.ylim([-1, 1])
        # plt.show()

    return match

