import glob, os

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from obspy.core import Stream, read
from obspy import UTCDateTime
from obspy.io.xseed import Parser

def visuMatchFinder(stat, jday, tresh):
    tr_t_lst, tr_coef_lst  = [], []
    #match_lst = []
    n_tr = 0 # that's the total number of matches
    mtc_outf = open('matches.out', 'w')

    #for jday in jday_lst:
    print('Processing station', stat, 'Julian day', jday)
    cc_stack = np.load('stacked_ccf_chile/corr.C1.MT03.stacked.2018.'+jday+'.npy')

    # ----- Get idx of peaks above treshold -----
    idxs, coefs = [], []
    for idx in range(len(cc_stack)):
        if (
            cc_stack[idx] > cc_stack[idx - 1] and
            cc_stack[idx] > cc_stack[idx + 1] and
            cc_stack[idx] >= tresh
           ):

             idxs.append(idx)
             coefs.append(cc_stack[idx])
    peaks = idxs

    # Here we store information about matches and corresponding cc
    for i,idx in enumerate(idxs):
        t = idx / 100 # TODO must be a soft value
        tr_t = UTCDateTime(year=2018, julday=jday) + t # TODO year must be a soft value
        tr_t_lst.append(tr_t)
        tr_coef_lst.append(coefs[i])

    # Total number of matches
    n_tr = n_tr + len(peaks)

    for i in range(n_tr):
        mtc = '{:d} {:} {:4.2f}'.format(i, tr_t_lst[i], tr_coef_lst[i])
        print(mtc, file=mtc_outf)
        #match_lst.append(mtc)
        #print(i, tr_t_lst[i], '{:4.2f}'.format(tr_coef_lst[i]), file=mtc_outf)
        #if tr_coef_lst[i] >= .99:
        #    i_tpl = i   # Get template ID
        #else:
        #    i_tpl = 0
    mtc_outf.close()
    #return match_lst

#_______________________________________________________________________________
def CleanMatches():
    dupls, match_lst_clean = [], []

    # Reads pickf and store values
    with open('matches.out') as f:
        match_lst = []
        for line in f:
            match_lst.append(line.rstrip('\n'))

    # This is to identify the doublets to be removed
    for iline in match_lst:
        it   = UTCDateTime(iline.split()[1])
        icc = float(iline.split()[2])
        for jline in match_lst:
            jt   = UTCDateTime(jline.split()[1])
            jcc = float(jline.split()[2])

            tdiff = jt-it
            if tdiff <= 10 and tdiff >= 0 and icc != jcc :
                 dupls.append(jline)
                #print(it, iprob, jt, jprob, tdiff)
                #if icc < jcc:
                #    dupls.append(iline)
                #elif jcc < icc:
                #    dupls.append(jline)
                #elif jcc == icc:
                #    dupls.append(jline)

    # print line when not in the doublet list
    for line in match_lst:
        if line not in dupls:
            match_lst_clean.append(line)

    return match_lst_clean
#_______________________________________________________________________________
def PlotCleanMatches(stat, jday_lst, mtc_lst_clean):
    tr_z_lst, tr_e_lst, tr_n_lst             = [], [], [] # Init. list of traces
    tr_nrm_z_lst, tr_nrm_e_lst, tr_nrm_n_lst = [], [], [] # Init. list of norm. traces
    #idxs = []
    print('Plotting clean matches for station', stat)

    for jday in jday_lst:
        idxs = []
        for item in mtc_lst_clean:
            t    = UTCDateTime(item.split()[1])

            # Make sure only to take idx corresponding to jday
            if int(jday) == t.julday:
                #print(jday,t.julday)
                t_h0 = UTCDateTime(item.split()[1].split('T')[0])
                idx  = int((t - t_h0) * 100) # TODO sr (100Hz) must be a soft value

                idxs.append(idx)

        peaks = idxs

        # read templ
        #st = Stream()
        #st += read('./templates_sauvt/??.' + stat + '.00.EH?.D.2017.226*')
        #print(st)

        #for tr in st:
        #    if tr.stats.channel == 'EHZ':
        #        tp_amp_z = tr.data
        #    elif tr.stats.channel == 'EHE':
        #        tp_amp_e = tr.data
        #    elif tr.stats.channel == 'EHN':
        #        tp_amp_n = tr.data


        # Extract matches
        st = Stream()
        # TODO : faire un tag + universel pour les noms de fichiers
        # TODO : stream processing detrend, demean etc.
        # TODO : make sure that the trace starts at 00:00 !

        # load trace in stream: raises error if trace doesn't exist
        try:
            st += read(workdir + 'wf_chile/??.' + stat + '.00.?H?.D.2018.' + jday)
        except Exception:
            continue

        st.merge(fill_value=0)
        #st.detrend('spline', order=2, dspline=100)
        st.detrend("linear")
        st.detrend("demean")


        t0   = st[0].stats.starttime
        dt   = st[0].stats.sampling_rate
        stnm = st[0].stats.station

        for tr in st:
            for tr_id, idx in enumerate(peaks):
                # Make a trace copy, estimate delay and trim
                tr_c = tr.copy()
                delay = idx / dt
                t = t0 + delay
                tr_c.trim(t, t+10)

                # Clean up the traces a little bit
                tr_c.detrend('spline', order=2, dspline=dt)
                #tr_c.detrend("linear")
                #tr_c.detrend("demean")
                tr_c.taper(max_percentage=0.05, type="hann")
                tr_c.filter("bandpass", freqmin=1, freqmax=15)

                # Get traces amplitudes and normalize
                tr_amp     = tr_c.data
                tr_amp_nrm = tr_amp / max(tr_amp)
                #tr_lst.append(tr_amp)
                #tr_nrm_lst.append(tr_aM03mp_nrm)

                # Here we create three lists, one for each comp
                if tr.stats.channel == 'EHZ' or tr.stats.channel == 'HHZ':
                    #coef = np.dot(tr_amp, tp_amp_z) / np.dot(tp_amp_z,tp_amp_z)
                    tr_z_lst.append(tr_amp)
                    #tr_nrm_z_lst.append(tr_amp/coef)
                    tr_nrm_z_lst.append(tr_amp_nrm)

                elif tr.stats.channel == 'EHE' or tr.stats.channel == 'HHE':
                    #coef = np.dot(tr_amp, tp_amp_e) / np.dot(tp_amp_e,tp_amp_e)
                    tr_e_lst.append(tr_amp)
                    #tr_nrm_e_lst.append(tr_amp/coef)
                    tr_nrm_e_lst.append(tr_amp_nrm)

                elif tr.stats.channel == 'EHN' or tr.stats.channel == 'HHN':
                    #coef = np.dot(tr_amp, tp_amp_n) / np.dot(tp_amp_n,tp_amp_n)
                    tr_n_lst.append(tr_amp)
                    #tr_nrm_n_lst.append(tr_amp/coef)
                    tr_nrm_n_lst.append(tr_amp_nrm)
    # Plot matches
    for comp in ['z', 'e', 'n']:

        # Make sure to get the right component
        if comp == 'z':
            tr_nrm_lst = tr_nrm_z_lst
            tr_lst = tr_z_lst

        elif comp == 'e':
            tr_nrm_lst = tr_nrm_e_lst
            tr_lst = tr_e_lst

        elif comp == 'n':
            tr_nrm_lst = tr_nrm_n_lst
            tr_lst = tr_n_lst

        # Create a np array with all the traces in tr_nrm_lst
        arr_tr = np.array(tr_nrm_lst)

        # Stack traces
        tr_amp_stk     = np.mean(tr_lst, axis=0)
        tr_amp_stk_nrm = tr_amp_stk / max(tr_amp_stk)

        # Plot traces + stack
        for i_tr, tr_amp_nrm in enumerate(tr_nrm_lst):
            plt.plot(tr_amp_nrm * .5 + i_tr, '-k', linewidth=1)
        plt.plot(tr_amp_stk_nrm * .5 + i_tr + 1, '-r', linewidth=1)
        plt.title(stnm + '_' + comp)

        #plt.show()

        # Plot matrix + stack
        #fig, ax = plt.subplots(2, sharex='col', sharey='row')
        #ax[0].imshow(arr_tr, interpolation='none', aspect='auto')
        #ax[0].set_title(stnm + '-' + comp)

        #for idx, tr_amp_nrm in enumerate(tr_nrm_lst):   # All norm. traces
        #    ax[1].plot(tr_amp_nrm, color='lightgray', linewidth=1)

        #ax[1].plot(tr_templ, '-b', linewidth=1, label='Template')   #Template
        #ax[1].plot(tr_amp_stk_nrm, '-r', linewidth=1, label='Stack')   #stack

        plt.show()

def visu_matches(stat, jday, tresh, workdir):
    os.chdir(workdir)
    # Find matches
    visuMatchFinder(stat, jday[-3:], tresh)
    # Clean up matches
    print('clean up matches...')
    matches_clean = CleanMatches()

    # Export clean matches to file
    outf = open('matches_clean.out.tout', 'w')
    for match in matches_clean:
        print(match)
        print(match, file=outf)

    # Plot matches
    PlotCleanMatches(stat, jday[-3:], matches_clean)


#_______________________________________________________________________________
if __name__ == "__main__":

    workdir = './'
    #workdir = '/Volumes/jibedata/Work/templ_match/'
    stat     = 'MT03'
    #jday_lst = ['220','221','222','223','224','225','226','227','228','229']
    jday_lst = ['053']

    tresh    = .2
    visu_matches(stat, jday_lst, tresh)

    # Create jday_lst
    #jday_lst = []
    #for jday in range(90,275):
    #     jday_lst.append('{:03d}'.format(jday))

    # Find matches
    #MatchFinder(stat, jday_lst, tresh)

    # Clean up matches
    print('clean up matches...')
    #matches_clean = CleanMatches()

    # Export clean matches to file
    #outf = open('matches_clean.out.tout','w')
    #for match in matches_clean:
    #    print(match)
    #    print(match, file=outf)

    # Plot matches
    #PlotCleanMatches(stat, jday_lst, matches_clean)

