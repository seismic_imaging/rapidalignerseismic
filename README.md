# RapidAlignerSeismic (RAS)

## About

RapidAlignerSeismic is a fork from the [rapidAligner](https://github.com/NVIDIA/rapidAligner) project designed to perform match filtering analysis on seismic data.
rAS  is a CUDA-accelerated library for the windowed alignment of time series using Obspy, Numba and Cupy

If you use RAS in research work, please quote the following article: J.B. Ammirati et al.: RapidAligner Seismic (RAS): A fast GPU-based approach for template matching


## Contains

The present repository contains:
1. RapidAlignerSeismic sources
2. A set of seismic data from the Centro Sismológico Nacional’s permanent network (https://doi.org/10.7914/SN/C1)
3. Example notebooks

## Required Hardware
- at least one NVIDIA GPU

## Required Software

Either:
- Singularity 2.6 or higher (you can follow Singularity documentation to install it for 3.4 or less [here](https://sylabs.io/guides/3.4/user-guide/installation.html#) and 3.5 [here](https://sylabs.io/guides/3.5/admin-guide/installation.html#))
-Latest version of NVIDIA driver according to your card and system

or
- GCC version 7.3.1 or higher
- Python 3.x
- CUDA 11
- pip version 21.2.4 or higher
- conda 4.12.0 or higher
- OpenMPI 3.1.4 or higher (optional)

## Singularity container

Build the singularity container from the main repository using the 'Singularity.Rapid.def' recipe. 

## Conda environment

If you build the singularity container, following previous step, the conda environment is already setup.
Once in the container, type:

<pre><code>source ~/.bashrc 
conda activate rapidSis
</code></pre>

If you are not using the container, you can use the 'align.txt' file to create your own conda environment:

<pre><code>conda config --add channels conda-forge
conda create --name rapidSis --file align.txt
</code></pre>

## mpi4py

If you want to use RAS on several GPUs, first you'll need to have OpenMPI already installed on your machine.
Then you'll need to activate your conda environment and type:
`pip install mpi4py`

DO NOT use `conda install` as it will use a precompiled version of mpi4py that might not be compatible with the rest of your environment.

To use RAS on parallel, please refer to the HOWTO document in the notebooks repository
