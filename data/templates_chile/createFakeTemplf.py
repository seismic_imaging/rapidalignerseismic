import os, sys
import numpy as np
from obspy import UTCDateTime, read, Trace, Stream

def ParseArgs(loa):
    if len(loa) == 6:
        net  = str(loa[1])
        stn  = str(loa[2])
        npts = int(loa[3])
        n    = int(loa[4])
        sr   = int(loa[5])
    else:
        print('USAGE: createFakeTemplf.py <network> <station> <npts> <n_templates> <sampling_rate>')
        exit()

    return net, stn, npts, n, sr

def GenerateMSEEDf(i, length, network, station, channel, sr):
    
    # Create a vector of random floats
    data = np.random.uniform(low=-1e5, high=1e5, size=length)
    
    # Fill header attributes
    stats = {'network': network, 'station': station, 'location': '',
             'channel': channel, 'npts': len(data), 'sampling_rate': sr,
             'mseed': {'dataquality': 'D'}}
    
    # set current time
    t0 = UTCDateTime(1985, 12, 28, 0, 0) + i * 24 * 3600
    stats['starttime'] = t0
    jday = '{:03d}'.format(t0.julday)
    year = t0.year
    st = Stream([Trace(data=data, header=stats)])
    
    # write as MSEED file
    st.write(net+'.'+stn+'.00.'+chan+'.D.'+str(year)+'.'+str(jday)+'.0000.00.00', format='MSEED')

if __name__ == "__main__":
    
    arg_lst = sys.argv
    net, stn, m, n, sr = ParseArgs(arg_lst)

    for i in range(n):
        for chan in ['HHE', 'HHN', 'HHZ']:
            GenerateMSEEDf(i, m, net, stn, chan, sr)
