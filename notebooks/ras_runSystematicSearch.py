#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime
from mpi4py import MPI
import numpy
import os
import cupy as cp
import sys, time

# For debug Only
import matplotlib.pyplot as plt

from data.extratPN import endEx

# This ensures to use the source code if not installed as Python package
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras

def getDates(dateUniq, numThread, numThreadMax):

    dates = []
    if numThreadMax > 1:
        sizeDateProc = int(len(dateUniq)/(numThreadMax/2))
        begEx = int(numThread/2)*sizeDateProc
        endEx = (int(numThread/2)+1)*sizeDateProc
    else:
        begEx = 0
        endEx = len(dateUniq)

    for i in range(begEx,endEx):
        dates.append(dateUniq[i])

    return dates

# Perform matching for a given daily file over each template
def doSystematicAlign(dailySig_gpu, templatelistComp_gpu, dailySigList_gpu, dailyInfoList, length, passStep, idBeg, thresholdMeth, threshVal, systMultFactor):
    matchList = []

    ntwkSt    = dailyInfoList[idBeg][0]
    dailySr   = dailyInfoList[idBeg][4]
    dailyTbeg = dailyInfoList[idBeg][5]

    for i in range(len(templatelistComp_gpu)):
        templateLen = len(templatelistComp_gpu[i])

        # Get the idx of the autocorrelation and mask the peak
        idxAutoCorr = int(i * (length * dailySr) + (passStep * dailySr))

        # Perform patern matching
        # Using Rolling mean- and amplitude-adjusted Euclidean Distance using FFT
        dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySig_gpu[idxAutoCorr:], mode="fft")

        # Mask the autocorrelation peak
        corr[0:templateLen / 2] = 0.

        # Filter best matches
        match = ras.getBestMatch(corr, templateLen, dailyTbeg, dailyTbeg, idxAutoCorr, idxAutoCorr, dailySr, numThread, thresholdMeth, threshVal, systMultFactor)

        # DEBUG Plot pattern and Daily to to check that there's no monkey business
        #threshold = ras.getThreshold(corr, templateLen, systMultFactor)
        #maxCorrVal = int(cp.amax(corr))
        #if (maxCorrVal / templateLen) > threshold and (maxCorrVal / templateLen) < 0.98:
        #    idxMaxCorr = int(cp.argmax(corr))
        #    maxCorrTime = dailyTbeg + (idxAutoCorr + idxMaxCorr) / dailySr
        #    patternTime = dailyTbeg + idxAutoCorr / dailySr
        #    plt.plot(cp.asnumpy(templatelistComp_gpu[i]))
        #    plt.plot(cp.asnumpy(dailySig_gpu[idxAutoCorr+idxMaxCorr:idxAutoCorr+idxMaxCorr+templateLen]))
        #    plt.show()

        if len(match) != 0:
            matchList.append(match)

        #While match is empty we carry on with the same template on other daily files from the same station
        if len(match) == 0 and idBeg != len(dailySigList_gpu)-1:
            for j in range(1,len(dailySigList_gpu[idBeg:])) :
                if dailyInfoList[idBeg + j][0] == ntwkSt :
                    dailySrNext = dailyInfoList[idBeg + j][4]
                    dailyNextTbeg = dailyInfoList[idBeg + j][5]

                    #Compute match Perform patern matching
                    # Using Rolling mean- and amplitude-adjusted Euclidean Distance using FFT
                    dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySigList_gpu[idBeg+j], mode="fft")

                    # Filter best matches
                    match = ras.getBestMatch(corr, templateLen, dailyTbeg, dailyNextTbeg, idxAutoCorr, 0, dailySrNext, numThread, thresholdMeth, threshVal, systMultFactor)

                    # DEBUG Plot pattern and Daily to check that there's no monkey business
                    #threshold = ras.getThreshold(corr, templateLen, systMultFactor)
                    #maxCorrVal = int(cp.amax(corr))
                    #if (maxCorrVal / templateLen) > threshold:
                    #    idxMaxCorr = int(cp.argmax(corr))
                    #    maxCorrTime = dailyNextTbeg + (0 + idxMaxCorr) / dailySr
                    #    patternTime = dailyTbeg + idxAutoCorr / dailySr
                    #    print(patternTime, maxCorrTime)
                    #    plt.plot(cp.asnumpy(templatelistComp_gpu[i]))
                    #    plt.plot(cp.asnumpy(dailySigList_gpu[idBeg+j][idxMaxCorr:idxMaxCorr+templateLen]))
                    #    plt.show()

                #If a match is found we stop the search for this template
                if len(match) != 0:
                    matchList.append(match)
                    break
    return matchList

def alignComposant(wfdir, quotiFilesComp, passStep, length, freqMin, freqMax, decimate, thresholdMeth, threshVal, systMultFactor):
    # Load daily file list for the given component
    dailySigList, dailyInfoList = ras.getDailyList(wfdir, quotiFilesComp, freqMin, freqMax, decimate)
    dailySigList_gpu = cp.asarray(dailySigList)
    matchList = []
    for i in range(len(dailySigList)):
        print('Thread=', numThread, 'Processing:', dailyInfoList[i][0] + '.' + dailyInfoList[i][1] + 
              '.' + str(dailyInfoList[i][2]) + '.' + str(dailyInfoList[i][3]))

        # From the daily file generate the array of templates
        templateSigList_gpu = ras.TrimDaily2Templates(dailySigList[i], length, dailyInfoList[i][4], dailyInfoList[i][5],
                                                      dailyInfoList[i][6], passStep)
        matchList = doSystematicAlign(dailySigList_gpu[i], templateSigList_gpu, dailySigList_gpu, dailyInfoList, length,
                                      passStep, i, thresholdMeth, threshVal, systMultFactor)

        if (len(matchList) != 0):
            with open(output_dir + '/' + dailyInfoList[i][0] + '.' + dailyInfoList[i][1] + '.' + str(
                    dailyInfoList[i][2]) + '.' + str(dailyInfoList[i][3]) + '_' + str(numThread),
                      'w') as outFile:
                for match in matchList:
                    print(match[0], match[1], match[2], match[3], file=outFile)

if __name__ == '__main__':
    controlParam = numpy.array(1, 'i')
    dateUniq = []

    #comm = MPI.COMM_WORLD
    nbrThreadMax = 0
    comm = MPI.Comm.Get_parent()  # Get informations on parent process
    numThread = comm.Get_rank()  # Get thread id
    numThreadMax = comm.Get_size()# Get amount of threads launched
    print(numThread)

    # Select GPU to work on according to thread id
    cp.cuda.runtime.setDevice(numThread)

    infoForThread = []

    # We get from master process needed informations
    infoForThread = comm.bcast(infoForThread, root=0)
    dateUniq      = infoForThread[0]
    length        = infoForThread[1]
    wfdir         = infoForThread[2]
    output_dir    = infoForThread[3]
    vecComp       = infoForThread[4]
    freqMin       = infoForThread[5]
    freqMax       = infoForThread[6]
    thresholdMeth = infoForThread[7]
    threshVal      = infoForThread[8]
    systMultFactor = infoForThread[9]
    decimate      = infoForThread[10]
    check         = infoForThread[11]

    #Get dates this thread have to work on
    dateUniqThread = getDates(dateUniq, numThread, numThreadMax)

    # Identify which pass this process have to compute according to thread id
    if numThreadMax > 2:
        passStep = ras.getPassStep(int(numThread/2), length)
    else:
        passStep = ras.getPassStep(numThread, length)    

    # Get the list of daily files to process for this thread
    quotiFilesE = []
    quotiFilesN = []
    quotiFilesZ = []

    fileList = os.listdir(wfdir) 

    fileListThread = []
    #Tri sur les dates
    if len(dateUniqThread)!=len(dateUniq) or check == True:
        for i in range(len(dateUniqThread)):
            fileListThread.extend([file for file in fileList if dateUniqThread[i] in file])

    else:
        fileListThread = fileList

    # Trouve tous les fichiers qui contiennent composante
    for i in range(len(vecComp)):
        if vecComp[i][-1:] == "E":
            quotiFilesE.extend([file for file in fileListThread if vecComp[i] in file])
        elif vecComp[i][-1:] == "N":
            quotiFilesN.extend([file for file in fileListThread if vecComp[i] in file])
        elif vecComp[i][-1:] == "Z":
            quotiFilesZ.extend([file for file in fileListThread if vecComp[i] in file])

    quotiFilesE.sort()
    quotiFilesN.sort()
    quotiFilesZ.sort()

    # Loop over daily file and process match
    beg = datetime.now()
    for i in range(len(vecComp)):
        if vecComp[i][-1:] == "E":
            alignComposant(wfdir, quotiFilesE, passStep, length, freqMin, freqMax, decimate, thresholdMeth, threshVal, systMultFactor)
        elif vecComp[i][-1:] == "N":
            alignComposant(wfdir, quotiFilesN, passStep, length, freqMin, freqMax, decimate, thresholdMeth, threshVal, systMultFactor)
        elif vecComp[i][-1:] == "Z":
            alignComposant(wfdir, quotiFilesZ, passStep, length, freqMin, freqMax, decimate, thresholdMeth, threshVal, systMultFactor)

    print('Runtime=', datetime.now() - beg)

    comm.Reduce([controlParam, MPI.INT], None, op=MPI.SUM, root=0)

    comm.Disconnect()
