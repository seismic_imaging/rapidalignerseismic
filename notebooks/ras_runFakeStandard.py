#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import os, sys
import numpy as np
import cupy as cp
from obspy import UTCDateTime, read, Trace, Stream
from mpi4py import MPI

def ParseArgs(loa):
    if len(loa) == 10:
        networkCode   = str(loa[1])
        stationCode   = str(loa[2])
        patternLength = int(loa[3])
        dailyNumber   = int(loa[4])
        patternNumber = int(loa[5])
        samplingRate  = int(loa[6])
        numGPU        = int(loa[7])
        templateDir   = str(loa[8])
        wfDir         = str(loa[9])
    else:
        print('USAGE: ras_FakeRun.py <network> <station> <template_length> <n_daily_files> <n_templates> <sampling_rate> <n_GPU> <path_to_template_dir> <path_to_daily_dir>')
        exit()

    return networkCode, stationCode, patternLength, dailyNumber, patternNumber, samplingRate, numGPU, templateDir, wfDir

#Function to generate dummy template files, MSEED format
def GeneratePatternFiles(templateDir, i, patternLength, networkCode, stationCode, chan, samplingRate):

    nptsPattern = patternLength * samplingRate

    # Create a vector of random floats
    patternRandArray = np.random.uniform(low=-1., high=1., size=nptsPattern)

    # Fill header attributes
    statsPattern = {'network': networkCode, 'station': stationCode, 'location': '',
                    'channel': chan, 'npts': len(patternRandArray), 'sampling_rate': samplingRate,
                    'mseed': {'dataquality': 'D'}}

    # set current time
    t0 = UTCDateTime(1985, 12, 28, 0, 0) + i * 24 * 3600
    statsPattern['starttime'] = t0
    jday = '{:03d}'.format(t0.julday)
    year = t0.year
    pattern = Stream([Trace(data=patternRandArray, header=statsPattern)])

    # write as MSEED file
    pattern.write(templateDir +'/'+ networkCode+'.'+stationCode+'.00.'+chan+'.D.'+str(year)+'.'+str(jday)+'.0000.00.00', format='MSEED')

#Function to generate dummy daily files, MSEED format
def GenerateDailyFiles(wfDir, j, networkCode, stationCode, chan, samplingRate):

    nptsDaily = 24 * 3600 * samplingRate
    dailyRandArray = np.random.uniform(low=-1., high=1., size=nptsDaily)

    # Fill header attributes
    statsDaily = {'network': networkCode, 'station': stationCode, 'location': '',
                  'channel': chan, 'npts': len(dailyRandArray), 'sampling_rate': samplingRate,
                  'mseed': {'dataquality': 'D'}}

    t0 = UTCDateTime(1985, 12, 28, 0, 0) + j * 24 * 3600
    statsDaily['starttime'] = t0
    jday = '{:03d}'.format(t0.julday)
    year = t0.year
    daily = Stream([Trace(data=dailyRandArray, header=statsDaily)])

    # write as MSEED file
    daily.write(wfDir +'/'+ networkCode+'.'+stationCode+'.00.'+chan+'.D.'+str(year)+'.'+str(jday), format='MSEED')

#Run the test standard version of rapidAlignerSeismic, i.e. a list of templates versus a list of daily files
def runStandardMatching_test(uniqDate, wfDir, templateDir, ouput_dir, vecComp, freqMin, freqMax, standMultFactor, decimate, numGPU):
    z = np.array(0, dtype='i')
    comm = MPI.COMM_WORLD

    # Find how many GPUs on the node
    numGPU_True = cp.cuda.runtime.getDeviceCount()


    if numGPU_True == 0:
        sys.exit("There is no available GPU on your current node! Stopping execution.")
    if numGPU > numGPU_True:
        sys.exit("You required more GPU than available on your system. Stopping execution.")
    else:
        nbrThreadMax = numGPU

    # Define the amount of MPI process according to the amount of CUDA devices available
    sizeDateProc = round(len(uniqDate) / nbrThreadMax)

    # Get index of the last file to consider per process
    endEx = []
    for i in range(1, nbrThreadMax):
        endEx.append(int(sizeDateProc * i))

    endEx.append(len(uniqDate))

    # Gather informations to broadcast to every MPI process
    info = [uniqDate, endEx, wfDir, templateDir, outputDir, vecComp, freqMin, freqMax, standMultFactor, decimate]

    # Parallel launch
    comm = MPI.COMM_SELF.Spawn(sys.executable,
                               args=['ras_runStandardMatching.py'],
                               maxprocs=int(nbrThreadMax))
    # Broadcast information to every MPI process
    comm.bcast(info, root=MPI.ROOT)

    # Get information from finished threads
    comm.Reduce(None, [z, MPI.INT], op=MPI.SUM, root=MPI.ROOT)
    print(z)

    # May induce errors: hangs execution on call to comm.Disconnect
    # If so comment the following line
    comm.Disconnect()





if __name__ == "__main__":

    chanList = ['HHE', 'HHN', 'HHZ']

    arg_lst = sys.argv
    networkCode, stationCode, patternLength, dailyNumber, patternNumber, samplingRate, numGPU, templateDir, wfDir = ParseArgs(arg_lst)

    decimate = False
    outputDir = 'test_res'
    freqMin = 2
    freqMax = 20
    standMultFactor = 20

    for patternNumber in range(1,41):

        if (os.path.isdir(templateDir) == True) or (os.path.isdir(wfDir) == True):
            #print('rm -rf '+templateDir+' '+wfDir+' '+outputDir)
            os.system('rm -rf '+templateDir+' '+wfDir+' '+outputDir)
        os.system('mkdir -p '+templateDir+' '+wfDir+' '+outputDir)

        # Create dummy templates
        for i in range(patternNumber):
            for chan in chanList:
                GeneratePatternFiles(templateDir, i, patternLength, networkCode, stationCode, chan, samplingRate)

        # Create dummy daily arrays
        for j in range(dailyNumber):
            for chan in chanList:
                GenerateDailyFiles(wfDir, j, networkCode, stationCode, chan, samplingRate)

        # Lists daily files in daily repository
        fileList = os.listdir(wfDir)

        ext = []
        # Find amount of days to process => In standard mode we do the parallelization of the work on days
        for file in fileList:
            ext.append(file[-3:])

        uniqDate = list(set(ext))
        uniqDate.sort()

        #Launch test version of runStandardMatching
        runStandardMatching_test(uniqDate, wfDir, templateDir, outputDir, chanList, freqMin, freqMax, standMultFactor, decimate, numGPU)


        # Clean Up
        os.system('rm -rf '+templateDir+' '+wfDir+' '+outputDir)
