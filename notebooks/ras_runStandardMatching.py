#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime, timedelta
from mpi4py import MPI
from obspy.core import UTCDateTime
import os, sys, time, math
import cupy as cp
import numpy as np
import matplotlib.pyplot as plt # This is only for debug

# This ensures to use the source code if not installed as Python package
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras


#Perform matching for each template over a given daily file
def doAlign(dailySig_gpu, templatelistComp_gpu, templatelistName, dailyInfo):
    #Loop over templates on a given component
    corrList = []
    for i in range(len(templatelistName)):
        if(dailyInfo[0] == templatelistName[i][0]):
            # Perform template matching
            dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySig_gpu, mode="fft")
            #Perform a CPU copy of correlation values to avoid memory issues. Comment this line and uncomment the next one for faster performances
            #corrList.append(cp.asnumpy(corr))
            corrList.append(corr)

            # DEBUG Check corr see if everything's fine
            #plt.plot(cp.asnumpy(corr) / len(templatelistComp_gpu[i]))
            #plt.ylim([-1, 1])
            #plt.show()

    return corrList

def MatchFinder(corrStack, threshold, year, jday, templateLen, dailySr):

    # DEBUG Check corrStack see if everything's fine
    #plt.plot(cp.asnumpy(corrStack)/ templateLen)
    #plt.axhline(y=threshold, color='red', linestyle='--')
    #plt.ylim([-1, 1])
    #plt.title(templateName)
    #plt.show()

    # Get the index of the cc values above threshold
    idxList = cp.argwhere(corrStack / templateLen > threshold)

    matchIdxList, matchCorrList = [], []
    for i in range(len(idxList)):
        if corrStack[idxList[i][0]] > corrStack[idxList[i][0]+1] and corrStack[idxList[i][0]] > corrStack[idxList[i][0]-1]:
            matchIdxList.append(idxList[i][0])
            matchCorrList.append(float(corrStack[idxList[i][0]] / templateLen))

    # Get the UTCDateTime corresponding to each match
    matchTimeList = [UTCDateTime(year=year, julday=jday) + float(idx / dailySr) for idx in matchIdxList]

    return [matchTimeList, matchCorrList] 

def AlignOneComp(chan, vecComp, dailySig_gpu, templateSigList_gpu, templatesNameList, dailyInfo):
    if chan == vecComp[0]:
        corr1 = doAlign(dailySig_gpu, templateSigList_gpu[0], templatesNameList[0], dailyInfo)

    return corr1

def AlignTwoComp(chan, vecComp, dailySig_gpu, templateSigList_gpu, templatesNameList, dailyInfo):
    if chan == vecComp[0]:
        corr1 = doAlign(dailySig_gpu, templateSigList_gpu[0], templatesNameList[0], dailyInfo)
    elif chan == vecComp[1]:
        corr2 = doAlign(dailySig_gpu, templateSigList_gpu[1], templatesNameList[1], dailyInfo)

    return corr1, corr2


def AlignThreeComp(chan, vecComp, dailySig_gpu, templateSigList_gpu, templatesNameList, dailyInfo):
    if chan == vecComp[0]:
        corr1 = doAlign(dailySig_gpu, templateSigList_gpu[0], templatesNameList[0], dailyInfo)
    elif chan == vecComp[1]:
        corr2 = doAlign(dailySig_gpu, templateSigList_gpu[1], templatesNameList[1], dailyInfo)
    elif chan == vecComp[2]:
        corr3 = doAlign(dailySig_gpu, templateSigList_gpu[2], templatesNameList[2], dailyInfo)

    return corr1, corr2, corr3


if __name__ == '__main__':
    controlParam = np.array(1, 'i')
    comm = MPI.COMM_WORLD
    nbrThreadMax=0
    comm = MPI.Comm.Get_parent() #Get informations on parent process
    numThread = comm.Get_rank() #Get thread id
    #print(numThread)

    #Select GPU to work on according to thread id
    cp.cuda.runtime.setDevice(numThread)

    #BP: memory control
    #mempool = cp.get_default_memory_pool()

    #We get from master process needed informations
    dateUniq, stopIndex, infoForThread = [], [], []
    infoForThread  = comm.bcast(infoForThread, root=0)
    dateUniq       = infoForThread[0]
    stopIndex      = infoForThread[1]
    wfdir          = infoForThread[2]
    template_dir   = infoForThread[3]
    output_dir     = infoForThread[4]
    vecComp        = infoForThread[5]
    freqMin        = infoForThread[6]
    freqMax        = infoForThread[7]
    flagWriteCC    = infoForThread[8]
    thresholdMeth  = infoForThread[9]
    threshVal       = infoForThread[10]
    standMultFactor = infoForThread[11]
    decimate       = infoForThread[12]
    batchSize      = infoForThread[13]

    #assert len(vecComp) == 3
    if len(vecComp) > 3:
        print("Error, too many components for vecComp\n Please modify your configuration file and relaunch\n")
        sys.exit(2)

    beginIndex    = stopIndex[0] * numThread

    #Get template matrix
    templateSigList_gpu = []
    templatesNameList   = []
    #templateSigList_gpu, templatesNameList = ras.getTemplateList(template_dir, vecComp, freqMin, freqMax, decimate)
    # DEBUG: See what we got
    #print(templatesNameList)
    #print(templateSigList_gpu)

    #Get the list of daily files to process for this thread
    quotiFiles = []
    fileList = os.listdir(wfdir)
    for i in range(beginIndex, stopIndex[numThread]):
        date = dateUniq[i]
        #Trouve tous les fichiers qui contiennent date
        quotiFiles.extend([file for file in fileList if date in file])

    templateList = os.listdir(template_dir)

    print("templatelist:", len(templateList))

    dateList=[]
    for template in templateList:
        dateList.append(".".join(template.split('.')[:2]+template.split('.')[4:]))
    uniqDate = list(set(dateList))

    print("uniqDate :", len(uniqDate))

    idBeg = 0
    if len(uniqDate) >= batchSize:
        idEnd = batchSize
    else:
        idEnd = len(uniqDate)

    beg = datetime.now()
    timeTot = timedelta(seconds=0)
    for j in range(math.ceil(len(uniqDate)/batchSize)):

        listTempl = []
        for k in range(idBeg, idEnd):
            templateComp = [template for template in templateList if(".".join(template.split('.')[:2]) in ".".join(uniqDate[k].split('.')[:2]) and ".".join(template.split('.')[4:]) in ".".join(uniqDate[k].split('.')[2:]))]
            listTempl.extend(templateComp)
        print(len(listTempl))        

        # Get template matrix
        templateSigList_gpu = []
        templatesNameList = []
        templateSigList_gpu, templatesNameList = ras.getTemplateList(listTempl, vecComp, freqMin, freqMax, decimate, template_dir)

        print("templateSig :", len(templateSigList_gpu[0]))


        #Loop over daily file and process match
        iCount=0

        for daily in quotiFiles:
            iCount = iCount + 1
            ntwkSt, chan, year, jday = ras.getDailyInfo(daily)
            dailySig, dailySr, dailyTbeg, dailyTend = ras.util.DailyLoader(wfdir +'/'+ daily, freqMin, freqMax, decimate).data
            dailySig_gpu = cp.asarray(dailySig)
            dailyInfo = [ntwkSt, chan, jday, year]

            #print(daily, mempool.used_bytes())

            tic=datetime.now()
            if chan == vecComp[0]:
                # print(dailyInfo)
                corrE = doAlign(dailySig_gpu, templateSigList_gpu[0], templatesNameList[0], dailyInfo)
            elif chan == vecComp[1]:
                # print(dailyInfo)
                corrN = doAlign(dailySig_gpu, templateSigList_gpu[1], templatesNameList[1], dailyInfo)
            elif chan == vecComp[2]:
                # print(dailyInfo)
                corrZ = doAlign(dailySig_gpu, templateSigList_gpu[2], templatesNameList[2], dailyInfo)

#            if len(vecComp) == 1:
#                corr1 = AlignOneComp(dailySig_gpu,templateSigList_gpu,templatesNameList,dailyInfo)
#            elif len(vecComp) == 2:
#                corr1, corr2 = AlignTwoComp(dailySig_gpu,templateSigList_gpu,templatesNameList,dailyInfo)
#            elif len(vecComp) == 3:
#                corr1, corr2, corr3 = AlignThreeComp(dailySig_gpu,templateSigList_gpu,templatesNameList,dailyInfo)


            toc=datetime.now()
            timeTot += toc-tic

            if iCount % len(vecComp) == 0:
                print('Thread=', numThread, 'Processing:', dailyInfo[0], year, dailyInfo[2])
                for i in range(len(templatesNameList[0])):

                    templateName = templatesNameList[0][i][0]+'.'+templatesNameList[0][i][1]

                    # stack three component cc functions
                    #if len(vecComp) == 1:
                    #    stack_gpu = corr1[i]
                    #elif len(vecComp) ==2:
                    #    stack_gpu = (corr1[i] + corr2[i] ) / 2
                    #else:
                    #    stack_gpu = (corr1[i] + corr2[i] + corr3[i]) / 3
                    stack_gpu = (corrE[i] + corrN[i] + corrZ[i]) / 3

                    # Get template length
                    templateLen = len(cp.asnumpy(templateSigList_gpu[0][0]))

                    if flagWriteCC:
                        cp.save(output_dir + '/corr.' + templateName + '.' + dailyInfo[0] + '.' + dailyInfo[1] + '.' +
                               str(year) + '.' + dailyInfo[2]  + '.npy', stack_gpu/templateLen)
                    else:
                        # Get template length
                        #templateLen = len(cp.asnumpy(templateSigList_gpu[0][0]))

                        # stack_gpu = cp.asarray(stack)

                        # Compute threshold for each dailySig
                        if (thresholdMeth == "RMS"):
                            threshold = ras.getThreshold(stack_gpu, templateLen, standMultFactor)
                        else:
                            threshold = threshVal

                        # Extract matches
                        matchList = MatchFinder(stack_gpu, threshold, int(year), int(jday), templateLen, dailySr)

                        # Export matches to disk
                        matchOutFile = open(
                            output_dir + '/' + templateName + '.' + dailyInfo[0] + '.' + year + '.' + dailyInfo[2], 'w')
                        for i in range(len(matchList[0])):
                            print(matchList[0][i], '{:.3f}'.format(matchList[1][i]), file=matchOutFile)
                        matchOutFile.close()

                        # remove empty files
                        if os.path.getsize(
                                output_dir + '/' + templateName + '.' + dailyInfo[0] + '.' + year + '.' + dailyInfo[
                                    2]) == 0:
                            os.system(
                                'rm -rf ' + output_dir + '/' + templateName + '.' + dailyInfo[0] + '.' + year + '.' +
                                dailyInfo[2])


                #Just to make sure to release memory
                corrE=[]
                corrN=[]
                corrZ=[]

        idBeg = idBeg + batchSize
        idEnd = idEnd + batchSize
        if idEnd > len(uniqDate):
            idEnd = len(uniqDate)

    print('Runtime=', datetime.now() - beg)
    print('Alignment Time', numThread, timeTot)

    comm.Reduce([controlParam, MPI.INT], None, op=MPI.SUM, root=0)
    comm.Disconnect()
