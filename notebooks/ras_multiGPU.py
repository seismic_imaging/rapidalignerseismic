#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime
from mpi4py import MPI
import numpy as np
import os
import sys
import cupy as cp
import getopt

try:
    import rapidAlignerSeismic
except:
    import os, sys
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras

#Define default values of parameters
#################################################################################################################
mode = "Standard"
#Repository containing data
wfdir = "../data/wf"
templateDir = "../data/templates"

#Repository to write results
output_dir = '../data/hits'

# Path to config File
configFile = './ras_config.par'

#Checkpoint to restart from last computed file in case of crash
check = False
#################################################################################################################
def readConfigFile(configFile):
    global wfdir, templateDir, output_dir

    with open(configFile, 'r') as f:
        for line in f:
            rLine="".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'length':
                    length = eval(rLine[1])
                    assert type(length) is float or int
                elif rLine[0] == 'vecComp':
                    vecComp = eval(rLine[1])
                    assert type(vecComp) is list
                elif rLine[0] == 'freqMin':
                    freqMin = eval(rLine[1])
                    assert type(freqMin) is float or int
                elif rLine[0] == 'freqMax':
                    freqMax = eval(rLine[1])
                    assert type(freqMax) is float or int
                elif rLine[0] == 'writeCC':
                    flagWriteCC = eval(rLine[1])
                    assert type(flagWriteCC) is bool
                elif rLine[0] == 'thresholdMethod':
                    thresholdMeth  = eval(rLine[1])
                elif rLine[0] == 'threshVal':
                    threshVal = eval(rLine[1])
                    assert type(threshVal) is float
                elif rLine[0] == 'systMultFactor':
                    systMultFactor = eval(rLine[1])
                    assert type(systMultFactor) is float or int
                elif rLine[0] == 'standMultFactor':
                    standMultFactor = eval(rLine[1])
                    assert type(systMultFactor) is float or int
                elif rLine[0] == 'decimate':
                    decimate = eval(rLine[1])
                    assert type(decimate) is bool
                elif rLine[0] == 'batch':
                    batch_size = eval(rLine[1])
                    assert type(batch_size) is int
                elif rLine[0] == 'dailyDir':
                    if rLine[1] != '' :
                        wfdir = eval(rLine[1])
                elif rLine[0] == 'templateDir':
                    if rLine[1] != '':
                        templateDir = eval(rLine[1])
                elif rLine[0] == 'outputDir':
                    if rLine[1] != '':
                        output_dir = eval(rLine[1])

    return length, vecComp, freqMin, freqMax, flagWriteCC, thresholdMeth, threshVal, systMultFactor, standMultFactor, decimate, batch_size

def readPNExtract(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine = "".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'phaseNetFile':
                    phaseNetFileName = eval(rLine[1])
                elif rLine[0] == 'eventsDir':
                    concatEventsDir = eval(rLine[1])
                elif rLine[0] == 'pnDir':
                    pnEventDir = eval(rLine[1])
                elif rLine[0] == 'extractEvents':
                    doExtractEvents = eval(rLine[1])
                    assert type(doExtractEvents) is bool
                elif rLine[0] == 'shift':
                    t0Shift = eval(rLine[1])
                elif rLine[0] == 'dailyLimit':
                    userDailyLimit = eval(rLine[1])

    return phaseNetFileName, concatEventsDir, pnEventDir, doExtractEvents, t0Shift, userDailyLimit



#Run the standard version of rapidAlignerSeismic, i.e. a list of templates versus a list of daily files
def runStandardMatching(uniqDate, wfdir, templateDir, output_dir, vecComp, freqMin, freqMax,  flagWriteCC, thresholdMeth, threshVal, standMultFactor, decimate, batchSize):
    z = np.array(0, dtype='i')
    comm = MPI.COMM_WORLD

    # Find how many GPUs on the node
    numGPU = cp.cuda.runtime.getDeviceCount()

    if numGPU == 0:
        sys.exit("There is no available GPU on your current node! Stopping execution.")
    else:
        nbrThreadMax = numGPU

    # Define the amount of MPI process according to the amount of CUDA devices available
    sizeDateProc = round(len(uniqDate) / nbrThreadMax)

    # Get index of the last file to consider per process
    endEx = []
    for i in range(1, nbrThreadMax):
        endEx.append(int(sizeDateProc * i))

    endEx.append(len(uniqDate))

    # Gather informations to broadcast to every MPI process
    info = [uniqDate, endEx, wfdir, templateDir, output_dir, vecComp, freqMin, freqMax, flagWriteCC, thresholdMeth, threshVal, standMultFactor, decimate, batchSize]

    # Parallel launch
    comm = MPI.COMM_SELF.Spawn(sys.executable,
                               args=['ras_runStandardMatching.py'],
                               maxprocs=int(nbrThreadMax))
    # Broadcast information to every MPI process
    comm.bcast(info, root=MPI.ROOT)

    # Get information from finished threads
    comm.Reduce(None, [z, MPI.INT], op=MPI.SUM, root=MPI.ROOT)
    print(z)

    # May induce errors: hangs execution on call to comm.Disconnect
    # If so comment the following line
    comm.Disconnect()

#Run the automatic search of the code
def runSelf(uniqDate, length, wfdir, output_dir):
    z = np.array(0, dtype='i')
    comm = MPI.COMM_WORLD

    # Find how many GPUs on the node
    numGPU = cp.cuda.runtime.getDeviceCount()

    if numGPU < 2:
        sys.exit("There is no available or not enough GPUs on your current node! You need 2 GPUs for this mode. Stopping execution.")
    else:
        #We need only 2 GPUs for this mode
        nbrThreadMax = 2

    info = [uniqDate, length, wfdir, output_dir]

    comm = MPI.COMM_SELF.Spawn(sys.executable,
                               args=['ras_runSelfSearch.py'],
                               maxprocs=int(nbrThreadMax))

    # Broadcast information to every MPI process
    comm.bcast(info, root=MPI.ROOT)

    # Get information from finished threads
    comm.Reduce(None, [z, MPI.INT], op=MPI.SUM, root=MPI.ROOT)
    print(z)

    # May induce errors: hangs execution on call to comm.Disconnect
    # If so comment the following line
    comm.Disconnect()

#Run the systematic
def runSystematic(uniqDate, length, wfdir, output_dir, vecComp, freqMin, freqMax, thresholdMeth, threshVal, systMultFactor, decimate):
    z = np.array(0, dtype='i')
    comm = MPI.COMM_WORLD

    # Find how many GPUs on the node
    numGPU = cp.cuda.runtime.getDeviceCount()

    if numGPU < 1:
        sys.exit(
            "There is no available GPUs on your current node! Stopping execution.")
    elif numGPU < 2:
        nbrThreadMax = 1
    else:
        # We need at least 2 GPUs for this mode
        #nbrThreadMax = 2
        nbrThreadMax = int(numGPU/2)*2

    info = [uniqDate, length, wfdir, output_dir, vecComp, freqMin, freqMax, thresholdMeth, threshVal, systMultFactor, decimate, check]

    comm = MPI.COMM_SELF.Spawn(sys.executable,
                               args=['ras_runSystematicSearch.py'],
                               maxprocs=int(nbrThreadMax))

    # Broadcast information to every MPI process
    comm.bcast(info, root=MPI.ROOT)

    # Get information from finished threads
    comm.Reduce(None, [z, MPI.INT], op=MPI.SUM, root=MPI.ROOT)
    print(z)

    # May induce errors: hangs execution on call to comm.Disconnect
    # If so comment the following line
    comm.Disconnect()

#
def runPNAlign(length, configFile, wfdir,output_dir,vecComp, freqMin, freqMax, systMultFactor):
    #First get extra infos specific for Phasenet mode
    phaseNetFileName, concatEventsDir, pnEventsDir, doExtractEvents, t0Shift, userDailyLimit = readPNExtract(configFile)

    # Find how many GPUs on the node
    numGPU = cp.cuda.runtime.getDeviceCount()

    if numGPU < 1:
        sys.exit("There is no available or not enough GPUs on your current node! You need at least 1 GPU for this mode. Stopping execution.")
    else:
        # We need at least 1 GPU for this mode
        nbrThreadMax = int(numGPU)

    # Extract the events from the daily files based on PhaseNet detection if it is not already done
    if doExtractEvents:
        # First check if the output repository exist or not and create it if needed
        if not os.path.isdir(concatEventsDir):
            os.mkdir(concatEventsDir)

        #Perform extraction
        ras.extractPN(phaseNetFileName, concatEventsDir, length, t0Shift, userDailyLimit, wfdir,vecComp, pnEventsDir,freqMin, freqMax)

    # Lists concatenated files in events repository
    fileList = os.listdir(concatEventsDir)

    stat = []
    #Make a list of stations from file name
    for file in fileList:
        stat.append('.'.join(file.split(".")[:2]))

    #Get list of stations
    uniqStat = list(set(stat))

    nbrThreadMax = min(len(uniqStat), int(numGPU))

    # Second  align events
    z = np.array(0, dtype='i')
    comm = MPI.COMM_WORLD

    info = [uniqStat, length, concatEventsDir, output_dir, vecComp, freqMin, freqMax, systMultFactor, decimate, check]

    comm = MPI.COMM_SELF.Spawn(sys.executable,
                               args=['ras_runPNSearch.py'],
                               maxprocs=int(nbrThreadMax))

    # Broadcast information to every MPI process
    comm.bcast(info, root=MPI.ROOT)

    # Get information from finished threads
    comm.Reduce(None, [z, MPI.INT], op=MPI.SUM, root=MPI.ROOT)
    print(z)

    # May induce errors: hangs execution on call to comm.Disconnect
    # If so comment the following line
    comm.Disconnect()




#Print the list of the options of the script and the details then exit
def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print("-h  | --help						Print the usage message")
    print("-m  | --mode <mode>	Name the mode to run the code, must be Standard, Self, Systematic or PhaseNet. By default '%s'"%(mode))
    print("-r  | --ras_config <config_file> To use a specific configuration file. By default '%s'"%(configFile))
    print("-c  | --checkpoint  To restart the job from the last computed file in case of crash")
    sys.exit()

if __name__ == "__main__":
    #Configuration
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hm:r:c",["help","mode=","ras_config=","checkpoint"])
    except getopt.GetoptError:
        print(sys.argv[0]+' -m <Mode> -r <configFile> -c')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h","--help"):
            printUsageAndExit(sys.argv[0])
        elif opt in ("-m", "--mode"):
            mode = arg
        elif opt in ("-r", "--ras_config"):
            configFile = arg
        elif opt in ("-c","--checkpoint"):
            check = True

    # Read parameters from configuration file
    length, vecComp, freqMin, freqMax, flagWriteCC, thresholdMeth, threshVal, systMultFactor, standMultFactor, decimate, batchSize = readConfigFile(configFile)


    #check if the ouput dir already exist, if not creates it
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    #Lists daily files in daily repository
    fileList = os.listdir(wfdir)

    #If checkpoint mode is activated clean the fileList from daily file already processed
    if check:
        fileList = ras.stopAndRestart(fileList, output_dir)

    ext = []
    #Find amount of days to process => In standard mode we do the parallelization of the work on days
    for file in fileList:
        ext.append(file[-8:])

    uniqDate = list(set(ext))
    uniqDate.sort()

    # Known template on files
    if mode == "Standard":
        runStandardMatching(uniqDate, wfdir, templateDir, output_dir, vecComp, freqMin, freqMax, flagWriteCC, thresholdMeth, threshVal, standMultFactor, decimate, batchSize)
    # Look for template on a given file
    elif mode == "Self":
        runSelf(uniqDate, length, wfdir, output_dir)
    #Perform systematic template identification on a list of files
    elif mode == "Systematic":
        runSystematic(uniqDate, length, wfdir, output_dir, vecComp, freqMin, freqMax, thresholdMeth, threshVal, systMultFactor, decimate)
    #Perform systematic template identification on events pre-identified by PhaseNet
    elif mode == "PhaseNet":
        runPNAlign(length, configFile, wfdir, output_dir, vecComp,freqMin, freqMax, systMultFactor)


    #May induce errors: hangs execution on call to comm.Disconnect
    #If so comment the following line
    #comm.Disconnect()
