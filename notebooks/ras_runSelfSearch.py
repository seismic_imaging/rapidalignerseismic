#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime
from mpi4py import MPI
import numpy
import os
import cupy as cp
import sys, time
#import matplotlib.pyplot as plt

# This ensures to use the source code if not installed as Python package
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras

#Perform matching for a given daily file over each template
def doAutoAlign(dailySig_gpu, templatelistComp_gpu, year, jday, dailySr, length, passStep):
    matchList = []

    #Loop over templates on a given component
    for i in ras.progressbar(range(len(templatelistComp_gpu)), 'Computing: ', 40):

        templateLen = len(templatelistComp_gpu[i])

        # Get the idx of the autocorrelation and mask the peak
        idx_AutoCorr = int(i * (length * dailySr) + (passStep * dailySr))

        # Perform patern matching
        # Using Rolling mean- and amplitude-adjusted Euclidean Distance using FFT 
        dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySig_gpu[idx_AutoCorr:], mode="fft")

        # Filter best matches
        # Dynamic treshold based on mean average deviation (MAD)
        mad = cp.mean(cp.absolute(corr - cp.median(corr)))
        treshold = float((mad * 20) / templateLen)

        # Mask the autocorrelation peak
        corr[0:templateLen / 2] = 0.


        # Get max value
        maxCorrVal = int(cp.amax(corr))
        if (maxCorrVal / templateLen) > treshold:
            idxMaxCorr  = int(cp.argmax(corr))
            maxCorrTime = dailyTbeg + (idx_AutoCorr + idxMaxCorr) / dailySr
            patternTime = dailyTbeg + idx_AutoCorr / dailySr
            
            match = [numThread, patternTime, maxCorrTime, '{:.6f}'.format(maxCorrVal / templateLen)] 
            matchList.append(match)

        else:
            continue

        # DEBUG  Plot stuff to make sure everything is alright ########################
        #corr_cpu = cp.asnumpy(corr)
        
        # DEBUG  Plot CC function and threshold
        #plt.plot(corr_cpu / templateLen)
        #plt.axhline(y = treshold, color = 'r', linestyle = '--')
        #plt.title(str(numThread)+'_window_'+str(i))
        #plt.show()

        # DEBUG  Plot Pattern and Daily when tney match
        #templ_cpu = cp.asnumpy(templatelistComp_gpu[i])
        #daily_cpu = cp.asnumpy(dailySig_gpu[idx_AutoCorr + idxMaxCorr:idx_AutoCorr + idxMaxCorr + templateLen])
        #plt.plot(templ_cpu, label='Pattern')
        #plt.plot(daily_cpu, label='Daily')
        #plt.title('GPU='+str(numThread)+' window='+str(i)+' CC='+str(maxCorrVal/templateLen))
        #plt.legend()
        #plt.show()
        ########################################################################
    return matchList




################################################################################
if __name__ == '__main__':
    controlParam = numpy.array(1, 'i')
    dateUniq = []

    comm = MPI.COMM_WORLD
    nbrThreadMax = 0
    comm = MPI.Comm.Get_parent()  # Get informations on parent process
    numThread = comm.Get_rank()  # Get thread id
    print(numThread)

    # Select GPU to work on according to thread id
    cp.cuda.runtime.setDevice(numThread)

    infoForThread = []

    # We get from master process needed informations
    infoForThread = comm.bcast(infoForThread, root=0)
    dateUniq = infoForThread[0]
    length = infoForThread[1]
    wfdir = infoForThread[2]
    output_dir = infoForThread[3]

    #Identify which pass this process have to compute according to thread id
    passStep = ras.getPassStep(numThread, length)

    # Get the list of daily files to process for this thread
    quotiFiles = []
    fileList = os.listdir(wfdir)
    for i in range(len(dateUniq)):
        date = dateUniq[i]
        # Trouve tous les fichiers qui contiennent date
        quotiFiles.extend([file for file in fileList if dateUniq[i] in file])

    quotiFiles.sort()

    # Loop over daily file and process match
    beg = datetime.now()
    for daily in quotiFiles:
        ntwkSt, chan, year, jday = ras.getDailyInfo(daily)
        dailySig, dailySr, dailyTbeg, dailyTend = ras.util.DailyLoader(wfdir +'/'+ daily).data
        dailySig_gpu = cp.asarray(dailySig)

        # From the daily file generate the array of templates
        templateSigList_gpu = ras.TrimDaily2Templates(dailySig, length, dailySr, dailyTbeg, dailyTend, passStep)
        matchList = doAutoAlign(dailySig_gpu, templateSigList_gpu, year, jday, dailySr, length, passStep)

        with open(output_dir+'/'+ntwkSt+'.'+chan+'.'+str(year)+'.'+str(jday)+'_'+str(numThread), 'w') as outFile:
            for match in matchList:
                print(match[0], match[1], match[2], match[3], file=outFile)
    
    print('runtime=', datetime.now() - beg)

    comm.Reduce([controlParam, MPI.INT], None, op=MPI.SUM, root=0)
    comm.Disconnect()
