#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime
from mpi4py import MPI
import numpy
import os
import cupy as cp
import sys, time

# For debug Only
import matplotlib.pyplot as plt

# This ensures to use the source code if not installed as Python package
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras


def getStations(uniqStat, numThread, numThreadMax):

    stations = []
    sizeStationProc = int(len(uniqStat) / numThreadMax)
    begEx = numThread * sizeStationProc
    if numThread != numThreadMax-1:
        endEx = (numThread + 1)*sizeStationProc
    else:
        endEx = len(uniqStat) -1

    for i in range(begEx,endEx):
        stations.append(uniqStat[i])

    return stations

# Perform matching for a given daily file over each template
def doSystematicAlign(dailySig_gpu, templatelistComp_gpu, dailySigList_gpu, dailyInfoList, length, idBeg):
    matchList = []

    ntwkSt    = dailyInfoList[idBeg][0]
    dailySr   = int(dailyInfoList[idBeg][4])
    Jday      = dailyInfoList[idBeg][3]

    for i in range(len(templatelistComp_gpu)):
        templateLen = len(templatelistComp_gpu[i])

        # Get the idx of the autocorrelation and mask the peak
        idxAutoCorr = int(i * (length * dailySr))

        # Perform patern matching
        # Using Rolling mean- and amplitude-adjusted Euclidean Distance using FFT
        dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySig_gpu[idxAutoCorr:], mode="fft")

        # Mask the autocorrelation peak
        corr[0:templateLen] = 0.

        # Filter best matches
        match = ras.getBestMatchPN(corr, templateLen, Jday, Jday, i, i, numThread, systMultFactor)

        # DEBUG Plot pattern and Daily to to check that there's no monkey business
        #treshold = ras.getTreshold(corr, templateLen, systMultFactor)
        #maxCorrVal = int(cp.amax(corr))
        #if (maxCorrVal / templateLen) > treshold and (maxCorrVal / templateLen) < 0.98:
        #    idxMaxCorr = int(cp.argmax(corr))
        #    maxCorrTime = dailyTbeg + (idxAutoCorr + idxMaxCorr) / dailySr
        #    patternTime = dailyTbeg + idxAutoCorr / dailySr
        #    plt.plot(cp.asnumpy(templatelistComp_gpu[i]))
        #    plt.plot(cp.asnumpy(dailySig_gpu[idxAutoCorr+idxMaxCorr:idxAutoCorr+idxMaxCorr+templateLen]))
        #    plt.show()

        if len(match) != 0:
            matchList.append(match)

        #While match is empty we carry on with the same template on other daily files from the same station
        if len(match) == 0 and idBeg != len(dailySigList_gpu)-1:
            for j in range(1,len(dailySigList_gpu[idBeg:])) :
                if dailyInfoList[idBeg + j][0] == ntwkSt :
                    dailySrNext = dailyInfoList[idBeg + j][4]
                    nextJday = dailyInfoList[idBeg + j][3]

                    #Compute match Perform patern matching
                    # Using Rolling mean- and amplitude-adjusted Euclidean Distance using FFT
                    dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySigList_gpu[idBeg+j], mode="fft")

                    # Filter best matches
                    match = ras.getBestMatchPN(corr, templateLen, Jday, nextJday, i, j, numThread, systMultFactor)

                    # DEBUG Plot pattern and Daily to check that there's no monkey business
                    #treshold = ras.getTreshold(corr, templateLen, systMultFactor)
                    #maxCorrVal = int(cp.amax(corr))
                    #if (maxCorrVal / templateLen) > treshold:
                    #    idxMaxCorr = int(cp.argmax(corr))
                    #    maxCorrTime = dailyNextTbeg + (0 + idxMaxCorr) / dailySr
                    #    patternTime = dailyTbeg + idxAutoCorr / dailySr
                    #    print(patternTime, maxCorrTime)
                    #    plt.plot(cp.asnumpy(templatelistComp_gpu[i]))
                    #    plt.plot(cp.asnumpy(dailySigList_gpu[idBeg+j][idxMaxCorr:idxMaxCorr+templateLen]))
                    #    plt.show()

                #If a match is found we stop the search for this template
                if len(match) != 0:
                    matchList.append(match)
                    break
    return matchList

def alignComposant(wfdir, quotiFilesComp, length, freqMin, freqMax, decimate):
    # Load daily file list for the given component
    concatSigList, concatInfoList = ras.getConcatList(wfdir, quotiFilesComp)
    concatSigList_gpu = cp.asarray(concatSigList)
    matchList = []
    for i in range(len(concatSigList)):
        print('Thread=', numThread, 'Processing:', concatInfoList[i][0] + '.' + concatInfoList[i][1] +
              '.' + str(concatInfoList[i][2]) + '.' + str(concatInfoList[i][3]))

        # From the daily file generate the array of templates
        templateSigList_gpu = ras.TrimConcat2Templates(concatSigList[i], length, concatInfoList[i][4])
        matchList = doSystematicAlign(concatSigList_gpu[i], templateSigList_gpu, concatSigList_gpu, concatInfoList, length,
                                      i)

        if (len(matchList) != 0):
            with open(output_dir + '/' + concatInfoList[i][0] + '.' + concatInfoList[i][1] + '.' + str(
                    concatInfoList[i][2]) + '.' + str(concatInfoList[i][3]) + '_' + str(numThread),
                      'w') as outFile:
                for match in matchList:
                    print(match[0], match[1], match[2], match[3], match[4], file=outFile)


if __name__ == '__main__':
    controlParam = numpy.array(1, 'i')

    comm = MPI.COMM_WORLD
    nbrThreadMax = 0
    comm = MPI.Comm.Get_parent()  # Get informations on parent process
    numThread = comm.Get_rank()  # Get thread id
    numThreadMax = comm.Get_size()# Get amount of threads launched
    print(numThread)

    # Select GPU to work on according to thread id
    cp.cuda.runtime.setDevice(numThread)

    infoForThread = []

    # We get from master process needed informations
    infoForThread = comm.bcast(infoForThread, root=0)
    uniqStat = infoForThread[0]
    length = infoForThread[1]
    concat_dir = infoForThread[2]
    output_dir = infoForThread[3]
    vecComp = infoForThread[4]
    freqMin = infoForThread[5]
    freqMax = infoForThread[6]
    systMultFactor = infoForThread[7]
    decimate = infoForThread[8]
    check = infoForThread[9]

    #Get the stations this thread have to process
    statUniqThread = getStations(uniqStat, numThread, numThreadMax)

    # Get the list of stations files to process for this thread
    statFilesE = []
    statFilesN = []
    statFilesZ = []

    fileList = os.listdir(concat_dir)

    fileListThread = []
    #Tri sur les stations
    if len(statUniqThread)!=len(uniqStat) or check == True:
        for i in range(len(statUniqThread)):
            fileListThread.extend([file for file in fileList if statUniqThread[i] in file])
        else:
            fileListThread = fileList

    # Trouve tous les fichiers qui contiennent composante
    for i in range(len(vecComp)):
        if vecComp[i][-1:] == "E":
            statFilesE.extend([file for file in fileListThread if vecComp[i] in file])
        elif vecComp[i][-1:] == "N":
            statFilesN.extend([file for file in fileListThread if vecComp[i] in file])
        elif vecComp[i][-1:] == "Z":
            statFilesZ.extend([file for file in fileListThread if vecComp[i] in file])


    statFilesE.sort()
    statFilesN.sort()
    statFilesZ.sort()

    # Loop over daily file and process match
    beg = datetime.now()
    for i in range(len(vecComp)):
        if vecComp[i][-1:] == "E":
            alignComposant(concat_dir, statFilesE, length, freqMin, freqMax, decimate)
        elif vecComp[i][-1:] == "N":
            alignComposant(concat_dir, statFilesN, length, freqMin, freqMax, decimate)
        elif vecComp[i][-1:] == "Z":
            alignComposant(concat_dir, statFilesZ, length, freqMin, freqMax, decimate)

    print('Runtime=', datetime.now() - beg)

    comm.Reduce([controlParam, MPI.INT], None, op=MPI.SUM, root=0)

    comm.Disconnect()