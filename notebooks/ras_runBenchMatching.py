#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
#         Based on: rapidAligner, Christian Hundt, NVIDIA
#
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================


#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
from datetime import datetime, timedelta
from mpi4py import MPI
from obspy.core import UTCDateTime
import os, sys, time
import cupy as cp
import numpy as np
import matplotlib.pyplot as plt # This is only for debug
# This ensures to use the source code if not installed as Python package
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras


#Perform matching for each template over a given daily file
def doAlign(dailySig_gpu, templatelistComp_gpu, templatelistName, dailyInfo, nTempBench):
    #Loop over templates on a given component
    corrList = []
    for i in range(nTempBench):
        if(dailyInfo[0] == templatelistName[i][0]):
            # Perform template matching
            dist, corr = ras.ED.zdist(templatelistComp_gpu[i], dailySig_gpu, mode="fft")
            corrList.append(corr)

            # DEBUG Check corr see if everything's fine
            #plt.plot(cp.asnumpy(corr) / len(templatelistComp_gpu[i]))
            #plt.ylim([-1, 1])
            #plt.show()

    return corrList

def MatchFinder(corrStack, treshold, year, jday, templateLen, dailySr):

    # DEBUG Check corrStack see if everything's fine
    #plt.plot(cp.asnumpy(corrStack)/ templateLen)
    #plt.axhline(y=treshold, color='red', linestyle='--')
    #plt.ylim([-1, 1])
    #plt.title(templateName)
    #plt.show()

    # Get the index of the cc values above treshold
    idxList = cp.argwhere(corrStack / templateLen > treshold)

    matchIdxList, matchCorrList = [], []
    for i in range(len(idxList)):
        if corrStack[idxList[i][0]] > corrStack[idxList[i][0]+1] and corrStack[idxList[i][0]] > corrStack[idxList[i][0]-1]:
            matchIdxList.append(idxList[i][0])
            matchCorrList.append(float(corrStack[idxList[i][0]] / templateLen))

    # Get the UTCDateTime corresponding to each match
    matchTimeList = [UTCDateTime(year=year, julday=jday) + float(idx / dailySr) for idx in matchIdxList]

    return [matchTimeList, matchCorrList] 

if __name__ == '__main__':
    controlParam = np.array(1, 'i')
    comm = MPI.COMM_WORLD
    nbrThreadMax=0
    comm = MPI.Comm.Get_parent() #Get informations on parent process
    numThread = comm.Get_rank() #Get thread id
    #print(numThread)

    #Select GPU to work on according to thread id
    cp.cuda.runtime.setDevice(numThread)

    #BP: memory control
    #mempool = cp.get_default_memory_pool()

    #We get from master process needed informations
    dateUniq, stopIndex, infoForThread = [], [], []
    infoForThread  = comm.bcast(infoForThread, root=0)
    dateUniq       = infoForThread[0]
    stopIndex      = infoForThread[1]
    wfdir          = infoForThread[2]
    template_dir   = infoForThread[3]
    output_dir     = infoForThread[4]
    vecComp        = infoForThread[5]
    freqMin        = infoForThread[6]
    freqMax        = infoForThread[7]
    standMultFactor = infoForThread[8]
    decimate       = infoForThread[9]

    assert len(vecComp) == 3

    beginIndex    = stopIndex[0] * numThread

    #Get template matrix
    templateSigList_gpu = []
    templatesNameList   = []
    listTempl = os.listdir(template_dir)
    templateSigList_gpu, templatesNameList = ras.getTemplateList(listTempl, vecComp, freqMin, freqMax, decimate, template_dir)
    # DEBUG: See what we got
    #print(templatesNameList)
    #print(templateSigList_gpu)

    #Get the list of daily files to process for this thread
    quotiFiles = []
    fileList = os.listdir(wfdir)
    for i in range(beginIndex, stopIndex[numThread]):
        date = dateUniq[i]
        #Trouve tous les fichiers qui contiennent date
        quotiFiles.extend([file for file in fileList if date in file.split('.')[6]])


    #Loop over daily file and process match
    #iCount = 0
    #timeTot = timedelta(seconds=0)
    for nTempBench in range(1, len(templatesNameList[0])+1):
        for j in range(1,5):
            iCount = 0
            timeTot = timedelta(seconds=0)
            beg = datetime.now()
            for daily in quotiFiles:
                iCount = iCount + 1
                ntwkSt, chan, year, jday = ras.getDailyInfo(daily)
                dailySig, dailySr, dailyTbeg, dailyTend = ras.util.DailyLoader(wfdir +'/'+ daily, freqMin, freqMax, decimate).data
                dailySig_gpu = cp.asarray(dailySig)
                dailyInfo = [ntwkSt, chan, jday]

                #print(daily, mempool.used_bytes())

                tic=datetime.now()
                if chan == vecComp[0]:
                    #print(dailyInfo)
                    corrE = doAlign(dailySig_gpu, templateSigList_gpu[0], templatesNameList[0], dailyInfo, nTempBench)
                elif chan == vecComp[1]:
                    #print(dailyInfo)
                    corrN = doAlign(dailySig_gpu, templateSigList_gpu[1], templatesNameList[1], dailyInfo, nTempBench)
                elif chan == vecComp[2]:
                    #print(dailyInfo)
                    corrZ = doAlign(dailySig_gpu, templateSigList_gpu[2], templatesNameList[2], dailyInfo, nTempBench)
                toc=datetime.now()
                timeTot += toc-tic

                if iCount % len(vecComp) == 0:
                    print('Thread=', numThread, 'Processing:', dailyInfo[0], year, dailyInfo[2])
                    for i in range(nTempBench):

                        templateName = templatesNameList[0][i][0]+'.'+templatesNameList[0][i][1]

                        # stack three component cc functions
                        stack = (corrE[i] + corrN[i] + corrZ[i]) / 3

                        # Get template length
                        templateLen = len(cp.asnumpy(templateSigList_gpu[0][0]))

                        # Compute treshold for each dailySig
                        treshold = ras.getThreshold(stack, templateLen, standMultFactor)

                        # Extract matches
                        matchList = MatchFinder(stack, treshold, int(year), int(jday), templateLen, dailySr)

                        # Export matches to disk
                        matchOutFile = open(output_dir+'/'+templateName+'.'+dailyInfo[0]+'.'+year+'.'+dailyInfo[2], 'w')
                        for i in range(len(matchList[0])):
                            print(matchList[0][i], '{:.3f}'.format(matchList[1][i]), file=matchOutFile)
                        matchOutFile.close()

                        # remove empty files
                        if os.path.getsize(output_dir+'/'+templateName+'.'+dailyInfo[0]+'.'+year+'.'+dailyInfo[2]) == 0:
                            os.system('rm -rf '+output_dir+'/'+templateName+'.'+dailyInfo[0]+'.'+year+'.'+dailyInfo[2])

            #print('Runtime=', datetime.now() - beg)
            print('Alignment Time', numThread, nTempBench, timeTot)

    comm.Reduce([controlParam, MPI.INT], None, op=MPI.SUM, root=0)
    comm.Disconnect()
