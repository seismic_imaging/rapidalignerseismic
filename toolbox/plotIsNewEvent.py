#!/usr/bin/env python
#-*- coding: utf-8 -*-

from obspy.core import UTCDateTime
from obspy.core import Stream, read
import matplotlib.pyplot as plt
import os, glob, shutil
import numpy as np
import pandas as pd


def printPiled(dailyE, dailyN, dailyZ, eventid, col, lenLines, eventList):
	
    outDir = 'Test2'

    fig = plt.figure()
    fig.set_figheight(10.8)
    fig.set_figwidth(19.2)
    #fig.suptitle("cluster correlation: "+ str(corr)) # + " type: " + type)

    minLenE = min([len(i) for i in dailyE])
    minLenN = min([len(i) for i in dailyN])
    minLenZ = min([len(i) for i in dailyZ])

    dailyStackE = np.zeros(minLenE)
    dailyStackN = np.zeros(minLenN)
    dailyStackZ = np.zeros(minLenZ)
    
    colplt = []
    for i in range(len(col)):
        if col[i] == 0:
            colplt.append('black')
        elif col[i] == 1:
            colplt.append('blue')
        else:
            colplt.append('red')

    print(colplt)

    ax = fig.add_subplot(1,3,1)
    ax.set_title('Z')
    for i in range(lenLines):
        ax.plot(dailyZ[i] / np.max(dailyZ[i]) * 0.75 + i, color=colplt[i], linewidth=.75, label=eventList[i])
        dailyStackZ += dailyZ[i][:minLenZ]/ np.max(dailyZ[i][:minLenZ])
    ax.plot(dailyStackZ / lenLines * 0.75 + i +1,  linewidth=.75, label= 'Stacked trace')
    handles, labels = ax.get_legend_handles_labels()
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(1,3,2,sharey=ax, sharex=ax)
    ax.set_title('E')
    for i in range(lenLines):
        ax.plot(dailyE[i] / np.max(dailyE[i]) * 0.75 + i, color=colplt[i], linewidth=.75)
        dailyStackE += dailyE[i][:minLenE] / np.max(dailyE[i][:minLenE])
    ax.plot(dailyStackE / lenLines * 0.75 + i +1, linewidth=.75)
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(1, 3, 3, sharey=ax, sharex=ax)
    ax.set_title('N')
    for i in range(lenLines):
        ax.plot(dailyN[i] / np.max(dailyN[i]) * 0.75 + i, color=colplt[i], linewidth=.75)
        dailyStackN += dailyN[i][:minLenN] / np.max(dailyN[i][:minLenN])
    ax.plot(dailyStackN / lenLines * 0.75 + i +1, linewidth=.75)
    plt.xlabel("Time (s)")

    fig.legend(handles[::-1], labels[::-1], loc='center right')
    xlocs, xlabs = plt.xticks()
    xlabs = xlocs / 100.0
    plt.xticks(xlocs, xlabs)
    plt.xlim(0, 3000)

    #plt.show()
    plt.subplots_adjust(right=0.84, hspace=0.3)
    plt.savefig(outDir+'/Pilled' + str(eventid) + '.svg', format='svg', dpi=1200)
    plt.close()


def loadEvent(events, repo, comp, fmin, fmax, mindeltat, plusdeltat):

    date = UTCDateTime(events)
    print(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily=read(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily.taper(max_percentage=0.0001, type="hann")
    Daily.merge(fill_value=0)
    Daily.filter("bandpass", freqmin=fmin,freqmax=fmax)
    t_ini=date
    t_ini=t_ini-mindeltat
    t_end = t_ini+plusdeltat
    Daily.trim(t_ini ,t_end, pad=True, nearest_sample = False, fill_value=0)
    
    return Daily[0].data



def loadAndPrintEvents(cluster, clusterId):

	eventsList = cluster['Event'].to_list()
	colId = cluster['type'].to_list()
	dailyZ = []
	dailyE = []
	dailyN = []
	fmin = 2
	fmax = 20
	mindeltat = -5
	plusdeltat = 5
	templLen = 40
	deltat = mindeltat + plusdeltat + templLen
	repo = 'Massif_Central/data/VERF/'
	
	
	vecComp = ["HHE", "HHN", "HHZ"]
	
	count = 0
	
	for event in eventsList:
		count = count +1
		for j in range(len(vecComp)):
			if vecComp[j][-1] == "Z":
				dailyZ.append(loadEvent(event,  repo, vecComp[j], fmin, fmax, mindeltat, deltat))
			elif vecComp[j][-1] == "E":
				dailyE.append(loadEvent(event,  repo, vecComp[j], fmin, fmax, mindeltat, deltat))
			elif vecComp[j][-1] == "N":
				dailyN.append(loadEvent(event, repo, vecComp[j], fmin, fmax, mindeltat, deltat))
		print(count)
		
	printPiled(dailyE, dailyN, dailyZ, clusterId, colId, count, eventsList)





if __name__ == "__main__":
	eventsListCluster = pd.read_csv('Massif_Central/events_clean_MC_dec3_St02.csv')

	alreadyFoundEvt = pd.read_csv('Massif_Central/alreadyFound_St0.2_dec2.csv')

	templateList = np.genfromtxt('Massif_Central/templateListMC.txt', dtype=str)



	events = eventsListCluster['Event'].to_list()

	alreadyFound = alreadyFoundEvt['Event'].to_list()

	typeEvent = []

	for event in events:
		if event in alreadyFound:
			typeEvent.append(0)
		elif event in templateList:
			typeEvent.append(1)
		else:
			typeEvent.append(2)


	eventsListCluster['type'] = typeEvent


	clusterList = list(set(eventsListCluster['clusterId'].to_list()))


	for clusterId in clusterList:
		eventsToPlot = eventsListCluster[eventsListCluster['clusterId']==clusterId]
		loadAndPrintEvents(eventsToPlot, clusterId)
	
	
	
