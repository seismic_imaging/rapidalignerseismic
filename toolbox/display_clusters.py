import numpy as np
from obspy.core import Stream, read, Trace
from obspy import UTCDateTime
import matplotlib.pyplot as plt
import pandas as pd
import getopt
from scipy import signal

def readConfigFile(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine = "".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'length':
                    length = eval(rLine[1])
                    assert type(length) is float or int
                elif rLine[0] == 'dailyDir':
                    wfdir = eval(rLine[1])
                elif rLine[0] == 'printDir':
                    outdir = eval(rLine[1])
                elif rLine[0] == 'vecComp':
                    vecComp = eval(rLine[1])
                    assert type(vecComp) is list
                elif rLine[0] == 'freqMin':
                    freqMin = eval(rLine[1])
                    assert type(freqMin) is float or int
                elif rLine[0] == 'freqMax':
                    freqMax = eval(rLine[1])
                    assert type(freqMax) is float or int
                elif rLine[0] == 'treshold':
                    treshold = eval(rLine[1])
                    assert type(treshold) is float or int
                elif rLine[0] == 'netStat':
                    netStat = eval(rLine[1])
                elif rLine[0] == 'concatenateMatches':
                    matchFile = eval(rLine[1])
                elif rLine[0] == 'cleanedEventFile':
                    eventsFile = eval(rLine[1])
                elif rLine[0] == 'consecTimeWindowsFile':
                    tWindowsFile = eval(rLine[1])
                elif rLine[0] == 'linkageFile':
                    linkageFileName = eval(rLine[1])

        repo = wfdir + netStat
    return length, freqMin, freqMax, repo, outdir, vecComp, matchFile, eventsFile, tWindowsFile, linkageFileName, treshold

def signaltonoise(a, axis=0, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, m/sd)


#Load event from daily file
def loadEvent(events, line, repo, comp, fmin, fmax, mindeltat, plusdeltat):

    date = UTCDateTime(events[line][1])
    print(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily=read(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily.taper(max_percentage=0.0001, type="hann")
    Daily.merge(fill_value=0)
    Daily.filter("bandpass", freqmin=fmin,freqmax=fmax)
    t_ini=date
    t_ini=t_ini+mindeltat
    t_end = t_ini+plusdeltat
    Daily.trim(t_ini ,t_end, pad=True, nearest_sample = False, fill_value=0)

    return Daily[0].data

def printSupperposed(dailyE, dailyN, dailyZ, eventid, lenLines, corr, count, outDir, type):

    fig, axs = plt.subplots(3, sharex=True, sharey=True)
    fig.set_figheight(10.8)
    fig.set_figwidth(19.2)
    for i in range(lenLines):
        axs[0].plot(dailyZ[i] / np.max(dailyZ[i]), linewidth=.75, label=eventid[i])
        axs[1].plot(dailyE[i] / np.max(dailyE[i]), linewidth=.75)
        axs[2].plot(dailyN[i] / np.max(dailyN[i]), linewidth=.75)

    axs[1].set_title("E")
    axs[0].set_title("Z")
    axs[2].set_title("N")
    handles, labels = axs[0].get_legend_handles_labels()
    fig.legend(handles[::-1], labels[::-1], loc='center right')
    fig.suptitle("cluster correlation: "+ str(corr)) # + " type: " + type)
    xlocs, xlabs = plt.xticks()
    xlabs = xlocs / 100.0
    plt.xticks(xlocs, xlabs)
    plt.xlim(0,3000)
    plt.subplots_adjust(right=0.84, hspace=0.3)
    plt.xlabel("Time (s)")
    #plt.show()
    plt.savefig(outDir+'/Supperposed'+str(count)+'.svg', format='svg', dpi=1200)
    plt.close()


def reLoadEvent(event, repo, comp, fmin, fmax, mindeltat, plusdeltat, isRaw):

    Daily = read(repo + '*' + comp + "*" + str(event.year) + '.' + str(event.julday).zfill(3))
    Daily.taper(max_percentage=0.0001, type="hann")
    Daily.merge(fill_value=0)
    if isRaw == False:
        Daily.filter("bandpass", freqmin=fmin, freqmax=fmax)
    t_ini = event
    t_ini = t_ini + mindeltat
    t_end = t_ini + plusdeltat
    Daily.trim(t_ini, t_end, pad=True, nearest_sample=False, fill_value=0)

    return Daily[0].data

def getDailyAlign(dailyE, dailyN, dailyZ, deltat, eventid, repo, vecComp, fmin, fmax, mindeltat):

    alDailyE = []
    alDailyN = []
    alDailyZ = []
    newEventList = []
    listSR = []

    # Get trace with best S/R as reference
    for i in range(len(dailyE)):
        srE = signaltonoise(dailyE[i])
        srN = signaltonoise(dailyN[i])
        srZ = signaltonoise(dailyZ[i])
        listSR.append(srE+srN+srZ)

    idSrMax = np.argmax(listSR)

    for i in range(len(dailyE)) :
        if i ==idSrMax:
            alDailyE.append(dailyE[i])
            alDailyN.append(dailyN[i])
            alDailyZ.append(dailyZ[i])
            newEventList.append(eventid[i])
        else:
            corrE = signal.correlate(dailyE[idSrMax], dailyE[i], mode="full", method="fft") / (np.linalg.norm(dailyE[idSrMax])*np.linalg.norm(dailyE[i]))
            corrN = signal.correlate(dailyN[idSrMax], dailyN[i], mode="full", method="fft") / (np.linalg.norm(dailyN[idSrMax])*np.linalg.norm(dailyN[i]))
            corrZ = signal.correlate(dailyZ[idSrMax], dailyZ[i], mode="full", method="fft") / (np.linalg.norm(dailyZ[idSrMax])*np.linalg.norm(dailyZ[i]))

            print(len(corrE), len(corrN), len(corrZ))

            try:
                idMax = np.argmax((corrE + corrN +corrZ)/3)
            except:
                print("Can't align due to dimension mismatch\n")
                idMax = len(dailyE[i]) + 1

            offset = len(dailyE[i]) - idMax -1

            #Refaire en utilisant Sr du signal
            offset_time = offset / 100.


            newEvent = eventid[i] + offset_time
            newEventList.append(newEvent)
            raw = False

            for j in range(len(vecComp)):
                if vecComp[j][-1] == "E":
                    alDailyE.append(reLoadEvent(newEvent, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))
                elif vecComp[j][-1] == "N":
                    alDailyN.append(reLoadEvent(newEvent, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))
                elif vecComp[j][-1] =="Z":
                    alDailyZ.append(reLoadEvent(newEvent, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))


    return alDailyE, alDailyN, alDailyZ, newEventList, idSrMax

def printStack(data, comp, info):

    stats = {'network': info[0], 'station': info[1], 'location': info[2], 'channel': info[3]+comp,
             'npts': len(data), 'sampling_rate': info[4], 'mseed': {'dataquality': 'D'}}
    st = Stream([Trace(data=data, header=stats)])
    st.write(info[0] + '.' + info[1]+ '..' + info[3]+ comp + '.cluster_' + info[5], format='MSEED')



def printPiled(dailyE, dailyN, dailyZ, eventid, lenLines, corr, count, outDir, deltat, spectro, type, saveStack):

    fig = plt.figure()
    fig.set_figheight(10.8)
    fig.set_figwidth(19.2)
    fig.suptitle("cluster correlation: "+ str(corr)) # + " type: " + type)

    minLenE = min([len(i) for i in dailyE])
    minLenN = min([len(i) for i in dailyN])
    minLenZ = min([len(i) for i in dailyZ])

    dailyStackE = np.zeros(minLenE)
    dailyStackN = np.zeros(minLenN)
    dailyStackZ = np.zeros(minLenZ)

    ax = fig.add_subplot(1,3,1)
    ax.set_title('Z')
    for i in range(lenLines):
        ax.plot(dailyZ[i] / np.max(dailyZ[i]) * 0.75 + i, linewidth=.75, label=eventid[i])
        dailyStackZ += dailyZ[i][:minLenZ]/ np.max(dailyZ[i][:minLenZ])
    ax.plot(dailyStackZ / lenLines * 0.75 + i +1,  linewidth=.75, label= 'Stacked trace')
    handles, labels = ax.get_legend_handles_labels()
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(1,3,2,sharey=ax, sharex=ax)
    ax.set_title('E')
    for i in range(lenLines):
        ax.plot(dailyE[i] / np.max(dailyE[i]) * 0.75 + i, linewidth=.75)
        dailyStackE += dailyE[i][:minLenE] / np.max(dailyE[i][:minLenE])
    ax.plot(dailyStackE / lenLines * 0.75 + i +1, linewidth=.75)
    plt.xlabel("Time (s)")

    ax = fig.add_subplot(1, 3, 3, sharey=ax, sharex=ax)
    ax.set_title('N')
    for i in range(lenLines):
        ax.plot(dailyN[i] / np.max(dailyN[i]) * 0.75 + i, linewidth=.75)
        dailyStackN += dailyN[i][:minLenN] / np.max(dailyN[i][:minLenN])
    ax.plot(dailyStackN / lenLines * 0.75 + i +1, linewidth=.75)
    plt.xlabel("Time (s)")

    fig.legend(handles[::-1], labels[::-1], loc='center right')
    xlocs, xlabs = plt.xticks()
    xlabs = xlocs / 100.0
    plt.xticks(xlocs, xlabs)
    plt.xlim(0, 3000)

    #plt.show()
    plt.subplots_adjust(right=0.84, hspace=0.3)
    plt.savefig(outDir+'/Pilled' + str(count) + '.svg', format='svg', dpi=1200)
    plt.close()

    if saveStack:
        info =['MQ', 'PBO', '', 'HH', 100.0, str(count)]
        printStack(dailyStackE, 'E', info)
        printStack(dailyStackN, 'N', info)
        printStack(dailyStackZ, 'Z', info)

    if spectro :
         FS = 100
         fig, axs = plt.subplots(nrows=2, ncols=3)
         fig.set_figheight(10.8)
         fig.set_figwidth(19.2)

         axs[0, 0].set_title('Z')
         Pxx, freqs, bins, im = axs[0, 0].specgram(dailyStackZ, Fs=FS, vmin=-100, cmap='plasma')
         axs[0, 0].set_xlim([0, 35])
         axs[0, 0].set_ylim([0, 30])
         axs[0, 0].set_xlabel('Time (s)')
         axs[0, 0].set_ylabel('Frequency (Hz)')

         axs[1, 0].set_title('Z')
         axs[1, 0].plot(dailyStackZ)
         axs[1, 0].set_xlim([0, 3500])
         plt.sca(axs[1, 0])
         xlocs, xlabs = plt.xticks()
         xlabs = xlocs / 100.0
         plt.xticks(xlocs, xlabs)
         axs[1, 0].set_xlabel('Time (s)')

         axs[0, 1].set_title('E')
         Pxx, freqs, bins, im = axs[0, 1].specgram(dailyStackE, Fs=FS, vmin=-100, cmap='plasma')
         #fig.colorbar(im)
         axs[0, 1].sharex(axs[0, 0])
         axs[0, 1].sharey(axs[0, 0])
         axs[0, 1].set_xlabel('Time (s)')
         axs[0, 1].set_ylabel('Frequency (Hz)')

         axs[1, 1].set_title('E')
         axs[1, 1].plot(dailyStackE)
         axs[1, 1].sharex(axs[1, 0])
         axs[1, 1].sharey(axs[1, 0])
         axs[1, 1].set_xlabel('Time (s)')

         axs[0, 2].set_title('N')
         Pxx, freqs, bins, im = axs[0, 2].specgram(dailyStackN, Fs=FS, vmin=-100, cmap='plasma')
         axs[0, 2].sharex(axs[0, 0])
         axs[0, 2].sharey(axs[0, 0])
         axs[0, 2].set_xlabel('Time (s)')
         axs[0, 2].set_ylabel('Frequency (Hz)')

         axs[1, 2].set_title('N')
         axs[1, 2].plot(dailyStackN)
         axs[1, 2].sharex(axs[1, 0])
         axs[1, 2].sharey(axs[1, 0])
         axs[1, 2].set_xlabel('Time (s)')

         #plt.show()
         plt.subplots_adjust(right=0.84, hspace=0.3)
         plt.savefig(outDir + '/spectro/Spectrogram_cluster' + str(count) + '_event_stack.svg', format='svg', dpi=1200)
         plt.close()


def getAlignStack(dailyE, dailyN, dailyZ, stackE, stackN, stackZ, deltat, eventid, repo,
                                                                    vecComp, fmin, fmax, mindeltat):
    alDailyE = []
    alDailyN = []
    alDailyZ = []
    newEventList = []

    #BP!: harcoded value
    deltat = deltat + 20

    for i in range(len(dailyE)) :
        corrE = signal.correlate(stackE, dailyE[i]/ np.max(dailyE[i]), mode="full", method="fft") / (np.linalg.norm(stackE)*np.linalg.norm(dailyE[i]/ np.max(dailyE[i])))
        corrN = signal.correlate(stackN, dailyN[i]/ np.max(dailyN[i]), mode="full", method="fft") / (np.linalg.norm(stackN)*np.linalg.norm(dailyN[i]/ np.max(dailyN[i])))
        corrZ = signal.correlate(stackZ, dailyZ[i]/ np.max(dailyZ[i]), mode="full", method="fft") / (np.linalg.norm(stackZ)*np.linalg.norm(dailyZ[i]/ np.max(dailyZ[i])))

        try:
            idMax = np.argmax((corrE + corrN +corrZ)/3)
        except:
            idMax = len(dailyE[i])+1

        offset = len(dailyE[i]) - idMax -1

        #Refaire en utilisant Sr du signal
        offset_time = offset / 100.


        newEvent = eventid[i] + offset_time
        newEventList.append(newEvent+ mindeltat)

        # BP!: harcoded value
        #Nice looking figures
        mindeltat = 0
        nfmin = 2
        nfmax = 20
        print('mindeltat=',mindeltat)
        raw = False

        for j in range(len(vecComp)):
            if vecComp[j][-1] == "E":
                alDailyE.append(reLoadEvent(newEvent, repo, vecComp[j], nfmin, nfmax, mindeltat, deltat, raw))
            elif vecComp[j][-1] == "N":
                alDailyN.append(reLoadEvent(newEvent, repo, vecComp[j], nfmin, nfmax, mindeltat, deltat, raw))
            elif vecComp[j][-1] == "Z":
                alDailyZ.append(reLoadEvent(newEvent, repo, vecComp[j], nfmin, nfmax, mindeltat, deltat, raw))

        #for j in range(len(newEventList)):
        #    newEventList[j] = newEventList[j] + mindeltat

    return alDailyE, alDailyN, alDailyZ, newEventList

def getStackedSignal(dailyE, dailyN, dailyZ):

    minLenE = min([len(i) for i in dailyE])
    minLenN = min([len(i) for i in dailyN])
    minLenZ = min([len(i) for i in dailyZ])

    dailyStackE = np.zeros(minLenE)
    dailyStackN = np.zeros(minLenN)
    dailyStackZ = np.zeros(minLenZ)

    for i in range(len(dailyE)) :
        dailyStackE += dailyE[i][:minLenE] / np.max(dailyE[i][:minLenE])
        dailyStackN += dailyN[i][:minLenN] / np.max(dailyN[i][:minLenN])
        dailyStackZ += dailyZ[i][:minLenZ] / np.max(dailyZ[i][:minLenZ])

    dailyStackE = dailyStackE / len(dailyE)
    dailyStackN = dailyStackN / len(dailyN)
    dailyStackZ = dailyStackZ / len(dailyZ)

    return dailyStackE, dailyStackN, dailyStackZ

#plot spectrogram of signals if needed
def printSpectro(sAlignDailyE, sAlignDailyN, sAlignDailyZ, deltat, lenlines, sNeventid, outDir, vecComp):

    raw = True
    dailyZR = []
    dailyER = []
    dailyNR = []
    deltat = deltat + 20
    mindeltat = 5
    fmin = 2
    fmax = 10
    for event in sNeventid:
        for j in range(len(vecComp)):
            if vecComp[j][-1] == "Z":
                dailyZR.append(reLoadEvent(event, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))
            elif vecComp[j][-1] == "E":
                dailyER.append(reLoadEvent(event, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))
            elif vecComp[j][-1] == "N":
                dailyNR.append(reLoadEvent(event, repo, vecComp[j], fmin, fmax, mindeltat, deltat, raw))

    FS = 100

    for i in range(lenlines):
        fig, axs = plt.subplots(nrows=2, ncols=3)
        fig.set_figheight(10.8)
        fig.set_figwidth(19.2)
        fig.suptitle(sNeventid[i])

        axs[0, 0].set_title('Z')
        Pxx, freqs, bins, im = axs[0, 0].specgram(dailyZR[i], Fs=FS, cmap='plasma')
        axs[0, 0].set_xlim([0, 35])
        axs[0, 0].set_ylim([0,30])
        axs[0, 0].set_xlabel('Time (s)')
        axs[0, 0].set_ylabel('Frequency (Hz)')

        axs[1, 0].set_title('Z')
        axs[1, 0].plot(sAlignDailyZ[i])
        axs[1, 0].set_xlim([0, 3500])
        plt.sca(axs[1, 0])
        xlocs, xlabs = plt.xticks()
        xlabs = xlocs / 100.0
        plt.xticks(xlocs, xlabs)
        axs[1, 0].set_xlabel('Time (s)')

        axs[0, 1].set_title('E')
        Pxx, freqs, bins, im = axs[0, 1].specgram(dailyER[i], Fs=FS, cmap='plasma')
        axs[0, 1].sharex(axs[0, 0])
        axs[0, 1].sharey(axs[0, 0])
        axs[0, 1].set_xlabel('Time (s)')
        axs[0, 1].set_ylabel('Frequency (Hz)')

        axs[1, 1].set_title('E')
        axs[1, 1].plot(sAlignDailyE[i])
        axs[1, 1].sharex(axs[1, 0])
        axs[1, 1].sharey(axs[1, 0])
        axs[1, 1].set_xlabel('Time (s)')

        axs[0, 2].set_title('N')
        Pxx, freqs, bins, im = axs[0, 2].specgram(dailyNR[i], Fs=FS, cmap='plasma')
        axs[0, 2].sharex(axs[0, 0])
        axs[0, 2].sharey(axs[0, 0])
        axs[0, 2].set_xlabel('Time (s)')
        axs[0, 2].set_ylabel('Frequency (Hz)')

        axs[1, 2].set_title('N')
        axs[1, 2].plot(sAlignDailyN[i])
        axs[1, 2].sharex(axs[1, 0])
        axs[1, 2].sharey(axs[1, 0])
        axs[1, 2].set_xlabel('Time (s)')

        # plt.show()
        plt.subplots_adjust(right=0.84, hspace=0.3)
        plt.savefig(outDir + '/spectro/Spectrogram_cluster' + str(count) + '_event_' + str(sNeventid[i]) + '.svg',
                    format='svg', dpi=1200)
        plt.close()





#Load events detected and print them grouped according to dendogram
def loadAndPrintClusters(events, lines, repo, vecComp, fmin, fmax, templLen, mindeltat, plusdeltat, corr, count, outDir, consecWindows, spectro, saveStack, type=''):
    dailyZ = []
    dailyE = []
    dailyN = []
    eventid = []
    eventHolder = []
    alignDailyE = []
    alignDailyN = []
    alignDailyZ = []


    deltat = mindeltat + plusdeltat + templLen

    for line in lines:
         #consecWindows[line] *
        for j in range(len(vecComp)):
            if vecComp[j][-1] == "Z":
                dailyZ.append(loadEvent(events, line, repo, vecComp[j], fmin, fmax, mindeltat, deltat))
            elif vecComp[j][-1] == "E":
                dailyE.append(loadEvent(events, line, repo, vecComp[j], fmin, fmax, mindeltat, deltat))
            elif vecComp[j][-1] == "N":
                dailyN.append(loadEvent(events, line, repo, vecComp[j], fmin, fmax, mindeltat, deltat))

        eventHolder.append(events[line][0])
        date1 = UTCDateTime(events[line][1])
        eventid.append(date1)
        print(date1)

    #Perform a first alignment using best SNR trace
    alignDailyE, alignDailyN, alignDailyZ, neventid, idSrMax = getDailyAlign(dailyE, dailyN, dailyZ, deltat, eventid, repo,
                                                                    vecComp, fmin, fmax, mindeltat)

    # Remove useless list
    del dailyE, dailyN, dailyZ

    #Use aligned signals to generate stack signal to use as reference
    stackE, stackN, stackZ = getStackedSignal(alignDailyE, alignDailyN, alignDailyZ)

    sAlignDailyE, sAlignDailyN, sAlignDailyZ, sNeventid = getAlignStack(alignDailyE, alignDailyN, alignDailyZ, stackE, stackN, stackZ, deltat, neventid, repo,
                                                                    vecComp, fmin, fmax, mindeltat)


    del stackE, stackN, stackZ
    print("cluster : ", count)
    print(" id signal ref: ", idSrMax)
    for i in range(len(alignDailyN)):
        if i != idSrMax:
            corrE = signal.correlate(alignDailyE[idSrMax], alignDailyE[i], mode="full", method="fft")/ (np.std(alignDailyE[idSrMax])*np.std(alignDailyE[i])*len(alignDailyE[i])) #(np.linalg.norm(alignDailyE[idSrMax])*np.linalg.norm(alignDailyE[i]))
            corrN = signal.correlate(alignDailyN[idSrMax], alignDailyN[i], mode="full", method="fft")/ (np.std(alignDailyN[idSrMax])*np.std(alignDailyN[i])*len(alignDailyN[i])) #(np.linalg.norm(alignDailyN[idSrMax])*np.linalg.norm(alignDailyN[i]))
            corrZ = signal.correlate(alignDailyZ[idSrMax], alignDailyZ[i], mode="full", method="fft")/ (np.std(alignDailyZ[idSrMax])*np.std(alignDailyZ[i])*len(alignDailyZ[i])) #(np.linalg.norm(alignDailyZ[idSrMax])*np.linalg.norm(alignDailyZ[i]))

            try:
                corrT = (corrE + corrN + corrZ)/3.
                idM = np.argmax(corrT)
            except:
                idM = 0
                corrT = np.array([1])

            print(idM)
            print("max corr = ", np.max(corrT))

            #plt.plot(corrT)
            #plt.show()

    del alignDailyE, alignDailyN, alignDailyZ

    printSupperposed(sAlignDailyE, sAlignDailyN, sAlignDailyZ, sNeventid, len(lines), corr, count, outDir, type)

    printPiled(sAlignDailyE, sAlignDailyN, sAlignDailyZ, sNeventid, len(lines), corr, count, outDir, deltat, spectro, type, saveStack)

    if spectro:
        printSpectro(sAlignDailyE, sAlignDailyN, sAlignDailyZ, deltat, len(lines), sNeventid, outDir, vecComp)

    eventAndType = []
    if len(type) != 0:
        for i in range(len(sNeventid)):
            eventAndType.append([eventHolder[i], sNeventid[i], type, UTCDateTime(sNeventid[i]).julday, count])


    return eventAndType




def getList(evList, nbEvts, linksDF):

    nList = []

    for ev in evList:
        print(ev)
        if ev <= nbEvts:
            nList.extend([int(ev)])
        else:
            id = ev - (nbEvts +1)
            print(id)
            print(linksDF)
            line = linksDF.loc[[id]].values.flatten().tolist()
            print(line)
            linksDF.drop(linksDF.loc[[id]].index, inplace=True)
            list = line [0:2]
            print(list)
            evt, linksDF =getList(list, nbEvts, linksDF)
            nList.extend(evt)

    return nList, linksDF

#Print the list of the options of the script and the details then exit
def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print("-h  | --help						Print the usage message")
    print("-r  | --ras_config <config_file> To use a specific configuration file")
    sys.exit()


if __name__ == "__main__":

    # try:
    #     opts, args = getopt.getopt(sys.argv[1:], "hr:", ["help", "ras_config="])
    # except getopt.GetoptError:
    #     print(sys.argv[0] + ' -r <configFile>')
    #     sys.exit(2)
    #
    # for opt, arg in opts:
    #     if opt in ("-h", "--help"):
    #         printUsageAndExit(sys.argv[0])
    #     elif opt in ("-r", "--ras_config"):
    #         configFile = arg

    #templLen, fmin, fmax, repo, outDir, vecComp, matchFile, eventFile, tWindowsFile, linkFile, treshold = readConfigFile(configFile)

    eventFile = 'Massif_Central/massif_central_2018_2024FRNF.txt' #martinique_events_6m_zdist_St_03_oct21_30s.txt' #martinique_events_6m_zdist_St_025_10H_15s_d.txt' #
    repo = "../data/wf_MC/TEMP/" #Massif_Central/data/VERF/" #../data/wf_MQ/post/MQ.PBO.00."
    linkFile = "Massif_Central/linkage_MC_FRNF_2018_2024.txt" #linkage_zdist_St_025_sept11.txt" #
    equivFile = "martinique_events_equivalence_id.txt"
    tWindowsFile = "Massif_Central/massif_cent_tWindows_VERF_229d.csv"
    outDir = "Massif_Central/Massif_Cent_FRNF_2018_2024" #Martinique_plots_stack_clean" stack_025" #
    typeFile = "Massif_Central/Tag_MC_20218-2024.txt" #Martinique_Tags_v4.txt" #"Martinique_Tags_St_025_Oct4.txt" #
    catalFileName = "Massif_Central/events_clean_MC_dec10_St02_v2.csv"
    EventListFile = "test.txt"
    equivNeeded = False
    genCatalog = False  #True
    spectro = True
    saveStack = False
    useLink = True



    # Dissimilarity threshold (1-CC)
    treshold = 0.8
    #Filtering
    fmin = 2
    fmax = 20

    mindeltat = -5 #5
    plusdeltat = 5
    templLen = 40

    vecComp = ["HHZ", "HHE", "HHN"]
    #lines = [307, 454]

    if useLink:
        consecTWindows = pd.read_csv(tWindowsFile)
        events = np.genfromtxt(eventFile, dtype=None)
        linkage = np.genfromtxt(linkFile, delimiter=" ")
        eventCount = len(linkage)
        links = np.array([])

        try:
            for link in linkage:
                if link[2]< treshold:
                    links = np.append(links, link)
        except:
            links = np.append(links, linkage)

        links=links.reshape(int(len(links)/4),4)

        linksDF = pd.DataFrame(links, columns=['ev1', 'ev2', 'dist', 'level'])
        linksDF.sort_values("dist", inplace=True, ascending=False)

        consecWindows = consecTWindows["consecTWindows"].to_list()

        clusters = []
        correlClusters = []

        #Identify clusters according to linkage
        while linksDF.empty == False:

            #Get the pair with the maximum distance compare to the treshold
            i = linksDF.query('dist == dist.max()').index
            line = linksDF.query('dist == dist.max()').values.flatten().tolist()
            #remove the line from the database
            linksDF.drop(i, inplace =True)
            evList = line[0:2]
            corr = 1-line[2]

            cluster, linksDF = getList(evList, eventCount, linksDF)

            clusters.append(cluster)
            correlClusters.append(corr)


        if equivNeeded == True:
            #Do correspondance with event id
            equiv = np.genfromtxt(equivFile)

            clustersEquiv = []
            for line in clusters:
                clusterEq = []
                for ev in line:
                    id, = np.where(equiv[:,0]== ev)
                    clusterEq.extend([int(equiv[id,1][0])])

                clustersEquiv.append(clusterEq)
        else:
            clustersEquiv=clusters

        if genCatalog:
            clusterType = np.genfromtxt(typeFile, usecols=1 ,dtype=str)


    #From here experimental stuff to just print one specific cluster if wanted
    else:
        events=np.genfromtxt(EventListFile, dtype=None)
        correlClusters = [0.527466]
        consecWindows = 1
        clustersEq= []
        clustersEquiv=[]
        for i in range(len(events)):
            clustersEq.append(i)
        clustersEquiv = [clustersEq]

    #Plot
    count = 0
    if genCatalog:
        catal = pd.DataFrame(columns=["Eventid", "Event", "Type", "julday", "clusterId"])

        for lines in clustersEquiv:
            if clusterType[count] != 'N':
                eventAndType = loadAndPrintClusters(events, lines, repo, vecComp, fmin, fmax, templLen, mindeltat, plusdeltat, correlClusters[count],count, outDir, consecWindows,spectro, saveStack, clusterType[count])
                for i in range(len(eventAndType)):
                    catal = pd.concat([pd.DataFrame([eventAndType[i]], columns=catal.columns), catal], ignore_index=True)

            count += 1

        catal.to_csv(catalFileName, index=False)
    else:
        for lines in clustersEquiv:
            eventAndType = loadAndPrintClusters(events, lines, repo, vecComp, fmin, fmax, templLen, mindeltat, plusdeltat, correlClusters[count],count, outDir, consecWindows, spectro, saveStack)
            count += 1



