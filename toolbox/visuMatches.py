#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import glob, os

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from obspy.core import Stream, read
from obspy import UTCDateTime
from obspy.io.xseed import Parser

def readMatchFile(outDir, stat, templ):

    os.system('cat '+outDir+'/'+stat+'*'+templ+'* > '+outDir+'/matches.'+stat+'.'+templ)
    matchList = []
    with open(outDir+'/matches.'+stat+'.'+templ) as matchFile:
        for line in matchFile:
            matchUTC  = line.split()[0]
            matchCorr = line.split()[1]
            matchList.append((matchUTC, matchCorr))

    return matchList

def PlotCleanMatches(stat, jday_lst, mtc_lst_clean):
    tr_z_lst, tr_e_lst, tr_n_lst             = [], [], [] # Init. list of traces
    tr_nrm_z_lst, tr_nrm_e_lst, tr_nrm_n_lst = [], [], [] # Init. list of norm. traces
    #idxs = []
    print('Plotting clean matches for station', stat)

    for jday in jday_lst:
        idxs = []
        for item in mtc_lst_clean:
            t = UTCDateTime(item[0])

            # Make sure only to take idx corresponding to jday
            if int(jday) == t.julday:
                t_h0 = UTCDateTime(year=t.year, julday=t.julday, hour=0, minute=0)
                idx  = int((t - t_h0) * 100) # TODO sr (100Hz) must be a soft value
                #print(t, t_h0, idx)
                idxs.append(idx)


        # Extract matches
        st = Stream()

        # load trace in stream: raises error if trace doesn't exist
        try:
            st += read(workdir+'../data/wf_martinique/'+stat+'.*.?H?.D.2021.' + jday)
        except Exception:
            print(workdir+'../data/wf_martinique/'+stat+'.*.?H?.D.2021.' + jday)
            continue

        st.merge(fill_value=0)

        t0   = st[0].stats.starttime
        dt   = st[0].stats.sampling_rate
        stnm = st[0].stats.station

        for tr in st:
            for tr_id, idx in enumerate(idxs):
                # Make a trace copy, estimate delay and trim
                tr_c = tr.copy()
                delay = idx / dt
                t = t0 + delay
                tr_c.trim(t+2, t+25)

                # Clean up the traces a little bit
                tr_c.detrend('spline', order=2, dspline=dt)
                #tr_c.detrend("linear")
                #tr_c.detrend("demean")
                tr_c.taper(max_percentage=0.05, type="hann")
                tr_c.filter("bandpass", freqmin=2, freqmax=6)


                # Get traces amplitudes and normalize
                tr_amp     = tr_c.data
                tr_amp_nrm = tr_amp / max(abs(tr_amp))

                # Here we create three lists, one for each comp
                if tr.stats.channel == 'EHZ' or tr.stats.channel == 'HHZ':
                    #coef = np.dot(tr_amp, tp_amp_z) / np.dot(tp_amp_z,tp_amp_z)
                    tr_z_lst.append(tr_amp)
                    #tr_nrm_z_lst.append(tr_amp/coef)
                    tr_nrm_z_lst.append(tr_amp_nrm)
                    print(t)

                elif tr.stats.channel == 'EHE' or tr.stats.channel == 'HHE':
                    #coef = np.dot(tr_amp, tp_amp_e) / np.dot(tp_amp_e,tp_amp_e)
                    tr_e_lst.append(tr_amp)
                    #tr_nrm_e_lst.append(tr_amp/coef)
                    tr_nrm_e_lst.append(tr_amp_nrm)

                elif tr.stats.channel == 'EHN' or tr.stats.channel == 'HHN':
                    #coef = np.dot(tr_amp, tp_amp_n) / np.dot(tp_amp_n,tp_amp_n)
                    tr_n_lst.append(tr_amp)
                    #tr_nrm_n_lst.append(tr_amp/coef)
                    tr_nrm_n_lst.append(tr_amp_nrm)
    # Plot matches
    for comp in ['z', 'e', 'n']:

        # Make sure to get the right component
        if comp == 'z':
            tr_nrm_lst = tr_nrm_z_lst
            tr_lst = tr_z_lst

        elif comp == 'e':
            tr_nrm_lst = tr_nrm_e_lst
            tr_lst = tr_e_lst

        elif comp == 'n':
            tr_nrm_lst = tr_nrm_n_lst
            tr_lst = tr_n_lst

        # Create a np array with all the traces in tr_nrm_lst
        arr_tr = np.array(tr_nrm_lst)

        # Stack traces
        tr_amp_stk     = np.mean(tr_lst, axis=0)
        tr_amp_stk_nrm = tr_amp_stk / max(abs(tr_amp_stk[:]))

        # Plot traces + stack
        for i_tr, tr_amp_nrm in enumerate(tr_nrm_lst):
            plt.plot(tr_amp_nrm * 0.75 + i_tr, '-k', linewidth=.5)
        plt.plot(tr_amp_stk_nrm * 0.75 + i_tr + 1, '-r', linewidth=1)
        plt.title(stnm + '_' + comp)


        plt.show()
#_______________________________________________________________________________
if __name__ == "__main__":

    workdir  = './'
    outDir   = '/home/ammjb/src/rapidalignerseismic-motif_searching/data/detections_martinique/'
    stat     = 'MQ.PBO'
    templ    = '0711.15.00'

    # Create jday_lst
    jday_lst = []
    for jday in range(256,276+1):
         jday_lst.append('{:03d}'.format(jday))

    matchList = readMatchFile(outDir, stat, templ)
    PlotCleanMatches(stat, jday_lst, matchList)
