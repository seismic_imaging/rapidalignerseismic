#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy as np
import getopt
import sys,os
# from obspy.core import Stream, read
from obspy import UTCDateTime
from collections import Counter
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras

from rapidAlignerSeismic.util.Loader import SismoLoader
from obspy import UTCDateTime, read, Trace, Stream



#Define the key to sort event per dates on a given station
def getDate(event):
	return event[0][2]
	
	
#Reconstruct file name with component
def getFileName(name, comp):
	splName = name.split('.')
	splName[3] = splName[3] + comp[-1]
	fileCompName = '.'.join(splName)
	return fileCompName
	
def readConfigFile(configFile):
	global wfdir, templateDir, output_dir
	
	with open(configFile, 'r') as f:
		for line in f:
			rline="".join(line.split()).split('=')
			if len(rline) > 1:
				if rline[0] == 'length':
					length = eval(rline[1])
					assert type(length) is float or int
				elif rline[0] == 'vecComp':
					vecComp = eval(rline[1])
					assert type(vecComp) is list
				elif rline[0] == 'freqMin':
					freqMin = eval(rline[1])
					assert type(freqMin) is float or int
				elif rline[0] == 'freqMax':
					freqMax = eval(rline[1])
					assert type(freqMax) is float or int
				elif rline[0] == 'dailyDir':
					if rline[1] != '' :
						wfdir = eval(rline[1])
				elif rline[0] == 'templateDir':
					if rline[1] != '':
						templatedir = eval(rline[1])
				

	return length, vecComp, freqMin, freqMax, wfdir, templatedir



def readPNExtract(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine = "".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'phaseNetFile':
                    phaseNetFileName = eval(rLine[1])
                elif rLine[0] == 'eventsDir':
                    concatEventsDir = eval(rLine[1])
                elif rLine[0] == 'pnDir':
                    pnEventDir = eval(rLine[1])
                elif rLine[0] == 'extractEvents':
                    doExtractEvents = eval(rLine[1])
                    assert type(doExtractEvents) is bool
                elif rLine[0] == 'shift':
                    t0Shift = eval(rLine[1])
                elif rLine[0] == 'dailyLimit':
                    userDailyLimit = eval(rLine[1])
    return phaseNetFileName, concatEventsDir, pnEventDir, doExtractEvents, t0Shift, userDailyLimit
    

def getIndex(daily, stEventList):
	
	
	print(daily)
	
	
	indexes = [x for x, y in enumerate(stEventList) if y[1] == daily]
	
	return indexes
    
    
def process_daily(daily,stEventList):
	#Load daily + sampling
	print(daily)
	
	indexes = getIndex(daily, stEventList)
	print(indexes)
	dailyName = getFileName(daily, component)
	netSt, channel, yr, jd = ras.getDailyInfo(dailyName)
	network, station = netSt.split('.')
	dailySig, dailySr, dailyTbeg, dailyTend, location= SismoLoader(wfdir + '/' + dailyName, freqMin, freqMax).data
	
	sizePt = int(length * dailySr)
	offset = 0
	stShift = int(t0Shift * dailySr)
	
	totCount = indexes[0]
	
	#Special care for first and last event of the day because of possible overlap accross days
	dailySig, offset, isDailyExtended = ras.handlingBorderEvents(dailySig, yesterdaySig, stEventList[totCount], stEventList[totCount+countEvent.get(daily)-1], dailyTbeg, dailyTend, dailySr, dailyName, wfdir, offset, stShift, sizePt, freqMin, freqMax)
	
	#Loop over events for this day 
	for ev in range(countEvent.get(daily)):
		idEv = int((stEventList[totCount][2] - dailyTbeg) * dailySr + offset - stShift)
		data = dailySig[idEv:idEv+sizePt]
		# Fill header attributes
		stats = {'network': network, 'station': station, 'location': location,
		'channel': channel, 'npts': len(data), 'sampling_rate': dailySr,
		'mseed': {'dataquality': 'D'}}
		
		st = Stream([Trace(data=data, header=stats)])
		# write as MSEED file
		st.write(templateDir+'/'+network+'.'+station+'.00.'+channel+'.D.'+ stEventList[totCount][0][-1].replace('-','.'), format='MSEED')  
		totCount += 1

    
def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print("-h  | --help						Print the usage message")
    print("-r  | --ras_config <config_file> To use a specific configuration file. By default '%s'"%(configFile))
    sys.exit()    
    
    

if __name__ == '__main__':
	#Configuration
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hr:",["help","ras_config="])
    except getopt.GetoptError:
        print(sys.argv[0]+' -r <configFile> ')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h","--help"):
            printUsageAndExit(sys.argv[0])
        elif opt in ("-r", "--ras_config"):
            configFile = arg
            
            
    print(configFile)        
            
    length, vecComp, freqMin, freqMax, wfdir, templateDir = readConfigFile(configFile)  
    phaseNetFileName, concatEventsDir, pnEventsDir, doExtractEvents, t0Shift, userDailyLimit = readPNExtract(configFile)
          
    pnDetect = np.genfromtxt(phaseNetFileName, dtype='str', skip_header=1, usecols=(0, 1, 2), delimiter=",")
    resInfo = pnDetect[:,0] # Contain info on every station, network : station.network..HH.D
    eventInfo = []
    
    
    if not os.path.isdir(templateDir):
        os.mkdir(templateDir)

    for i in range(len(pnDetect[:,2])):
        try:
            eventInfo.append(UTCDateTime(pnDetect[i,2])) # Contain info on every phasenet detection time: YYYY-MM-DDTHH:MM:ss.xxxxxxZ obspy format
        except:
            eventInfo.append('NAN')
    
    #Identify list stations to process
    uniqStat = list(set(resInfo))
    uniqStat.sort()

    #Don't know if this is because on results concatenation in working file or not, so in case
    if 'seedid' in uniqStat:
        uniqStat.remove('seedid')


    #If there is any duplicates for a station, we need to consider a special treatments
    eventList      = []
    duplicatesList = []  
    
    #For each station, for each component, extract signal detected via phasenet
    for i in range(len(uniqStat)):
        stEventList   = []
        duplEventList = []
        dateList = []
        #Identify and remove duplicate
        for j in range(len(resInfo)):
            if resInfo[j] == uniqStat[i]:
                #Duplicate filter
                if(pnDetect[j][2] in dateList):
                    duplEventList.append(pnDetect[j])
                else:
                    # get the event: PhaseNet detection, sismo file name, event time in second
                    stEventList.append([pnDetect[j],
                    #resInfo[j].replace('00', '') + '.' + str(eventInfo[j].year) + '.' + str(eventInfo[j].julday), eventInfo[j]])
                    resInfo[j] + '.' + str(eventInfo[j].year) + '.' + str(eventInfo[j].julday).zfill(3), eventInfo[j]])
                dateList.append(pnDetect[j][2])
        stEventList.sort(key=getDate)
        
        dateEventList = []
        for j in range(len(stEventList)):
            dateEventList.append(stEventList[j][1])

        #Count the amount of unique events detected per day for a given station
        countEvent = dict(Counter(dateEventList))
        
        #Loop over component defined in vecComp
        for component in vecComp:
            totCount = 0
            yesterdaySig = np.array([])
            dailyCount = 0


            #Loop over days
            for daily in countEvent.keys():

                process_daily(daily, stEventList)
                

