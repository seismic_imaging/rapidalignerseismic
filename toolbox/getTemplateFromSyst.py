#!/usr/bin/env python
#-*- coding: utf-8 -*-

## Import
import numpy as np
import getopt
import sys,os
# from obspy.core import Stream, read
from obspy import UTCDateTime
from collections import Counter
try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras

from rapidAlignerSeismic.util.Loader import SismoLoader
from obspy import UTCDateTime, read, Trace, Stream
import pandas as pd


def getExtractInfo(configFile):
	with open(configFile, 'r') as f:
		for line in f:
			rLine = "".join(line.split()).split('=')
			if len(rLine) > 1:
				if rLine[0] == 'station_list':
					stationListFile = eval(rLine[1])
				elif rLine[0] == 'catalogue':
					catalogueFile	 = eval(rLine[1])
				elif rLine[0] == 'vecComp':
					vecComp = eval(rLine[1])
					assert type(vecComp) is list
				elif rLine[0] == 'dailyDir':
					if rLine[1] != '' :
						wfdir = eval(rLine[1])
				elif rLine[0] == 'freqMin':
					freqMin = eval(rLine[1])
					assert type(freqMin) is float or int
				elif rLine[0] == 'freqMax':
					freqMax = eval(rLine[1])
					assert type(freqMax) is float or int
				elif rLine[0] == 'length':
					length = eval(rLine[1])
					assert type(length) is float or int
				elif rLine[0] == 'templateDir':
					if rLine[1] != '':
						templateDir = eval(rLine[1])
			
	return stationListFile, catalogueFile, vecComp, wfdir, freqMin, freqMax, length, templateDir



if __name__ == '__main__':
	#Configuration
	try:
		opts, args = getopt.getopt(sys.argv[1:],"hr:",["help","ras_config="])
	except getopt.GetoptError:
		print(sys.argv[0]+' -r <configFile> ')
		sys.exit(2)
	
	for opt, arg in opts:
		if opt in ("-h","--help"):
			printUsageAndExit(sys.argv[0])
		elif opt in ("-r", "--ras_config"):
			configFile = arg
			
	
	stationListFile, catalogueFile, vecComp, wfdir, freqMin, freqMax, length, templateDir = getExtractInfo(configFile)
	
	
	stationList = np.genfromtxt(stationListFile, dtype='str', usecols=(0), delimiter=" ")
	
	catalogue = np.genfromtxt(catalogueFile, dtype='str', usecols=(1), delimiter=" ")

	#sort event to avoid date mismatch later on
	catalogue.sort()

	try:
		for station in stationList:
			print("Stations : ", station)
	except:
		stationList=np.append(stationList, '??')
    
	fileList = os.listdir(wfdir)
    
	t0Catalogue = []
	dateList=[]

	for i in range(len(catalogue)):
		t0Catalogue.append(UTCDateTime(catalogue[i]))
		dateList.append(str(t0Catalogue[i].year) + '.' + str(t0Catalogue[i].julday).zfill(3))
		
	countDate=dict(Counter(dateList))
	
	
	for comp in vecComp:
		
		for station in stationList:
			dailyList = [f for f in fileList if station in f and comp in f]
			
			totCount = 0
			for date in countDate.keys():
			
				dailyName = [f for f in dailyList if date in f]
				
				#handle possible issues
				if len(dailyName) > 1:
					print("Several files ", dailyName, "are found, it's not supposed to happen")
					sys.exit()
				elif len(dailyName) < 1:
					print("No file found for station ", station, " and date", date)
					continue
				
				
				#Load daily
				#Get daily info!!
				dailySig, dailySr, dailyTbeg, dailyTend, location= SismoLoader(wfdir + '/' + dailyName[0], freqMin, freqMax).data
				ntwkSt, channel, year, jday = ras.getDailyInfo(dailyName[0])
				sizePt = int(length * dailySr)
				
				for ev in range(countDate.get(date)):
					#get t0Catalogue[totCount] and extract
					idEv = int((t0Catalogue[totCount] - dailyTbeg) * dailySr)
					
					data = dailySig[idEv:idEv+sizePt]
					stats = {'network': ntwkSt.split('.')[0], 'station': station, 'location': location,'channel': channel, 'npts': len(data), 'sampling_rate': dailySr,'mseed': {'dataquality': 'D'}}
					
					st = Stream([Trace(data=data, header=stats)])
					# write as MSEED file
					st.write(templateDir+'/'+dailyName[0]+'.T'+ f"{t0Catalogue[totCount].hour:02}" + ':' + f"{t0Catalogue[totCount].minute:02}" + ':' + f"{t0Catalogue[totCount].second:02}" +'.'+ f"{t0Catalogue[totCount].microsecond:04}"[:4], format='MSEED')
					
					totCount = totCount + 1
						
					
    
	
	
