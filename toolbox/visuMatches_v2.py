#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import glob, os

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from obspy.core import Stream, read
from obspy import UTCDateTime
from obspy.io.xseed import Parser

from datetime import timedelta
import pandas as pd

def readMatchFile(outDir, stat, templList):
    
    matchList = []
    
    for templ in templList:
        os.system('cat '+outDir+'/'+stat+'*'+templ+'* > '+outDir+'/matches.'+stat+templ)
        matches = []
        with open(outDir+'/matches.'+stat+templ) as matchFile:
            for line in matchFile:
                matchUTC  = line.split()[0]
                matchCorr = line.split()[1]
                matches.append((matchUTC, matchCorr))

        matchList.append(matches)

    return matchList

def readAndCleanMatchFile(outDir, workdir, stat, templList, uniqTemplDate):

    matchList = []
    matchDataF= []
    
    print("read and clean matches")
    
    for templ in templList:

        print(templ)

        split = templ[1:-1].split('.T')
        splitR = [split[0].replace('.', '-'), split[1]]

        templDate = UTCDateTime('T'.join(splitR))

        os.system('cat '+outDir+'/'+stat+'*'+templ+'* > '+workdir+'/matches.'+stat+templ)
        matches = []

        matchTempl = stat+templ
        clean = open(workdir+'/cleanMatches.'+stat+templ,"w")
        
        with open(workdir+'/matches.'+stat+templ) as matchFile:
            for line in matchFile:
                if (abs(templDate - UTCDateTime(line.split()[0])) > 2000):
                    count = 0
                    for j in range(len(uniqTemplDate)):
#                        print("date template : ", uniqTemplDate[j]," date event: ", UTCDateTime(line.split()[0])," diff : ", abs(uniqTemplDate[j] - UTCDateTime(line.split()[0])))
                        if (abs(uniqTemplDate[j] - UTCDateTime(line.split()[0])) < 2000):
                            count = 1
#                    print("count = ",count)
                    if count == 1:
                        line = line[:-1] + ' 0' + line[-1]
                    else:
                        line = line[:-1] + ' 1' + line[-1]

                    matchUTC  = line.split()[0]
                    matchCorr = line.split()[1]
                    isNew     = line.split()[2]
                    matches.append((matchTempl, matchUTC, matchCorr,isNew))
        
#        clean.close()

        #Remove duplicates from list
        i=0
        j=0
        dupls = []
        print('len(matches):', len(matches))
        while i<len(matches)-1:
            print('i=',i)
            Test = 1
            j = i+1
            while Test ==1 and j<len(matches):
#                for j in range(i+1, len(matches)):
                print('j=',j)
                if abs(UTCDateTime(matches[i][1])-UTCDateTime(matches[j][1])) <= 1000:
                    if matches[i][2] > matches[j][2]:
                        dupls.append(matches[j])
                    elif matches[i][2] < matches[j][2]:
                        dupls.append(matches[i])
                    elif matches[i][2] == matches[j][2]:
                         dupls.append(matches[j])
                else:
                     Test = 0
                     j = j-1
                j = j+1
            i = j

        matchesClean=[]
        for match in matches:
 #           print(match)
            if match not in dupls:
                matchesClean.append(match)
                matchDataF.append(match)

        for k in range(len(matchesClean)):
            line = matchesClean[k][1]+' '+matchesClean[k][2]+' '+matchesClean[k][3]
            print('line = ',line)
            clean.write(line+'\n')
        clean.close()

        print(matches, len(matches))
        print(matchesClean, len(matchesClean))
        print(dupls, len(dupls))

        matchList.append(matchesClean)

    return matchList, matchDataF

def getBestTemplateForEvent(matchDF):

    # Convert list to data frame
    frameMatch = pd.DataFrame(matchDF, columns=['Template', 'Event', 'Correlation', 'isNew'])

    # Reorganise dataframe according to events detected
    frameMatch.sort_values('Event', inplace=True)

    # Compute difference between 2 consecutives events to identify identical events
    frameMatch['delta_Time'] = pd.to_datetime(frameMatch['Event']).diff()

    evName = frameMatch.iloc[0, 1]
    cclean = [evName]
    for i in range(len(frameMatch[1:])):
        if (frameMatch.iloc[i + 1, 4].total_seconds() < 3):
            cclean.append(evName)
        else:
            evName = frameMatch.iloc[i + 1, 1]
            cclean.append(evName)

    frameMatch['eventClean'] = pd.Series(cclean).values

    isMax = [0]*len(frameMatch)
    frameMatch=frameMatch.assign(isCorrMax=isMax)

    # Extract list of events detected and get unique events (i.e. detected in the same second)
    events = frameMatch["eventClean"].to_list()

    uniqEv = list(set(events))

    #For each event detected identify the template with max correlation
    for unEv in uniqEv:
        evCorrMax = frameMatch[frameMatch['eventClean'].str.contains(unEv)].query('Correlation == Correlation.max()')
        frameMatch.loc[evCorrMax.index,'isCorrMax']=1
        #print(evCorrMax)

    cleanFrame = frameMatch[frameMatch["isNew"] == "1"]

    return frameMatch, cleanFrame


def PlotCleanMatches(stat, jday_lst, mtc_lst_clean, uniqTempl):
    tr_z_lst, tr_e_lst, tr_n_lst             = [], [], [] # Init. list of traces
    tr_nrm_z_lst, tr_nrm_e_lst, tr_nrm_n_lst = [], [], [] # Init. list of norm. traces
    #idxs = []
    print('Plotting clean matches for station', stat, ' template ', uniqTempl)

    if mtc_lst_clean != [] :

        new = 0
        for item in mtc_lst_clean:
            if item[3] == '1':
                new = 1

        if new == 1:
            #Will need to change this to be more flexible
            tempE = read(workdir+'../data/template_arette/FR/FULL/'+stat+'.*.?HE.D'+uniqTempl[:-1])
            tempN = read(workdir+'../data/template_arette/FR/FULL/'+stat+'.*.?HN.D'+uniqTempl[:-1])
            tempZ = read(workdir+'../data/template_arette/FR/FULL/'+stat+'.*.?HZ.D'+uniqTempl[:-1])


            subLabel = []
            corrs = []
            isNew = []

            for jday in jday_lst:
                idxs = []
        #        corrs = []
                for item in mtc_lst_clean:
                    t = UTCDateTime(item[1])

                    # Make sure only to take idx corresponding to jday
                    if int(jday) == t.julday and item[3] == '1':
                        t_h0 = UTCDateTime(year=t.year, julday=t.julday, hour=0, minute=0)
                        idx  = int((t - t_h0) * 100) # TODO sr (100Hz) must be a soft value
                        #print(t, t_h0, idx)
                        idxs.append(idx)
                        corrs.append(item[2])
                        isNew.append(item[3])


                # Extract matches
                st = Stream()

                # load trace in stream: raises error if trace doesn't exist
                try:
                    st += read(workdir+'../data/wf_arette/FR/'+stat+'.*.?H?.D.2022.' + jday)
                except Exception:
                    print(workdir+'../data/wf_arette/FR/'+stat+'.*.?H?.D.2022.' + jday)
                    continue

                st.merge(fill_value=0)

                t0   = st[0].stats.starttime
                dt   = st[0].stats.sampling_rate
                stnm = st[0].stats.station

                for tr in st:
                    for tr_id, idx in enumerate(idxs):
                        # Make a trace copy, estimate delay and trim
                        tr_c = tr.copy()
                        delay = idx / dt
                        t = t0 + delay
                        tr_c.trim(t+0, t+15)

                        # Clean up the traces a little bit
                        tr_c.detrend('spline', order=2, dspline=dt)
                        #tr_c.detrend("linear")
                        #tr_c.detrend("demean")
                        tr_c.taper(max_percentage=0.05, type="hann")
                        tr_c.filter("bandpass", freqmin=4, freqmax=20)


                        # Get traces amplitudes and normalize
                        tr_amp     = tr_c.data
                        tr_amp_nrm = tr_amp / max(abs(tr_amp))

                        # Here we create three lists, one for each comp
                        if tr.stats.channel == 'EHZ' or tr.stats.channel == 'HHZ':
                            #coef = np.dot(tr_amp, tp_amp_z) / np.dot(tp_amp_z,tp_amp_z)
                            tr_z_lst.append(tr_amp)
                            #tr_nrm_z_lst.append(tr_amp/coef)
                            tr_nrm_z_lst.append(tr_amp_nrm)
                            subLabel.append(t)
                            print(t)

                        elif tr.stats.channel == 'EHE' or tr.stats.channel == 'HHE':
                            #coef = np.dot(tr_amp, tp_amp_e) / np.dot(tp_amp_e,tp_amp_e)
                            tr_e_lst.append(tr_amp)
                            #tr_nrm_e_lst.append(tr_amp/coef)
                            tr_nrm_e_lst.append(tr_amp_nrm)

                        elif tr.stats.channel == 'EHN' or tr.stats.channel == 'HHN':
                            #coef = np.dot(tr_amp, tp_amp_n) / np.dot(tp_amp_n,tp_amp_n)
                            tr_n_lst.append(tr_amp)
                            #tr_nrm_n_lst.append(tr_amp/coef)
                            tr_nrm_n_lst.append(tr_amp_nrm)
            # Plot matches
            fig = plt.figure()

        #    print(subLabel[0].julday)
        #    print(corrs)
        #    print(str(subLabel[0]) +' '+ corrs[0])

            fig.suptitle(stat + uniqTempl)
            count = 0
            for comp in ['z', 'e', 'n']:

                count = count +1

                # Make sure to get the right component
                if comp == 'z':
                    tr_nrm_lst = tr_nrm_z_lst
                    tr_lst = tr_z_lst
                    tr_templ = tempZ[0].data/ max(abs(tempZ[0].data))

                elif comp == 'e':
                    tr_nrm_lst = tr_nrm_e_lst
                    tr_lst = tr_e_lst
                    tr_teml = tempE[0].data/ max(abs(tempE[0].data))

                elif comp == 'n':
                    tr_nrm_lst = tr_nrm_n_lst
                    tr_lst = tr_n_lst
                    tr_templ = tempN[0].data/ max(abs(tempN[0].data))

                # Create a np array with all the traces in tr_nrm_lst
                arr_tr = np.array(tr_nrm_lst)

                # Stack traces
                tr_amp_stk     = np.mean(tr_lst, axis=0)
                tr_amp_stk_nrm = tr_amp_stk / max(abs(tr_amp_stk[:]))

                # Plot traces + stack
                if count == 1:
                    ax = fig.add_subplot(1,3,count) #TO DO replace 3 by len(vec)
                else:
                    ax = fig.add_subplot(1,3,count,sharey=ax, sharex=ax)#TO DO replace 3 by len(vec)
                j=0
                for i_tr, tr_amp_nrm in enumerate(tr_nrm_lst):
                    print("isNew : ",isNew[j])
                    if isNew[j] == '1':
                        ax.plot(tr_amp_nrm * 0.75 + i_tr, '-k', linewidth=.5, label=str(subLabel[j])+' '+str(subLabel[j].julday)+' '+str(corrs[j]))
                    #else:
                    #    ax.plot(tr_amp_nrm * 0.75 + i_tr, '-k', linewidth=.5, label=str(subLabel[j])+' '+str(subLabel[j].julday)+' '+str(corrs[j]))
                    j = j+1
                ax.plot(tr_amp_stk_nrm * 0.75 + i_tr + 1, '-g', linewidth=1, label='Normalized stack')
                ax.plot(tr_templ * 0.75 + i_tr + 2, '-r', linewidth=1)
                ax.set_title(stnm + '_' + comp)
                handles, labels = ax.get_legend_handles_labels()

            fig.legend(handles[::-1], labels[::-1], loc='right')
            plt.subplots_adjust(right=0.85)

            #plt.show()

            plt.savefig(stat + uniqTempl+'.png', dpi=1000)
            plt.close()
        else:
            print("No new detection")
    else:
        print("No clean match")


#_______________________________________________________________________________
if __name__ == "__main__":

    workdir  = './'
    outDir   = '/home/plab/rapidAlignerSeismic/data/hits_template_arette_4-20/' #/home/ammjb/src/rapidalignerseismic-motif_searching/data/detections_martinique/'
    stat     = 'FR.ATE' #MQ.PBO'
    templ    = [] #'.2022.158.T9:31:37.8000.'] #'2022.169.T1:30:3.80000' #0711.15.00'

    #List all hits files for station 'stat'
    fileList = os.listdir(outDir)
    fileListStat = [file for file in fileList if stat in file]
    for file in fileListStat:
        templ.append(file.split(stat)[1])
 
    uniqTempl=list(set(templ))


    #Convert template info in date
    uniqTemplDate = []
    for i in range(len(uniqTempl)):
        split = uniqTempl[i][1:-1].split('.T')
        splitR = [split[0].replace('.','-'), split[1]]
   #     print(splitR)
        uniqTemplDate.append(UTCDateTime('T'.join(splitR)))


    # Create jday_lst
    jday_lst = []
    #Here don't forget to add +1
    for jday in range(141,169+1): #256,276+1):
         jday_lst.append('{:03d}'.format(jday))

#    matchList = readMatchFile(outDir, stat, uniqTempl)

    matchList, matchDF = readAndCleanMatchFile(outDir, workdir, stat, uniqTempl, uniqTemplDate)

    frameMatch, cleanFrame = getBestTemplateForEvent(matchDF)
    frameMatch.to_csv(workdir+'/'+stat+'.dataBase.csv')
    cleanFrame.to_csv(workdir+'/newOnly.'+stat+'.dataBase.csv')

    for i in range(len(matchList)):
        PlotCleanMatches(stat, jday_lst, matchList[i], uniqTempl[i])

