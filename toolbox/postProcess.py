#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import matplotlib.pyplot as plt
import numpy as np
import cupy as cp
import sys, os, glob
import getopt

from scipy.cluster import hierarchy
from scipy.spatial import distance
from obspy.core import UTCDateTime,Stream, read
import pandas as pd
from scipy.ndimage import gaussian_filter1d
from scipy import signal
import obspy.signal

try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras


def readConfigFile(configFile):

    with open(configFile, 'r') as f:
        for line in f:
            rLine = "".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'length':
                    length = eval(rLine[1])
                    assert type(length) is float or int
                elif rLine[0] == 'dailyDir':
                    wfdir = eval(rLine[1])
                elif rLine[0] == 'vecComp':
                    vecComp = eval(rLine[1])
                    assert type(vecComp) is list
                elif rLine[0] == 'freqMin':
                    freqMin = eval(rLine[1])
                    assert type(freqMin) is float or int
                elif rLine[0] == 'freqMax':
                    freqMax = eval(rLine[1])
                    assert type(freqMax) is float or int
                elif rLine[0] == 'netStat':
                    netStat = eval(rLine[1])
                elif rLine[0] == 'concatenateMatches':
                    matchFile = eval(rLine[1])
                elif rLine[0] == 'cleanedEventFile':
                    eventsFile = eval(rLine[1])
                elif rLine[0] == 'consecTimeWindowsFile':
                    tWindowsFile = eval(rLine[1])
                elif rLine[0] == 'linkageFile':
                    linkageFileName = eval(rLine[1])
                elif rLine[0] == 'covariancematrixFile':
                    CovMatFileName = eval(rLine[1])

    return length, freqMin, freqMax, wfdir, vecComp, netStat, matchFile, eventsFile, tWindowsFile, linkageFileName, CovMatFileName



def ReadMatches(matchFile):
    matchList = []
    with open(matchFile, 'r') as f:
        for line in f:
            numThread   = eval(line.split()[0])
            patternTime = line.split()[1]
            dailyTime   = line.split()[2]
            corr        = eval(line.split()[3])
            if corr >= 0.3:
                matchList.append([numThread, patternTime, dailyTime, corr])

    print('Total matches:', len(matchList))
    return matchList

def sortMatches(matchList, patternLen, tWindowsFile):

    #convert MatchList to dataframe
    frameMatch = pd.DataFrame(matchList, columns=['Thread_id','Template', 'Event', 'Correlation'])

    #split data frame according to thread id
    th0 = frameMatch[frameMatch["Thread_id"] == 0]
    th1 = frameMatch[frameMatch["Thread_id"] == 1]

    #count template occurence in each
    th0["Count"] = th0["Template"].apply(lambda x :(th0["Template"] == x).sum())
    th1["Count"] = th1["Template"].apply(lambda x :(th1["Template"] == x).sum())

    # get only "templates" with 3 detections (assuming 3 components detection)
    events = pd.concat([th1[th1["Count"]==3],th0[th0["Count"]==3]])
    #events.sort_values('Correlation', inplace=True)

    #get max correlation after sorting to correlation
    eventsClean = events.sort_values('Correlation').drop_duplicates(subset=['Template'], keep='last')
    eventsClean.sort_values('Template', inplace=True)

    #For each unique "template" get the one with max correlation
    templ = eventsClean['Template'].to_list()
    corr = eventsClean['Correlation'].to_list()

    #First pass to remove templates matched on both pass
    templOne = []
    count = 0
    #while count < len(templ)-1:
    #    diff = UTCDateTime(templ[count+1]) - UTCDateTime(templ[count])
    #    #We keep the best of the two detections
    #    if diff == patternLen/2:
    #        if corr[count+1] > corr[count]:
    #            templOne.append(templ[count+1])
    #            count = count +2
    #        else:
    #            templOne.append(templ[count])
    #            count = count+2
    #    else:
    #        templOne.append(templ[count])
    #        if count == len(templ)-2:
    #            templOne.append(templ[count+1])
    #        count = count +1

    templOne = templ

    #Second pass to remove and track when a template is seen accross several time windows
    templClean = []
    timeWindows = []
    j=0
    count = 0
    while count < len(templOne) - 1:
        diff = UTCDateTime(templOne[count + 1]) - UTCDateTime(templOne[count])
        timeWindows.append(1)
        if diff > patternLen:
            templClean.append(templOne[count])
            if count == len(templOne)-2:
                templClean.append(templOne[count+1])
                timeWindows.append(1)
            count = count +1
        elif diff <= patternLen:
            if count < len(templOne) -2:
                ncount = 1
                diff1 = UTCDateTime(templOne[ncount + count + 1]) - UTCDateTime(templOne[ncount+count])
                timeWindows[j] = timeWindows[j] + 0.5
                while diff1 <= patternLen and (count + ncount) < len(templOne)-2:
                    ncount = ncount + 1
                    diff1 = UTCDateTime(templOne[ncount + count + 1]) - UTCDateTime(templOne[ncount+ count ])
                    timeWindows[j] = timeWindows[j]+0.5
                templClean.append(templOne[count])
                count = ncount + count + 1
            else:
                templClean.append(templOne[count])
                timeWindows[j] = timeWindows[j] + 0.5
                count = count +1
        j = j + 1

    #Store in a data frame the templates and the amount of consecutive time windows they have been seen
    tWindowsDF = pd.DataFrame({"Template":templClean, "tWindows":timeWindows})

    #Now get the list of "events" detected per "template"
    dailyDF = pd.DataFrame(columns=["Thread_id","Template","Event","Correlation","Count"])
    for temp in templClean:
        dailyDF = dailyDF.append(events[events['Template'].str.contains(temp)], ignore_index=True)

    dailyTime = dailyDF['Event'].to_list()
    dailyTimeList = list(set(dailyTime))
    dailyTimeList.sort()

    templateList = dailyTimeList + templClean
    templateList.sort()

    count = 0
    j=0
    templateCleanList = []
    timeWindows = []
    while count < len(templateList) - 1:
        diff = UTCDateTime(templateList[count + 1]) - UTCDateTime(templateList[count])
        timeWindows.append(1)
        if diff > patternLen:
            templateCleanList.append(templateList[count])
            if count == len(templateList)-2:
                templateCleanList.append(templateList[count+1])
                timeWindows.append(1)
            count = count +1
        elif diff <= patternLen:
            ncount = count
            diff1 = UTCDateTime(templateList[ncount + 1]) - UTCDateTime(templateList[ncount])
            timeWindows[j] = timeWindows[j] + 0.5
            while diff1 <= patternLen and (ncount < len(templateList) - 2):
                ncount = ncount + 1
                diff1 = UTCDateTime(templateList[ncount +  1]) - UTCDateTime(templateList[ncount ])
                timeWindows[j] = timeWindows[j] + 0.5
            templateCleanList.append(templateList[count])
            count = ncount + 1
        j = j+1

    print('Match on 3 components:', len(templateCleanList))

    #Merge the two DF containing info on consecutive time windows
    nTWindowsDF = pd.DataFrame({"Template": templateCleanList, "tWindows": timeWindows})
    newDF= pd.merge(tWindowsDF, nTWindowsDF, on=["Template"], how="outer", indicator=True)
    newDF["consecTWindows"] = newDF.sum(axis=1)
    #Avoid to count twice a same event
    newDF.loc[newDF["_merge"] == "both", "consecTWindows"] -= 1
    #Delete construction column
    newDF = newDF.drop(columns=["tWindows_x", "tWindows_y", "_merge"])

    newDF.to_csv(tWindowsFile)


    return sorted(templateCleanList), newDF #templateList)

def getTemplateTrimed(template, newTbeg, dailyDir, netStat, comp, patternLen, fmin, fmax, isSyst):

    # Get Julday
    jday = UTCDateTime(template).julday
    year = UTCDateTime(template).year

    print(jday)
    print(dailyDir+netStat+'*'+comp+'*'+str(year)+'.'+'{:03d}'.format(jday))

    templCut = read(dailyDir+netStat+'*'+comp+'*'+str(year)+'.'+'{:03d}'.format(jday))

    templCut.merge(fill_value=0)

    # Extract template
    templateTbeg = newTbeg #UTCDateTime(template)

    #Warning BP
    if isSyst == True:
        templateTend = templateTbeg + 2 * patternLen
    else:
        templateTend = templateTbeg + patternLen


    # First we check in case the detection was noise ouside of the daily data recorded
    if templateTbeg >= templCut[0].stats.endtime or templateTend < templCut[0].stats.starttime:
        templCut.trim(templCut[0].stats.starttime, templCut[0].stats.starttime+1.0)
    else:
        # Here we filter and clean up the stream and estimate the position of
        # peak amplitude. Then shift the template to the time of max amplitude
        templCut.detrend("linear")
        templCut.detrend("demean")
        templCut.taper(max_percentage=0.0001, type="hann")
        #templCut.merge(fill_value=0)
        templCut.filter("bandpass", freqmin=fmin, freqmax=fmax)
        templCut.trim(templateTbeg, templateTend)

    print("patterLen = ", patternLen)
    print("len templ = ", len(templCut[0].data))
    print("Sr = ", templCut[0].stats.sampling_rate)

    return templCut[0].data.astype('float64'), templCut[0].stats.sampling_rate, templCut[0].stats.channel, templateTbeg

# Function to find the max amplitude value on the 3 components and shift the traces around this value
def getMaxAmpT0(template, dailyDir, netStat, comp, patternLen, fmin, fmax, tWindows):
    templStream = Stream()
    jday = UTCDateTime(template).julday
    templStream += read(dailyDir+netStat+'*'+comp[0][:-1]+'*'+'{:03d}'.format(jday))

    templStream.merge(fill_value=0)
    templStream.taper(max_percentage=0.0001, type="hann")
    templStream.filter("bandpass", freqmin=fmin, freqmax=fmax)

    # Extract template
    templateTbeg = UTCDateTime(template)
    templateTend = templateTbeg + tWindows * patternLen
    templStream.trim(templateTbeg, templateTend, pad=True, nearest_sample = False, fill_value=0)

    # find the max amplitude value on the 3 components and shift the traces around this value
    # ampMaxE = np.amax(abs(templStream[0].data))
    # ampMaxN = np.amax(abs(templStream[1].data))
    # ampMaxZ = np.amax(abs(templStream[2].data))
    # if np.amax([ampMaxE, ampMaxN, ampMaxZ]) == ampMaxE:
    #     idxAmpMax = np.argmax(abs(templStream[0].data))
    # elif np.amax([ampMaxE, ampMaxN, ampMaxZ]) == ampMaxN:
    #     idxAmpMax = np.argmax(abs(templStream[1].data))
    # else:
    #     idxAmpMax = np.argmax(abs(templStream[2].data))

    # Look for maximum amplitude in signal envelope
    envelE = gaussian_filter1d(obspy.signal.filter.envelope(templStream[0].data),200)
    envelN = gaussian_filter1d(obspy.signal.filter.envelope(templStream[1].data), 200)
    envelZ = gaussian_filter1d(obspy.signal.filter.envelope(templStream[2].data), 200)

    ampMaxE = np.amax(abs(envelE))
    ampMaxN = np.amax(abs(envelN))
    ampMaxZ = np.amax(abs(envelZ))

    if np.amax([ampMaxE, ampMaxN, ampMaxZ]) == ampMaxE:
        idxAmpMax = np.argmax(abs(templStream[0].data))
    elif np.amax([ampMaxE, ampMaxN, ampMaxZ]) == ampMaxN:
        idxAmpMax = np.argmax(abs(templStream[1].data))
    else:
        idxAmpMax = np.argmax(abs(templStream[2].data))


    ampMaxTime = templateTbeg + idxAmpMax / templStream[0].stats.sampling_rate - (patternLen) #/2.0)

    return ampMaxTime

def extractTemplates(templateList, netStat, vecComp, dailyDir, patternLen, fmin, fmax, eventsFileName, tWindowsDF):

    templListStream = [] #Stream()
    templListSr     = []
    templListCh     = []
    templListTbeg   = []
    templListLen    = []
    isSyst = True

    for template in templateList:

        tWindows = tWindowsDF[tWindowsDF['Template']==template]['consecTWindows'].values[0]

        newTbeg = getMaxAmpT0(template, dailyDir, netStat, vecComp, patternLen, fmin, fmax, tWindows)

        for comp in vecComp:
            templCut, templSr, templCh, templTbeg = getTemplateTrimed(template, newTbeg, dailyDir, netStat, comp, patternLen, fmin, fmax, isSyst)
            templCut_gpu = cp.asarray(templCut)
            templListStream.append(templCut_gpu)
            templListSr.append(templSr)
            templListCh.append(templCh)
            templListTbeg.append(templTbeg)
            templListLen.append(len(templCut))

    print("len templList : ",len(templListStream))

    #templList = [templListStream, templListSr, templListCh, templListTbeg, templListLen]
    templatesDF = pd.DataFrame({"Data":templListStream, "Sr":templListSr, "Channel":templListCh, "Tbeg":templListTbeg, "DataLen":templListLen})
    templatesDF.to_csv('templList.csv', index=True)

    # Reload dataframe because of some issues otherwise due to format
    templatesDF = pd.read_csv('templList.csv')

    templatesDF_Trash = templatesDF[templatesDF["DataLen"] != (2*patternLen)*templSr+1]

    templatesDF_Trash.to_csv('templList_trash.csv', index=False)

    templatesDF = pd.merge(templatesDF, templatesDF_Trash, on=["Tbeg"], how="outer", indicator=True)

    templatesDF = templatesDF.loc[templatesDF["_merge"] == "left_only"].drop("_merge", axis=1)

    # Make sure we have events on 3 components after cleanup
    templatesDF["Count"] = templatesDF["Tbeg"].apply(lambda x :(templatesDF["Tbeg"] == x).sum())

    templatesDF = templatesDF[templatesDF["Count"]==3]

    templatesDF.to_csv('templListCl_after.csv', index=False)

    templListStreamCL1 = [] #Stream()
    templListStreamCL2 = []
    templListStreamCL3 = []
    templListSrCL1     = []
    templListSrCL2     = []
    templListSrCL3     = []
    #templListChCL     = []
    templListTbegCL1   = []
    templListTbegCL2   = []
    templListTbegCL3   = []

    count = 0

    for index in templatesDF["Unnamed: 0_x"]:
        if templatesDF.iloc[count]["Channel_x"] == vecComp[0]:
            templListStreamCL1.append(templListStream[index])
            templListSrCL1.append(templListSr[index])
            #templListChCL.append(templListCh[index])
            templListTbegCL1.append(templListTbeg[index])
            print(templListTbeg[index], templatesDF.iloc[count]["Tbeg"], templListCh[index], templatesDF.iloc[count]["Channel_x"])
        elif templatesDF.iloc[count]["Channel_x"] == vecComp[1]:
            templListStreamCL2.append(templListStream[index])
            templListSrCL2.append(templListSr[index])
            templListTbegCL2.append(templListTbeg[index])
        elif templatesDF.iloc[count]["Channel_x"] == vecComp[2]:
            templListStreamCL3.append(templListStream[index])
            templListSrCL3.append(templListSr[index])
            templListTbegCL3.append(templListTbeg[index])
        count += 1

    iCount = -1
    covMatrixE = []
    covMatrixN = []
    covMatrixZ = []
    eventsFile = open(eventsFileName, "w")
    for i in range(len(templListStreamCL1)):
        jCount = -1
        ccVecZ = []
        ccVecN = []
        ccVecE = []
        print(i)
        iCount = iCount + 1
        for j in range(len(templListStreamCL1)):
            jCount = jCount + 1
            #dist1, corr1 = ras.ED.zdist(templListStreamCL1[i], templListStreamCL1[j], mode="fft")
            #ccE = float(cp.asnumpy(cp.amax(corr1) / len(templListStreamCL1[i])))
            corr1 = signal.correlate(cp.asnumpy(templListStreamCL1[i]), cp.asnumpy(templListStreamCL1[j]), mode="full", method="fft")
            ccE = float(np.amax(corr1) / (cp.std(templListStreamCL1[i])*cp.std(templListStreamCL1[j])*len(templListStreamCL1[i])))
            ccVecE.append(ccE)
            #dist2, corr2 = ras.ED.zdist(templListStreamCL2[i], templListStreamCL2[j], mode="fft")
            #ccN = float(cp.asnumpy(cp.amax(corr2) / len(templListStreamCL2[i])))
            corr2 = signal.correlate(cp.asnumpy(templListStreamCL2[i]), cp.asnumpy(templListStreamCL2[j]), mode="full", method="fft")
            ccN = float(np.amax(corr2) /  (cp.std(templListStreamCL2[i])*cp.std(templListStreamCL2[j])*len(templListStreamCL2[i])))
            ccVecN.append(ccN)
            #dist3, corr3 = ras.ED.zdist(templListStreamCL3[i], templListStreamCL3[j], mode="fft")
            #ccZ = float(cp.asnumpy(cp.amax(corr3) / len(templListStreamCL3[i])))
            corr3 = signal.correlate(cp.asnumpy(templListStreamCL3[i]), cp.asnumpy(templListStreamCL3[j]), mode="full", method="fft")
            ccZ = float(np.amax(corr3) /  (cp.std(templListStreamCL3[i])*cp.std(templListStreamCL3[j])*len(templListStreamCL3[i])))
            ccVecZ.append(ccZ)

        print(iCount, templListTbegCL1[i])
        text = str(iCount)+" "+str(templListTbegCL1[i])+"\n"
        eventsFile.write(text)

        covMatrixE.append(ccVecE)
        covMatrixN.append(ccVecN)
        covMatrixZ.append(ccVecZ)


        # if templListChCL[i] == 'HHZ':
        #     iCount = iCount + 1
        #     for j in range(len(templListStreamCL)):
        #         if templListChCL[j]== 'HHZ':
        #             jCount = jCount + 1
        #             dist, corr = ras.ED.zdist(templListStreamCL[i],
        #                                       templListStreamCL[j],
        #                                       mode="fft"
        #                                      )
        #             ccZ = float(cp.asnumpy(cp.amax(corr) / len(templListStreamCL[i])))
        #             ccVecZ.append(ccZ)
        #     covMatrixZ.append(ccVecZ)
        #     print(iCount, templListTbegCL[i])
        #     text = str(iCount)+" "+str(templListTbegCL[i])+"\n"
        #     eventsFile.write(text)
        # elif templListChCL[i] == 'HHE':
        #     for j in range(len(templListStreamCL)):
        #         if templListChCL[j] == 'HHE':
        #             dist, corr = ras.ED.zdist(templListStreamCL[i],
        #                                       templListStreamCL[j],
        #                                       mode="fft"
        #                                      )
        #             ccE = float(cp.asnumpy(cp.amax(corr) / len(templListStreamCL[i])))
        #             ccVecE.append(ccE)
        #     covMatrixE.append(ccVecE)
        # elif templListChCL[i] == 'HHN':
        #     for j in range(len(templListStreamCL)):
        #         if templListChCL[j] == 'HHN':
        #             dist, corr = ras.ED.zdist(templListStreamCL[i],
        #                                       templListStreamCL[j],
        #                                       mode="fft"
        #                                      )
        #             ccN = float(cp.asnumpy(cp.amax(corr) / len(templListStreamCL[i])))
        #             ccVecN.append(ccN)
        #     covMatrixN.append(ccVecN)

    eventsFile.close()

    npCovMatrixE = np.asarray(covMatrixE)
    npCovMatrixN = np.asarray(covMatrixN)
    npCovMatrixZ = np.asarray(covMatrixZ)

    npCovMatrix = (npCovMatrixZ + npCovMatrixE + npCovMatrixN)/3

    npCovMatrixRes = npCovMatrix / np.amax(npCovMatrix)


    return np.array(npCovMatrixRes)

def extractTemplatesFlag(templateList, netStat, vecComp, dailyDir, patternLen, fmin, fmax, eventsFileName, tWindowsDF):

    templListStream = [] #Stream()
    templListSr     = []
    templListCh     = []
    templListTbeg   = []
    templListLen    = []

    for template in templateList:

        tWindows = tWindowsDF[tWindowsDF['Template']==template]['consecTWindows'].values[0]

        newTbeg = UTCDateTime(template)
        print(newTbeg)
        isSyst = False

        for comp in vecComp:
            templCut, templSr, templCh, templTbeg = getTemplateTrimed(template, newTbeg, dailyDir, netStat, comp, patternLen, fmin, fmax, isSyst)
            templCut_gpu = cp.asarray(templCut)
            templListStream.append(templCut_gpu)
            templListSr.append(templSr)
            templListCh.append(templCh)
            templListTbeg.append(templTbeg)
            templListLen.append(len(templCut))

    print("len templList : ",len(templListStream))

    templatesDF = pd.DataFrame({"Data":templListStream, "Sr":templListSr, "Channel":templListCh, "Tbeg":templListTbeg, "DataLen":templListLen})
    templatesDF.to_csv('templList.csv', index=True)

    # Reload dataframe because of some issues otherwise due to format
    templatesDF = pd.read_csv('templList.csv')

    #BP!! Warning!!!
    templatesDF_Trash = templatesDF[templatesDF["DataLen"] != (patternLen)*templSr+1] #(2*patternLen)*templSr+1]

    templatesDF_Trash.to_csv('templList_trash.csv', index=False)

    templatesDF = pd.merge(templatesDF, templatesDF_Trash, on=["Tbeg"], how="outer", indicator=True)

    templatesDF = templatesDF.loc[templatesDF["_merge"] == "left_only"].drop("_merge", axis=1)

    # Make sure we have events on 3 components after cleanup
    templatesDF["Count"] = templatesDF["Tbeg"].apply(lambda x :(templatesDF["Tbeg"] == x).sum())

    templatesDF = templatesDF[templatesDF["Count"]>=3]

    templatesDF.to_csv('templListCl_after.csv', index=False)

    templListStreamCL1 = [] #Stream()
    templListStreamCL2 = []
    templListStreamCL3 = []
    templListSrCL1     = []
    templListSrCL2     = []
    templListSrCL3     = []
    templListTbegCL1   = []
    templListTbegCL2   = []
    templListTbegCL3   = []

    count = 0

    for index in templatesDF["Unnamed: 0_x"]:
        if templatesDF.iloc[count]["Channel_x"] == vecComp[0]:
            templListStreamCL1.append(templListStream[index])
            templListSrCL1.append(templListSr[index])
            templListTbegCL1.append(templListTbeg[index])
            print(templListTbeg[index], templatesDF.iloc[count]["Tbeg"], templListCh[index], templatesDF.iloc[count]["Channel_x"])
        elif templatesDF.iloc[count]["Channel_x"] == vecComp[1]:
            templListStreamCL2.append(templListStream[index])
            templListSrCL2.append(templListSr[index])
            templListTbegCL2.append(templListTbeg[index])
        elif templatesDF.iloc[count]["Channel_x"] == vecComp[2]:
            templListStreamCL3.append(templListStream[index])
            templListSrCL3.append(templListSr[index])
            templListTbegCL3.append(templListTbeg[index])
        count += 1

    iCount = -1
    covMatrixE = []
    covMatrixN = []
    covMatrixZ = []
    eventsFile = open(eventsFileName, "w")
    for i in range(len(templListStreamCL1)):
        jCount = -1
        ccVecZ = []
        ccVecN = []
        ccVecE = []
        print(i)
        iCount = iCount + 1
        for j in range(len(templListStreamCL1)):
            jCount = jCount + 1
            corr1 = signal.correlate(cp.asnumpy(templListStreamCL1[i]), cp.asnumpy(templListStreamCL1[j]), mode="full", method="fft")
            ccE = float(np.amax(corr1) / (cp.std(templListStreamCL1[i])*cp.std(templListStreamCL1[j])*len(templListStreamCL1[i])))
            ccVecE.append(ccE)
            corr2 = signal.correlate(cp.asnumpy(templListStreamCL2[i]), cp.asnumpy(templListStreamCL2[j]), mode="full", method="fft")
            ccN = float(np.amax(corr2) /  (cp.std(templListStreamCL2[i])*cp.std(templListStreamCL2[j])*len(templListStreamCL2[i])))
            ccVecN.append(ccN)
            corr3 = signal.correlate(cp.asnumpy(templListStreamCL3[i]), cp.asnumpy(templListStreamCL3[j]), mode="full", method="fft")
            ccZ = float(np.amax(corr3) /  (cp.std(templListStreamCL3[i])*cp.std(templListStreamCL3[j])*len(templListStreamCL3[i])))
            ccVecZ.append(ccZ)

        print(iCount, templListTbegCL1[i])
        text = str(iCount)+" "+str(templListTbegCL1[i])+"\n"
        eventsFile.write(text)

        covMatrixE.append(ccVecE)
        covMatrixN.append(ccVecN)
        covMatrixZ.append(ccVecZ)

    eventsFile.close()

    npCovMatrixE = np.asarray(covMatrixE)
    npCovMatrixN = np.asarray(covMatrixN)
    npCovMatrixZ = np.asarray(covMatrixZ)

    npCovMatrix = (npCovMatrixZ + npCovMatrixE + npCovMatrixN)/3

    npCovMatrixRes = npCovMatrix / np.amax(npCovMatrix)


    return np.array(npCovMatrixRes)

def templateClustering(covMatrix, linkFileName):

    print(len(covMatrix))
    #plt.subplot(121)
    plt.imshow(covMatrix, interpolation='nearest')
    cbar = plt.colorbar()
    cbar.set_label('CC value')
    plt.show()

    dissimilarity = distance.squareform(1 - np.clip(covMatrix, -1, 1), checks=False)
    threshold = 0.75
    #linkage = hierarchy.linkage(dissimilarity, method="single")
    #linkage = hierarchy.linkage(dissimilarity, method="average")
    #linkage = hierarchy.linkage(dissimilarity, method="complete")
    linkage = hierarchy.linkage(dissimilarity, method="weighted")
    clusters = hierarchy.fcluster(linkage, threshold, criterion="distance")
    print(sorted(set(clusters)))

    np.savetxt(linkFileName, linkage, delimiter=" ")

    # A little nicer set of colors.
    cmap = plt.get_cmap('Paired', lut=6)
    colors = ['#%02x%02x%02x' % tuple(int(col * 255) for col in cmap(i)[:3]) for i in range(6)]
    try:
        hierarchy.set_link_color_palette(colors[1:])
    except AttributeError:
        # Old version of SciPy
        pass

    #plt.subplot(122)
    try:
        hierarchy.dendrogram(linkage, color_threshold=threshold,
                             above_threshold_color=cmap(0))
    except TypeError:
        # Old version of SciPy
        hierarchy.dendrogram(linkage, color_threshold=threshold)
    plt.xlabel("Event number")
    plt.ylabel("Dissimilarity")
    plt.show()

def templateClusteringFlag(covMatrix, linkFileName, new_dict):

    print(len(covMatrix))
    #plt.subplot(121)
    im = plt.imshow(covMatrix, interpolation='nearest')
    cbar = plt.colorbar()
    cbar.set_label('CC value', size=35, labelpad = 25)
    im.figure.axes[1].tick_params(labelsize=25)
    plt.xticks(size=25)
    plt.yticks(size=25)
    plt.show()

    dissimilarity = distance.squareform(1 - np.clip(covMatrix, -1, 1), checks=False)
    threshold = 0.15#5
    linkage = hierarchy.linkage(dissimilarity, method="single")
    #linkage = hierarchy.linkage(dissimilarity, method="average")
    #linkage = hierarchy.linkage(dissimilarity, method="complete")
    #linkage = hierarchy.linkage(dissimilarity, method="weighted")
    clusters = hierarchy.fcluster(linkage, threshold, criterion="distance")
    print(sorted(set(clusters)))

    #np.savetxt(linkFileName, linkage, delimiter=" ")

    #Set colors according to event type
    link_cols = {}
    dflt_col = "#ff5733"
    for i, i12 in enumerate(linkage[:, :2].astype(int)):
        c1, c2 = (link_cols[x] if x > len(linkage) else new_dict[str(x)]  for x in i12)
        link_cols[i+1+len(linkage)] = c1 if c1 == c2 else dflt_col

    hierarchy.dendrogram(linkage, color_threshold=None, link_color_func=lambda x: link_cols[x])

    plt.xlabel("Detections", size=35, labelpad=20)
    ticks = [tick for tick in plt.gca().get_xticklabels()]
    detectionId = []
    for i, t in enumerate(ticks):
        detectionId.append(t.get_text())
    plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    plt.ylabel("Dissimilarity", size=35, labelpad=20)
    plt.yticks(size=25)

    plt.show()

    return detectionId



#Clean Correlation Matrix if needed
def cleanCovMatrix(covMatrixVec, correspFile):

    print(covMatrixVec)
    indexes = []
    for i in range(len(covMatrixVec)):
        count = 0
        for j in range(len(covMatrixVec[i])):
            if covMatrixVec[i][j] < 1.0 and covMatrixVec[i][j] > 0.65:
                count += 1
        if count != 0:
            indexes.append(i)

    cleanCovMatrix = np.array([])

    if indexes != []:
        for i in indexes:
            for j in indexes:
                cleanCovMatrix = np.append(cleanCovMatrix, covMatrixVec[i][j])
        cleanCov = cleanCovMatrix.reshape(len(indexes), len(indexes))

        equivFile = open(correspFile, "w")
        for i in range(len(indexes)):
            text = str(i)+ " "+ str(indexes[i]) + "\n"
            equivFile.write(text)

        equivFile.close()

    else:
        cleanCov = covMatrixVec

    return cleanCov

def readTemplList(templFileList):
    timeWindows = []
    templTimeList = []
    templTimeListType = []
    templTimeListId = []
    templClusterId = []

    templTimeAll= np.genfromtxt(templFileList, skip_header=1, delimiter=',', usecols=(0,1,2,4), dtype=str)

    for i in range(len(templTimeAll)):
        timeWindows.append(1)
        templTimeList.append(templTimeAll[i][1])
        templTimeListType.append(templTimeAll[i][2])
        templTimeListId.append(templTimeAll[i][0])
        templClusterId.append(templTimeAll[i][3])

    tWindowsDF = pd.DataFrame({"Template": templTimeList, "consecTWindows": timeWindows})

    return templTimeList, templTimeListType, templTimeListId, tWindowsDF, templClusterId


def readStandardMatchList(matchFile):
    matchList = []
    corrList = []

    matchFileList = np.genfromtxt(matchFile, delimiter=' ', dtype=str)

    for i in range(len(matchFileList)):
        matchList.append(matchFileList[i][0])
        corrList.append(float(matchFileList[i][1]))

    matchDF = pd.DataFrame({"Event": matchList, "Corr": corrList})
    matchDF.sort_values("Event", inplace=True)

    evtClean = matchDF["Event"].to_list()
    corrClean = matchDF["Corr"].to_list()

    cleanEv = []
    cleanCorr= []
    i=0

    while i <= len(evtClean) - 1:
        TempEv = [evtClean[i]]
        TempC  = [corrClean[i]]

        while i < len(evtClean) - 1 and UTCDateTime(evtClean[i+1]) - UTCDateTime(evtClean[i]) < 20.0 :
            TempEv.append(evtClean[i+1])
            TempC.append(corrClean[i+1])
            i = i+1

        j = TempC.index(max(TempC))
        cleanEv.append(TempEv[j])
        cleanCorr.append(TempC[j])

        i = i+1

    cleanDF = pd.DataFrame({"Event": cleanEv, "Corr": cleanCorr})
    # Dirty trick to remove
    cleanDF = cleanDF[cleanDF["Corr"] >= 0.3]

    cleanDF.to_csv('CleanList.csv', index=False)

    newDF = pd.merge(matchDF, cleanDF,on=["Event"], how="outer", indicator=True)
    left = newDF.loc[newDF["_merge"] == "left_only"].drop("_merge", axis=1)
    left.sort_values("Event", inplace=True)

    left.to_csv("duplicate.csv", index=False)

    newDF = pd.DataFrame({"Template": cleanEv, "consecTWindows": np.ones(len(cleanEv))})

    print(newDF)


    return cleanEv, cleanCorr, newDF




#Print the list of the options of the script and the details then exit
def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print("-h  | --help						Print the usage message")
    print("-r  | --ras_config <config_file> To use a specific configuration file")
    sys.exit()


if __name__ == '__main__':

    # try:
    #     opts, args = getopt.getopt(sys.argv[1:],"hr:",["help","ras_config="])
    # except getopt.GetoptError:
    #     print(sys.argv[0]+' -r <configFile>')
    #     sys.exit(2)
    #
    # for opt, arg in opts:
    #     if opt in ("-h","--help"):
    #         printUsageAndExit(sys.argv[0])
    #     elif opt in ("-r", "--ras_config"):
    #         configFile = arg
    #
    # patternLen, fmin, fmax, dailyDir, vecComp, netStat, matchFile, eventsFile, tWindowsFile, linkageFileName, CovMatFileName = readConfigFile(configFile) #"../notebooks/ras_config_martin_6m.par")

    netStat = 'FR.FRNF' #MQ.PBO' #XZ.A18'
    vecComp = ['HHE', 'HHN', 'HHZ']
    matchFile = 'Massif_Central/hits/CMPS_FRNF_LBL/templates_offset/concat_2018_2024.txt' #concat_Standard.txt' #VERF_hits.txt' #concat_St_03_6m.txt' #concat_St025_2_10H_15s.txt' #concat_cluster21.txt' #concat_martin6'  #../data/hits_martinique_6Months_Z/  #hits_martinique_6Months_Z/concat_martin'  #hits_martinique/output'#CASC'
    dailyDir = '../data/wf_MC/TEMP/' #Massif_Central/data/VERF/' #../data/wf_MQ/post/' #martinique/'#CASC/'
    eventsFile = "Massif_Central/massif_central_2018_2024FRNF.txt"  #massif_cent_events_2018_2024_v2.txt" #martinique_events_6m_zdist_St_03_oct21_30s.txt" #martinique_events_6m_zdist_St_025_10H_15s_d.txt"
    correspFile = "Massif_Central/martinique_events_equivalence_id.txt"
    tWindowsFile = "Massif_Central/massif_cent_tWindows_2024.csv" #martinique_tWindows_zdist_oct21_30s.csv"
    linkageFileName = "Massif_Central/linkage_MC_FRNF_2018_2024.txt" #linkage_massif_cent_2018_2024_v2_clean2.txt" #linkage_zdist_oct21_30s.txt" #St_025_oct4.txt"
    CovMatFileName = "Massif_Central/massif_cent_FRNF_2018_2024_" #zdist_oct21_martin_CovMat_"
    # Computing mode used to produce results. Either Standard or Systematic
    mode = "Standard"
    patternLen = 30.
    fmin = 2
    fmax = 20
    covFileName = ''
    covLen = 49

    isFlaged = False
    templFileList = "Massif_Central/events_clean_MC_dec10_St02_v2.csv" #events_clean_oct15_test.csv" #oct4_St025.csv" #

    print("patternLen = ", patternLen, " fmin = ", fmin, " fmax = ",fmax," dailyDir = ", dailyDir," vecComp = ", vecComp," netStat = ", netStat)
    print(matchFile)
    print(eventsFile)
    print(tWindowsFile)
    print(linkageFileName)
    print(CovMatFileName)

    detectFileName = "Massif_Central/detectList_dec12.txt"

    if len(covFileName) == 0:
        if isFlaged:
            templTimeList, templTimeListType, templTimeListId, tWindowsDF, templClusterId = readTemplList(templFileList)
            new_dict = dict()
            for i in range((len(templTimeListType))):
                if templTimeListType[i] == 'VT':
                    new_dict.update({str(i): "#61ffff"})
                elif templTimeListType[i] == 'T':
                    new_dict.update({str(i): "#808080"})
                elif templTimeListType[i] == 'S':
                    new_dict.update({str(i): "#B061FF"})


            npCovMatrix = extractTemplatesFlag(templTimeList, netStat, vecComp, dailyDir, patternLen, fmin, fmax,eventsFile,tWindowsDF)
            npCovMatrix.tofile(CovMatFileName+str(len(npCovMatrix)))
            detectionId = templateClusteringFlag(npCovMatrix, linkageFileName, new_dict)

            detectType = []
            detectDate = []
            detectCluster = []
            for i in range(len(detectionId)):
                detectType.append(templTimeListType[int(detectionId[i])])
                detectDate.append(templTimeList[int(detectionId[i])])
                detectCluster.append(templClusterId[int(detectionId[i])])

            detectFile = open(detectFileName, "w")
            detectFile.write("id, type, date, clusterId \n")
            for i in range(len(detectionId)):
                detectFile.write(detectionId[i] + " " + detectType[i] + " " + detectDate[i] + " " + detectCluster[i] +"\n")

            detectFile.close()


        elif mode == "Standard" :

            matchList, corrList, tWindowsFile = readStandardMatchList(matchFile)
            npCovMatrix = extractTemplatesFlag(matchList, netStat, vecComp, dailyDir, patternLen, fmin, fmax, eventsFile,
                                       tWindowsFile)
            npCovMatrix.tofile(CovMatFileName + str(len(npCovMatrix)))
            templateClustering(npCovMatrix, linkageFileName)

        elif mode == "Systematic":
            matchList = ReadMatches(matchFile)
            templTimeList, tWindowsDF = sortMatches(matchList, patternLen, tWindowsFile)
            npCovMatrix = extractTemplates(templTimeList, netStat, vecComp, dailyDir, patternLen, fmin, fmax,eventsFile,tWindowsDF)
            npCovMatrix.tofile(CovMatFileName+str(len(npCovMatrix)))
            templateClustering(npCovMatrix, linkageFileName)
    else:
        npCovMatrix = np.fromfile(covFileName)
        covMatrixVec = npCovMatrix.reshape(covLen, covLen)
        covMatrixVecCl = cleanCovMatrix(covMatrixVec, correspFile)
        covMatrix = covMatrixVecCl.tolist()
        templateClustering(covMatrix, linkageFileName)
