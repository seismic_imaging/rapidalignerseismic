#
# =====================================================================
#
# 				          rapidAlignerSeismic
# 				          -------------------
#
#      	   Main authors: Bastien Plazolles and Jean-Baptiste Ammirati
#   				         CNRS, FRANCE
# 			                   (c) 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

import matplotlib.pyplot as plt
import numpy as np
import cupy as cp
import sys, os, glob
import getopt


from obspy.core import UTCDateTime,Stream, read
from obspy.geodetics import locations2degrees, degrees2kilometers
import pandas as pd

try:
    import rapidAlignerSeismic
except:
    sys.path.append(os.path.join(os.getcwd(),".."))
import rapidAlignerSeismic as ras


def getList(evList, nbEvts, linksDF):

    nList = []

    for ev in evList:
        print(ev)
        if ev <= nbEvts:
            nList.extend([int(ev)])
        else:
            id = ev - (nbEvts +1)
            print(id)
            print(linksDF)
            line = linksDF.loc[[id]].values.flatten().tolist()
            print(line)
            linksDF.drop(linksDF.loc[[id]].index, inplace=True)
            list = line [0:2]
            print(list)
            evt, linksDF =getList(list, nbEvts, linksDF)
            nList.extend(evt)

    return nList, linksDF


def getEventListFromLink(eventsFileName,linkFileName,treshold):

    eventsList = np.genfromtxt(eventsFileName, dtype=None)
    linkage = np.genfromtxt(linkFileName, delimiter=" ")
    eventCount = len(linkage)

    links = np.array([])

    for link in linkage:
        if link[2] < treshold:
            links = np.append(links, link)

    links = links.reshape(int(len(links) / 4), 4)

    linksDF = pd.DataFrame(links, columns=['ev1', 'ev2', 'dist', 'level'])
    linksDF.sort_values("dist", inplace=True, ascending=False)

    clusters = []
    # Identify clusters according to linkage
    while linksDF.empty == False:
        # Get the pair with the maximum distance compare to the treshold
        i = linksDF.query('dist == dist.max()').index
        line = linksDF.query('dist == dist.max()').values.flatten().tolist()
        # remove the line from the database
        linksDF.drop(i, inplace=True)
        evList = line[0:2]
        corr = 1 - line[2]

        cluster, linksDF = getList(evList, eventCount, linksDF)

        clusters.append(cluster)

    eventsJday = []
    eventsUTC = []
    for cluster in clusters:
        for evt in cluster:
            eventsJday.append(UTCDateTime(eventsList[int(evt)][1]).julday)
            eventsUTC.append(UTCDateTime(eventsList[int(evt)][1]))


    eventsDF = pd.DataFrame({"julday": eventsJday, "Event": eventsUTC})

    return eventsDF


def getEventListFromConcatFiles(templTypeFile):

    templListType = np.genfromtxt(templTypeFile, dtype=None)

    templateId = []
    templateType = []

    for i in range(len(templListType)):
        templateId.append(templListType[i][0])
        templateType.append(templListType[i][1])

    templateDF = pd.DataFrame({"Id": templateId, "Type": templateType})

    eventDFDoubl = pd.DataFrame(columns=["Event", "Type", "template", "Corr"])


    for i in range(len(templateDF)) :
        eventsUTC = []
        eventsCorr = []
        eventsType = []
        eventsTempl = []
        if templateDF["Type"].iloc[i] != b'N':
            eventList = np.genfromtxt("concat_cluster"+str(i)+"_St025_2_10H_15s.txt", dtype=None)

            for j in range(len(eventList)):
                eventsUTC.append(UTCDateTime(eventList[j][0]))
                eventsCorr.append(eventList[j][1])
                eventsType.append(templateDF["Type"].iloc[i])
                eventsTempl.append(i)

            eventTempDF = pd.DataFrame({"Event":eventsUTC, "Type":eventsType, "template":eventsTempl, "Corr":eventsCorr})

            eventDFDoubl = eventDFDoubl.append(eventTempDF)

    eventDFDoubl.sort_values("Event", inplace=True)

    eventsUTC = eventDFDoubl["Event"].to_list()
    eventsCorr = eventDFDoubl["Corr"].to_list()
    eventsType = eventDFDoubl["Type"].to_list()
    eventsTempl = eventDFDoubl["template"].to_list()

    cleanEv = []
    cleanCorr= []
    cleanType = []
    cleanTempl = []
    cleanJulday = []

    i = 0
    while i <= len(eventsUTC) - 1:
        TempEv = [eventsUTC[i]]
        TempC  = [eventsCorr[i]]
        TempType = [eventsType[i]]
        TempTempl = [eventsTempl[i]]

        while i < len(eventsUTC) - 1 and UTCDateTime(eventsUTC[i+1]) - UTCDateTime(eventsUTC[i]) < 10.0 :
            TempEv.append(eventsUTC[i+1])
            TempC.append(eventsCorr[i+1])
            TempType.append(eventsType[i + 1])
            TempTempl.append(eventsTempl[i + 1])
            i = i+1

        j = TempC.index(max(TempC))
        cleanEv.append(TempEv[j])
        cleanCorr.append(TempC[j])
        cleanType.append(TempType[j])
        cleanTempl.append(TempTempl[j])
        cleanJulday.append(UTCDateTime(TempEv[j]).julday)

        i = i+1

    eventsDF = pd.DataFrame({"Event":cleanEv, "Type":cleanType, "template":cleanTempl, "Corr":cleanCorr, "julday": cleanJulday})

    eventsDF.to_csv("concat_events_clean_St025_10H_15s.csv", index = False)

    return eventsDF


if __name__ == '__main__':

    catalogFileName ="Massif_Central/events-4.txt" #seismes_analyse.xy" #2021_catalog_all.csv"
    eventListSource = "CATAL" #CATAL" #either TXT, LINK, CATAL or CONCAT
    eventsFileName = "martinique_events_6m_zdist_St_03_oct2.txt"
    linkFileName = "linkage_zdist_St_03_oct2.txt"
    clusterCatalName = "Massif_Central/events_clean_MC_dec10_St02.csv"
    templTypeFile = "Martinique_Tags_St_03_Oct2.txt"



    delta = 30
    treshold = 0.4
    LatStat = 14.81030
    LonStat = -61.17080

    DateEnd = "2024-12-01T00:00:00.00000Z"
    catlSource = "ovsm"

    catalList = np.genfromtxt(catalogFileName, skip_header=0, usecols=(1,2,3,10,4) , delimiter="|", dtype=str) #kip_header=2, usecols=(0,1,2,4,6)


    catalUTC = []
    catalJday = []
    catalLat = []
    catalLon = []
    catalDist = []
    catalMag = []


    if eventListSource == "TXT":
        eventsList = np.genfromtxt(eventsFileName, usecols=1, dtype=str)
        eventsUTC = []
        eventsJday = []

        for i in range(len(eventsList)):
            eventsUTC.append(UTCDateTime(eventsList[i]))
            eventsJday.append(eventsUTC[i].julday)

        eventsDF = pd.DataFrame({"julday":eventsJday , "Event":eventsUTC})
        #Sort in case events in disorder
        eventsDF.sort_values("Event", inplace = True)
    elif eventListSource == "LINK":
        eventsDF = getEventListFromLink(eventsFileName,linkFileName,treshold)
        # Sort in case events in disorder
        eventsDF.sort_values("Event", inplace=True)
    elif eventListSource == "CATAL":
        eventsDF = pd.read_csv(clusterCatalName)
        eventsDF.sort_values("Event", inplace=True)
    elif eventListSource == "CONCAT":
        eventsDF = getEventListFromConcatFiles(templTypeFile)


    j=0
    for i in range(len(catalList)):
        #if catlSource in catalList[i][4]:
        catalUTC.append(UTCDateTime(catalList[i][0]))
        catalJday.append(catalUTC[j].julday)
        j = j+1
        catalLat.append(catalList[i][1])
        catalLon.append(catalList[i][2])
        catalDist.append(degrees2kilometers(locations2degrees(LatStat, LonStat, float(catalList[i][1]),float(catalList[i][2]))))
        catalMag.append(catalList[i][3])

    catalDF = pd.DataFrame({"julday": catalJday, "Event": catalUTC, "Lat": catalLat, "Lon": catalLon, "Dist": catalDist, "Mag" : catalMag})
    # Sort in case events in disorder
    catalDF.sort_values("Event", inplace=True)

    check = False

    newEventList = pd.DataFrame(columns=["Eventid", "Event", "Type", "julday", "clusterId"])
    notNewEventList = []

    for i in range(len(eventsDF)):
        test = catalDF.loc[catalDF["julday"]==eventsDF["julday"].iloc[i]]
        print(test)
        for index, row in test.iterrows():
            if abs(UTCDateTime(eventsDF["Event"].iloc[i]) - row["Event"]) <= delta:
                check = True
                notNewEventList.append([eventsDF["Event"].iloc[i], row["Event"], catalDF["Lat"].iloc[index], catalDF["Lon"].iloc[index], catalDF["Dist"].iloc[index], catalDF["Mag"].iloc[index]])

        if check == False:
            #newEventList.append([eventsDF["Event"].iloc[i], eventsDF["Type"].iloc[i], eventsDF["clusterId"].iloc[i]])
            newEventList = newEventList.append(eventsDF.iloc[i])

        print(check)
        check = False

    notNewEvDF = pd.DataFrame(notNewEventList, columns=["Event", "Catal", "Lat", "Lon", "Dist", "Mag"])

    MissedEvDF = catalDF[~catalDF["Event"].isin(notNewEvDF["Catal"])]

    newEvFileName = "Massif_Central/newEvent_St0.2_dec10.csv"
    missedEvFileName = "Massif_Central/missed_St0.2_dec10.csv"
    notNewEvFile = "Massif_Central/alreadyFound_St0.2_dec10.csv"

    newEventList.to_csv(newEvFileName, index = False)
    MissedEvDF.to_csv(missedEvFileName, index = False)
    notNewEvDF.to_csv(notNewEvFile, index = False)

    #for i in range(len(newEventList)):
    #    newEvFile.write(newEventList[i])

    #for i in range(len(notNewEventList)):
    #    notNewEvFile.write(str(notNewEventList[i][0]) + " " + str(notNewEventList[i][1]) + "\n")
    #notNewEvFile.close()



