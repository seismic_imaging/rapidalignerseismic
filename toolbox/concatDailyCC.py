#!/usr/bin/env python
#-*- coding: utf-8 -*-

import glob, os, sys
import getopt

import numpy as np
import matplotlib.pyplot as plt

from obspy.core import Stream, read, UTCDateTime

# Path to config File
configFile = '../notebooks/ras_config.par'

def readConfigFile(configFile):
    with open(configFile, 'r') as f:
        for line in f:
            rLine="".join(line.split()).split('=')
            if len(rLine) > 1:
                if rLine[0] == 'length':
                    length = eval(rLine[1])
                    assert type(length) is float or int
                elif rLine[0] == 'thresholdMethod':
                    thresholdMeth  = eval(rLine[1])
                elif rLine[0] == 'threshVal':
                    threshVal = eval(rLine[1])
                    assert type(threshVal) is float
                elif rLine[0] == 'systMultFactor':
                    systMultFactor = eval(rLine[1])
                    assert type(systMultFactor) is float or int
                elif rLine[0] == 'standMultFactor':
                    standMultFactor = eval(rLine[1])
                    assert type(systMultFactor) is float or int
                elif rLine[0] == 'outputDir':
                    if rLine[1] != '':
                        output_dir = eval(rLine[1])

    return length, thresholdMeth, threshVal, systMultFactor, standMultFactor, output_dir

def MatchFinder(corrStack, threshold, year, jday, templateLen, dailySr):

    # DEBUG Check corrStack see if everything's fine
    #plt.plot(cp.asnumpy(corrStack)/ templateLen)
    #plt.axhline(y=threshold, color='red', linestyle='--')
    #plt.ylim([-1, 1])
    #plt.title(templateName)
    #plt.show()

    # Get the index of the cc values above threshold
    idxList = np.argwhere(corrStack / templateLen > threshold)

    matchIdxList, matchCorrList = [], []
    for i in range(len(idxList)):
        if corrStack[idxList[i][0]] > corrStack[idxList[i][0]+1] and corrStack[idxList[i][0]] > corrStack[idxList[i][0]-1]:
            matchIdxList.append(idxList[i][0])
            matchCorrList.append(float(corrStack[idxList[i][0]] / templateLen))

    # Get the UTCDateTime corresponding to each match
    matchTimeList = [UTCDateTime(year=year, julday=jday) + float(idx / dailySr) for idx in matchIdxList]

    return [matchTimeList, matchCorrList]


#Print the list of the options of the script and the details then exit
def printUsageAndExit(script):
    print("Usage %s [options]"%(script))
    print("-h  | --help						Print the usage message")
    print("-r  | --ras_config <config_file> To use a specific configuration file. By default '%s'"%(configFile))
    print("-s  | --save_stack <saveDailyStack> To save the daily stack of all staions. True or False")
    sys.exit()

if __name__ == "__main__":

    #resDir = '../data/hits_MC_Standard'

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hr:s:",["help","ras_config=","save_stack="])
    except getopt.GetoptError:
        print(sys.argv[0]+' -r <configFile> -s <saveDailyStack>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h","--help"):
            printUsageAndExit(sys.argv[0])
        elif opt in ("-r", "--ras_config"):
            configFile = arg
        elif opt in ("-s", "--ras_config", "--save_stack"):
            saveStack = arg


    # Read parameters from configuration file
    length, thresholdMeth, threshVal, systMultFactor, standMultFactor, resDir = readConfigFile(configFile)

    resFiles = os.listdir(resDir)

    dates = [file[-7:-4] for file in resFiles]

    # BP!! Will need to be change once the results are changed
    year = "2024"
    #year = dates[0].split('.')[-3]
    dailySr = 100
    length = length * dailySr

    uniqDates = list(set(dates))
    uniqDates.sort()

    temps = [".".join(file.split('.')[3:7]) for file in resFiles]
    uniqTempl = list(set(temps))
    uniqTempl.sort()

    # If something went wrong while processing a day it will appear here
    trash = open(resDir + '/trash_daily.txt', 'w')

    for date in uniqDates:
        for template in uniqTempl:

            toConcatFilesList=[]
            for file in resFiles:
                if date in file .split('.')[-2] and template in file:
                    toConcatFilesList.append(file)

            ccStacked = []
            stationlist = []
            for file in toConcatFilesList:
                cc = np.load(resDir+'/'+file)
                ccStacked.append(cc)
                stationlist.append(file.split('.')[2])
            stack = np.mean(ccStacked, axis=0)

            if thresholdMeth == "Fixed":
                threshold = threshVal
            else:
                # Mean average deviation (MAD)
                mad = np.mean(np.absolute(stack - np.median(stack)))
                treshold = float((mad * systMultFactor) / length)


            # Extract matches
            try:
                matchList = MatchFinder(stack, threshold, int(year), int(date), length, dailySr)

                if saveStack:
                    np.save(resDir + '/' + template + '.full.' + year + '.' + date + '.npy', stack / length)

                # Export matches to disk
                stations = ".".join(stationlist)
                matchOutFile = open(
                    resDir + '/' + template + '.' + '.' + year + '.' + date , 'w')
                for i in range(len(matchList[0])):
                    print(matchList[0][i], '{:.3f}'.format(matchList[1][i]),  ' ', len(stationlist), ' ', stations, file=matchOutFile)
                matchOutFile.close()

                # remove empty files
                if os.path.getsize(
                        resDir + '/' + template + '.' + '.' + year + '.' + date) == 0:
                    os.system(
                        'rm -rf ' + resDir + '/' + template + '.' + '.' + year + '.' + date)

            except:
                print("Error while processing the results of day ", date)
                print("Proceed to next day")

                print(year, date, template, file = trash)



    trash.close()