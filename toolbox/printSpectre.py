#!/usr/bin/env python
#-*- coding: utf-8 -*-

from obspy.core import UTCDateTime
from obspy.core import Stream, read
import matplotlib.pyplot as plt
import os, glob, shutil
import numpy as np
import pandas as pd

def loadEvent(event, repo, comp, fmin, fmax, mindeltat, plusdeltat):

    date = UTCDateTime(event)
    print(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily=read(repo+'*'+comp+"*"+str(date.year)+'.'+str(date.julday).zfill(3))
    Daily.taper(max_percentage=0.0001, type="hann")
    Daily.merge(fill_value=0)
    Daily.filter("bandpass", freqmin=fmin,freqmax=fmax)
    t_ini=date
    t_ini=t_ini+mindeltat
    t_end = t_ini+plusdeltat
    Daily.trim(t_ini ,t_end, pad=True, nearest_sample = False, fill_value=0)

    return Daily[0].data




if __name__ == "__main__":
	
	eventsFileName = "Massif_Central/events_clean_MC_dec10_St02_select.csv"
	resDir = "Massif_Central/data/VERF/"
	
	
	fmin = 1
	fmax = 30
	length = 30
	vecComp = ["HHE", "HHN", "HHZ"]
	mindeltat = 0
	plusdeltat = length 
	
	
	eventsList = np.genfromtxt(eventsFileName, skip_header = 1, usecols = (1,4) , delimiter=",", dtype=str)
	
	
	events = []
	cluster = []
	
	for i in range(len(eventsList)):
		events.append(eventsList[i][0])
		cluster.append(eventsList[i][1])
	
	
	for i in range(len(events)):
		event = events[i]
		clusterNum = cluster[i]
		
		print(event)
		
		signalE = loadEvent(event, resDir, vecComp[0], fmin, fmax, mindeltat, plusdeltat)
		signalN = loadEvent(event, resDir, vecComp[1], fmin, fmax, mindeltat, plusdeltat)
		signalZ = loadEvent(event, resDir, vecComp[2], fmin, fmax, mindeltat, plusdeltat)
		
		fourrierE  = np.abs(np.fft.fft(signalE))
		fourrierN  = np.abs(np.fft.fft(signalN))
		fourrierZ  = np.abs(np.fft.fft(signalZ))
		
		n = signalE.size
		
		timestep = 1 /100.
		
		freq = np.fft.fftfreq(n, d=timestep)
		
		npt = int(len(freq)/2)
		
		truefreq=freq[0:npt]
		
		
		fig = plt.figure()
		ax = fig.add_subplot(3,2,1)
		ax.plot(signalZ)
		ax.set_title('Z')
		plt.xlabel("Time (s)")
		xlocs, xlabs = plt.xticks()
		xlabs = xlocs / 100.0
		plt.xticks(xlocs, xlabs)
		plt.xlim(0, 3000)
		
		ax = fig.add_subplot(3,2,2)
		ax.plot(truefreq, fourrierZ[0:npt])
		ax.set_title('Z')
		plt.xlabel("Frequency (Hz)")
		plt.xlim(0, 30)
		
		ax = fig.add_subplot(3,2,3)
		ax.plot(signalE)
		ax.set_title('E')
		plt.xlabel("Time (s)")
		xlocs, xlabs = plt.xticks()
		xlabs = xlocs / 100.0
		plt.xticks(xlocs, xlabs)
		plt.xlim(0, 3000)
		
		
		ax = fig.add_subplot(3,2,4)
		ax.plot(truefreq, fourrierE[0:npt])
		ax.set_title('E')
		plt.xlabel("Frequency (Hz)")
		plt.xlim(0, 30)
		
		
		ax = fig.add_subplot(3,2,5)
		ax.plot(signalN)
		ax.set_title('N')
		plt.xlabel("Time (s)")
		xlocs, xlabs = plt.xticks()
		xlabs = xlocs / 100.0
		plt.xticks(xlocs, xlabs)
		plt.xlim(0, 3000)
		
		ax = fig.add_subplot(3,2,6)
		ax.plot(truefreq, fourrierN[0:npt])
		ax.set_title('N')
		plt.xlabel("Frequency (Hz)")
		plt.xlim(0, 30)
		
				
		plt.savefig('Massif_Central/spectro_2018_2024/'+ str(clusterNum) + '/' + str(event) +'.svg', format='svg', dpi=1200)
		plt.close()
		#plt.show()
		
		
		
		
	
